//
//  MockEntity.h
//  UnitTests
//
//  Created by Ben Hopewell on 20/2/19.
//  Copyright © 2019 Ben Hopewell. All rights reserved.
//

#pragma once

#include "EngineCore/Entities/IEntity.h"

class EntityStub : public Hope::EngineCore::IEntity
{
public:
    virtual void Tick(f32) override { }
};
