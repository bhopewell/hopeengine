//
//  MockComponent.h
//  UnitTests
//
//  Created by Ben Hopewell on 18/2/19.
//  Copyright © 2019 Ben Hopewell. All rights reserved.
//

#pragma once

#include "EngineCore/Components/IComponent.h"

class ComponentStub : public Hope::EngineCore::IComponent
{
public:
    ComponentStub() : IComponent(), mTestArg(0) {}
    ComponentStub(int testArg) : IComponent(), mTestArg(testArg) {}
    
    virtual void Initialise() override { }
    virtual void Tick(f32 deltaTime) override { HOPE_UNUSED(deltaTime) }
    virtual void Destroy() override { }
    
    int GetTestArg() const { return mTestArg; }
    
private:
    int mTestArg;
};
