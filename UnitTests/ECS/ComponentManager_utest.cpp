//
//  ComponentManager_utest.cpp
//  UnitTests
//
//  Created by Ben Hopewell on 18/2/19.
//  Copyright © 2019 Ben Hopewell. All rights reserved.
//

#include <gtest/gtest.h>

#include "Stub/ComponentStub.h"
#include "EngineCore/Components/ComponentManager.h"

using namespace ::testing;
using namespace Hope::EngineCore;

class ComponentManager_utest : public Test
{
public:
    void SetUp()
    {
        mComponentManager = new ComponentManager();
    }
    
    void TearDown()
    {
        delete mComponentManager;
    }
    
protected:
    ComponentManager* mComponentManager;
};

TEST_F(ComponentManager_utest, ConstructComponent_NoArgs)
{
    //Construct the component via the ComponentManager
    ComponentStub* component = mComponentManager->ConstructComponent<ComponentStub>();
    ASSERT_NE(nullptr, component);
    EXPECT_EQ(0, component->GetTestArg());
}

TEST_F(ComponentManager_utest, ConstructComponent_Args)
{
    //Construct the component via the ComponentManager
    ComponentStub* component = mComponentManager->ConstructComponent<ComponentStub>(999);
    ASSERT_NE(nullptr, component);
    EXPECT_EQ(999, component->GetTestArg());
}

TEST_F(ComponentManager_utest, ConstructComponent_Duplicate)
{
    //Construct component1 via the ComponentManager
    ComponentStub* component1 = mComponentManager->ConstructComponent<ComponentStub>();
    ASSERT_NE(nullptr, component1);

    //Construct component2 via the ComponentManager
    ComponentStub* component2 = mComponentManager->ConstructComponent<ComponentStub>();
    ASSERT_NE(nullptr, component2);
    EXPECT_NE(component1, component2);
}

TEST_F(ComponentManager_utest, FindComponent_NonTemplateMethod)
{
    //Construct component1 via the ComponentManager
    ComponentStub* component1 = mComponentManager->ConstructComponent<ComponentStub>();
    ASSERT_NE(nullptr, component1);

    IComponent* foundComponent = mComponentManager->FindComponent(component1->GetComponentId());
    ASSERT_NE(nullptr, foundComponent);
    EXPECT_EQ(component1, foundComponent);
}

TEST_F(ComponentManager_utest, FindComponentWhenMultiple_NonTemplateMethod)
{
    //Construct component1 via the ComponentManager
    ComponentStub* component1 = mComponentManager->ConstructComponent<ComponentStub>();
    ASSERT_NE(nullptr, component1);

    //Construct component2 via the ComponentManager
    ComponentStub* component2 = mComponentManager->ConstructComponent<ComponentStub>();
    ASSERT_NE(nullptr, component2);
    EXPECT_NE(component1, component2);

    //Find the correct component
    IComponent* foundComponent = mComponentManager->FindComponent(component1->GetComponentId());
    ASSERT_NE(nullptr, foundComponent);
    EXPECT_EQ(component1, foundComponent);
    EXPECT_NE(component2, foundComponent);
}

TEST_F(ComponentManager_utest, FindComponent_TemplateMethod)
{
    //Construct component1 via the ComponentManager
    ComponentStub* component1 = mComponentManager->ConstructComponent<ComponentStub>();
    ASSERT_NE(nullptr, component1);

    ComponentStub* foundComponent = mComponentManager->FindComponent<ComponentStub>(component1->GetComponentId());
    ASSERT_NE(nullptr, foundComponent);
    EXPECT_EQ(component1, foundComponent);
}

TEST_F(ComponentManager_utest, FindComponentWhenMultiple_TemplateMethod)
{
    //Construct component1 via the ComponentManager
    ComponentStub* component1 = mComponentManager->ConstructComponent<ComponentStub>();
    ASSERT_NE(nullptr, component1);

    //Construct component2 via the ComponentManager
    ComponentStub* component2 = mComponentManager->ConstructComponent<ComponentStub>();
    ASSERT_NE(nullptr, component2);
    EXPECT_NE(component1, component2);

    //Find the correct component
    ComponentStub* foundComponent = mComponentManager->FindComponent<ComponentStub>(component1->GetComponentId());
    ASSERT_NE(nullptr, foundComponent);
    EXPECT_EQ(component1, foundComponent);
    EXPECT_NE(component2, foundComponent);
}

TEST_F(ComponentManager_utest, DestroyComponent)
{
    //Construct component1 via the ComponentManager
    ComponentStub* component1 = mComponentManager->ConstructComponent<ComponentStub>();
    ASSERT_NE(nullptr, component1);

    //Destroy the component
    const HComponentId& componentId = component1->GetComponentId();
    mComponentManager->DestroyComponent(component1);

    IComponent* component = mComponentManager->FindComponent(componentId);
    EXPECT_EQ(nullptr, component);
}

TEST_F(ComponentManager_utest, DestroyComponentWhenMultiple)
{
    //Construct component1 via the ComponentManager
    ComponentStub* component1 = mComponentManager->ConstructComponent<ComponentStub>();
    ASSERT_NE(nullptr, component1);

    //Construct component2
    ComponentStub* component2 = mComponentManager->ConstructComponent<ComponentStub>();
    ASSERT_NE(nullptr, component2);
    EXPECT_NE(component1, component2);

    //Destroy component1
    const HComponentId& componentId1 = component1->GetComponentId();
    mComponentManager->DestroyComponent(component1);

    //Verify the first component has been destroyed
    IComponent* component = mComponentManager->FindComponent(componentId1);
    EXPECT_EQ(nullptr, component);

    //Ensure the second component has not been destroyed
    component = mComponentManager->FindComponent(component2->GetComponentId());
    EXPECT_NE(nullptr, component);
}

TEST_F(ComponentManager_utest, BeginEqualsEndWhenEmpty)
{   
    ComponentIterator<ComponentStub> begin = mComponentManager->begin<ComponentStub>();
    ComponentIterator<ComponentStub> end = mComponentManager->end<ComponentStub>();
    
    EXPECT_TRUE(begin == end);
}

TEST_F(ComponentManager_utest, Iterate)
{
    //Construct component1 via the ComponentManager
    ComponentStub* component1 = mComponentManager->ConstructComponent<ComponentStub>();
    ASSERT_NE(nullptr, component1);

    //Construct component2
    ComponentStub* component2 = mComponentManager->ConstructComponent<ComponentStub>();
    ASSERT_NE(nullptr, component2);
    EXPECT_NE(component1, component2);

    //ComponentIterator should point to component1
    ComponentIterator<ComponentStub> it = mComponentManager->begin<ComponentStub>();
    EXPECT_EQ(component1, *it);

    //Move the iterator once - should now point to component2
    ++it;
    EXPECT_EQ(component2, *it);

    //Verify the component iterator sets itself to the end item
    ++it;
    EXPECT_EQ(mComponentManager->end<ComponentStub>(), it);
}
