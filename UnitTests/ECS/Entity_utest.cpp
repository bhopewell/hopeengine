//
//  Entity_utest.cpp
//  UnitTests
//
//  Created by Ben Hopewell on 20/2/19.
//  Copyright © 2019 Ben Hopewell. All rights reserved.
//

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "Stub/EntityStub.h"
#include "Stub/ComponentStub.h"
#include "EngineCore/Entities/EntityManager.h"

using namespace ::testing;
using namespace Hope::EngineCore;

//Fixture class
class Entity_utest : public Test
{
public:
    void SetUp()
    {
        mComponentManager = new ComponentManager();
        mEntityManager = new EntityManager(mComponentManager);
        mEntity = mEntityManager->ConstructEntity<EntityStub>();
    }
    
    void TearDown()
    {
        mEntityManager->DestroyEntity(mEntity);
        delete mEntityManager;
        delete mComponentManager;
    }
    
protected:
    ComponentManager* mComponentManager;
    EntityManager* mEntityManager;
    IEntity* mEntity;
};

TEST_F(Entity_utest, AttachComponent)
{
    //Ensure the component map is empty
    std::vector<IComponent*> components = mEntity->GetAllComponents();
    ASSERT_EQ(0, components.size());
    
    //Attach a components
    ComponentStub* component = mComponentManager->ConstructComponent<ComponentStub>();
    mEntity->AttachComponent(component);
    
    //Verify the component has indeed been added
    components = mEntity->GetAllComponents();
    ASSERT_EQ(1, components.size());

    std::vector<IComponent*>::const_iterator it = components.begin();
    EXPECT_EQ(component, *it);
}

TEST_F(Entity_utest, RemoveComponent)
{
    //Attach a components
    ComponentStub* component = mComponentManager->ConstructComponent<ComponentStub>();
    mEntity->AttachComponent(component);
    
    //Verify the component has indeed been added
    std::vector<IComponent*> components = mEntity->GetAllComponents();
    ASSERT_EQ(1, components.size());
    
    std::vector<IComponent*>::const_iterator it = components.begin();
    EXPECT_EQ(component, *it);
    
    //Remove the component
    mEntity->RemoveComponent(component);
    
    //Ensure the component has been removed
    components = mEntity->GetAllComponents();
    EXPECT_EQ(0, components.size());
}

TEST_F(Entity_utest, GetComponentsOfType_NoComponents)
{
    const std::vector<ComponentStub*>& components = mEntity->GetComponentsOfType<ComponentStub>();
    EXPECT_EQ(0, components.size());
}

TEST_F(Entity_utest, GetComponentsOfType)
{
    ComponentStub* component1 = mComponentManager->ConstructComponent<ComponentStub>();
    ComponentStub* component2 = mComponentManager->ConstructComponent<ComponentStub>();
    
    //Attach one component and ensure it is returned when calling GetComponentsOfType
    mEntity->AttachComponent(component1);
    std::vector<ComponentStub*> components = mEntity->GetComponentsOfType<ComponentStub>();
    ASSERT_EQ(1, components.size());
    EXPECT_THAT(components, UnorderedElementsAre(
        component1
    ));
    
    //Attach the second component and ensure both components are returned when calling GetComponentsOfType
    mEntity->AttachComponent(component2);
    components = mEntity->GetComponentsOfType<ComponentStub>();
    ASSERT_EQ(2, components.size());
    EXPECT_THAT(components, UnorderedElementsAre(
        component1,
        component2
    ));
}

TEST_F(Entity_utest, GetComponent_NoComponent)
{
    ComponentStub* component1 = mComponentManager->ConstructComponent<ComponentStub>();
    EXPECT_EQ(nullptr, mEntity->GetComponent(component1->GetComponentId()));
}

TEST_F(Entity_utest, GetComponent)
{
    ComponentStub* component1 = mComponentManager->ConstructComponent<ComponentStub>();
    ComponentStub* component2 = mComponentManager->ConstructComponent<ComponentStub>();
    
    //Attach one component and ensure it is returned when calling GetComponent
    mEntity->AttachComponent(component1);
    EXPECT_EQ(component1, mEntity->GetComponent(component1->GetComponentId()));
    
    //Attach the second component and ensure it is returned when calling GetComponent.
    mEntity->AttachComponent(component2);
    EXPECT_EQ(component2, mEntity->GetComponent(component2->GetComponentId()));
    
    //Ensure component2 is not returned when calling GetComponent with component1s ComponentId.
    EXPECT_NE(component2, mEntity->GetComponent(component1->GetComponentId()));
}
