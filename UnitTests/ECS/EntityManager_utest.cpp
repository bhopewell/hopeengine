//
//  EntityManager_utest.cpp
//  UnitTests
//
//  Created by Ben Hopewell on 20/2/19.
//  Copyright © 2019 Ben Hopewell. All rights reserved.
//

#include <gtest/gtest.h>

#include "Stub/EntityStub.h"
#include "EngineCore/Components/ComponentManager.h"
#include "EngineCore/Entities/EntityManager.h"

using namespace ::testing;
using namespace Hope::EngineCore;

class EntityManager_utest : public Test
{
public:
    void SetUp()
    {
        mComponentManager = new ComponentManager();
        mEntityManager = new EntityManager(mComponentManager);
    }
    
    void TearDown()
    {
        delete mEntityManager;
        delete mComponentManager;
    }
    
protected:
    ComponentManager* mComponentManager;
    EntityManager* mEntityManager;
};

TEST_F(EntityManager_utest, ConstructEntity)
{
    //Construct the entity via the EntityManager
    EntityStub* entity = mEntityManager->ConstructEntity<EntityStub>();
    EXPECT_NE(nullptr, entity);
}

TEST_F(EntityManager_utest, ConstructEntity_Duplicate)
{
    //Construct the entity1 via the EntityManager
    EntityStub* entity1 = mEntityManager->ConstructEntity<EntityStub>();
    ASSERT_NE(nullptr, entity1);

    //Construct the entity2 via the EntityManager
    EntityStub* entity2 = mEntityManager->ConstructEntity<EntityStub>();
    ASSERT_NE(nullptr, entity2);
    EXPECT_NE(entity1, entity2);
}

TEST_F(EntityManager_utest, FindEntity_NonTemplateMethod)
{
    EntityStub* entity1 = mEntityManager->ConstructEntity<EntityStub>();
    EXPECT_EQ(entity1, mEntityManager->FindEntity(entity1->GetEntityId()));
}

TEST_F(EntityManager_utest, FindEntityWhenMultiple_NonTemplateMethod)
{
    EntityStub* entity1 = mEntityManager->ConstructEntity<EntityStub>();
    EntityStub* entity2 = mEntityManager->ConstructEntity<EntityStub>();
    
    EXPECT_EQ(entity1, mEntityManager->FindEntity(entity1->GetEntityId()));
    EXPECT_EQ(entity2, mEntityManager->FindEntity(entity2->GetEntityId()));
}

TEST_F(EntityManager_utest, FindEntity_TemplateMethod)
{
    EntityStub* entity1 = mEntityManager->ConstructEntity<EntityStub>();
    EXPECT_EQ(entity1, mEntityManager->FindEntity<EntityStub>(entity1->GetEntityId()));
}

TEST_F(EntityManager_utest, FindEntityWhenMultiple_TemplateMethod)
{
    EntityStub* entity1 = mEntityManager->ConstructEntity<EntityStub>();
    EntityStub* entity2 = mEntityManager->ConstructEntity<EntityStub>();
    
    EXPECT_EQ(entity1, mEntityManager->FindEntity<EntityStub>(entity1->GetEntityId()));
    EXPECT_EQ(entity2, mEntityManager->FindEntity<EntityStub>(entity2->GetEntityId()));
}

TEST_F(EntityManager_utest, DestroyEntity)
{
    //Construct the entity via the EntityManager
    EntityStub* entity = mEntityManager->ConstructEntity<EntityStub>();
    ASSERT_NE(nullptr, entity);
    EXPECT_EQ(entity, mEntityManager->FindEntity(entity->GetEntityId()));
    
    //Destroy the entity and verify it no longer exists.
    mEntityManager->DestroyEntity(entity);
    EXPECT_EQ(nullptr, mEntityManager->FindEntity(entity->GetEntityId()));
}

TEST_F(EntityManager_utest, DestroyEntity_MoreThanOnce)
{
    //Construct the entity via the EntityManager
    EntityStub* entity = mEntityManager->ConstructEntity<EntityStub>();
    ASSERT_NE(nullptr, entity);
    EXPECT_EQ(entity, mEntityManager->FindEntity(entity->GetEntityId()));
    
    //Destroy the entity and verify it no longer exists.
    mEntityManager->DestroyEntity(entity);
    EXPECT_EQ(nullptr, mEntityManager->FindEntity(entity->GetEntityId()));
    
    //Destroy the entity a second time
    mEntityManager->DestroyEntity(entity);
}

TEST_F(EntityManager_utest, DestroyEntityWhenMultiple)
{
    //Construct the entity via the EntityManager
    EntityStub* entity1 = mEntityManager->ConstructEntity<EntityStub>();
    EXPECT_NE(nullptr, entity1);
    EXPECT_EQ(entity1, mEntityManager->FindEntity(entity1->GetEntityId()));
    
    //Construct the second entity via the EntityManager
    EntityStub* entity2 = mEntityManager->ConstructEntity<EntityStub>();
    EXPECT_NE(nullptr, entity2);
    EXPECT_EQ(entity2, mEntityManager->FindEntity(entity2->GetEntityId()));
    
    //Destroy the entity and verify it no longer exists.
    mEntityManager->DestroyEntity(entity1);
    EXPECT_EQ(nullptr, mEntityManager->FindEntity(entity1->GetEntityId()));
    EXPECT_EQ(entity2, mEntityManager->FindEntity(entity2->GetEntityId()));
    
    //Destroy the second entity and verify it no longer exists
    mEntityManager->DestroyEntity(entity2);
    EXPECT_EQ(nullptr, mEntityManager->FindEntity(entity2->GetEntityId()));
}

TEST_F(EntityManager_utest, BeginEqualsEndWhenEmpty)
{    
    EntityIterator<EntityStub> begin = mEntityManager->begin<EntityStub>();
    EntityIterator<EntityStub> end = mEntityManager->end<EntityStub>();
    
    EXPECT_TRUE(begin == end);
}

TEST_F(EntityManager_utest, Iterate)
{
    //Construct entity1 via the EntityManager
    EntityStub* entity1 = mEntityManager->ConstructEntity<EntityStub>();
    ASSERT_NE(nullptr, entity1);
    
    //Construct entity2 via the EntityManager
    EntityStub* entity2 = mEntityManager->ConstructEntity<EntityStub>();
    ASSERT_NE(nullptr, entity2);
    
    //EntityIterator should point to entity1
    EntityIterator<EntityStub> it = mEntityManager->begin<EntityStub>();
    EXPECT_EQ(entity1, *it);
    
    //Move the iterator once - should now point to entity2
    ++it;
    EXPECT_EQ(entity2, *it);
    
    //Verify the entity iterator sets itself to the end item
    ++it;
    EXPECT_EQ(mEntityManager->end<EntityStub>(), it);
}
