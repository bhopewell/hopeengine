//
//  DynamicPoolAllocator_utest.cpp
//  UnitTests
//
//  Created by Ben Hopewell on 20/2/19.
//  Copyright © 2019 Ben Hopewell. All rights reserved.
//

#include <gtest/gtest.h>
#include "Core/Memory/Allocators/PoolFreeList.h"
#include "Stub/DynamicPoolAllocatorStub.h"

using namespace ::testing;

class DynamicPoolAllocator_utest : public Test
{
public:
    void SetUp()
    {
        mAllocator = new DynamicPoolAllocatorStub(8, 4);
        mAllocator_Padding = new DynamicPoolAllocatorStub(9, 4);
        mAllocator_InvalidSize = new DynamicPoolAllocatorStub(sizeof(Hope::Core::PoolFreeList*) - 1, 4);
    }
    
    void TearDown()
    {
        delete mAllocator_InvalidSize;
        delete mAllocator_Padding;
        delete mAllocator;
    }
    
protected:
    DynamicPoolAllocatorStub* mAllocator;
    DynamicPoolAllocatorStub* mAllocator_Padding;
    DynamicPoolAllocatorStub* mAllocator_InvalidSize;
};

TEST_F(DynamicPoolAllocator_utest, Allocate)
{   
    void* memBlock1 = mAllocator->InvokeAllocateObject();
    ASSERT_NE(nullptr, memBlock1);
    
    void* memBlock2 = mAllocator->InvokeAllocateObject();
    ASSERT_NE(nullptr, memBlock2);
    
    void* memBlock3 = mAllocator->InvokeAllocateObject();
    ASSERT_NE(nullptr, memBlock3);
    
    uintptr_t diff1 = (reinterpret_cast<uintptr_t>(memBlock2) - reinterpret_cast<uintptr_t>(memBlock1));
    EXPECT_EQ(8, diff1);
    
    uintptr_t diff2 = (reinterpret_cast<uintptr_t>(memBlock3) - reinterpret_cast<uintptr_t>(memBlock1));
    EXPECT_EQ(16, diff2);
}

TEST_F(DynamicPoolAllocator_utest, AllocateMultiplePools)
{
    // Allocate 2 full pools
    for(u32 i = 0; i < 20; ++i)
    {
        void* memBlock = mAllocator->InvokeAllocateObject();
        ASSERT_NE(nullptr, memBlock);
    }
    
    EXPECT_EQ(2, mAllocator->GetPoolAllocatorCount());
    EXPECT_EQ(20*8, mAllocator->GetBytesUsed());
    EXPECT_EQ(20, mAllocator->GetAllocations());
}

// Test that allocating, freeing and then reallocating works as expected
TEST_F(DynamicPoolAllocator_utest, AllocateFreeAllocate)
{
    void* memBlock1 = mAllocator->InvokeAllocateObject();
    ASSERT_NE(nullptr, memBlock1);
    
    void* memBlock2 = mAllocator->InvokeAllocateObject();
    ASSERT_NE(nullptr, memBlock2);
    
    // Free the first allocated block of memory
    mAllocator->InvokeFree(memBlock1);
    ASSERT_EQ(1, mAllocator->GetAllocations());
    EXPECT_EQ(8, mAllocator->GetBytesUsed());
    
    // This should allocate to the same block as memBlock1
    void* memBlock3 = mAllocator->InvokeAllocateObject();
    ASSERT_NE(nullptr, memBlock3);
    
    // This will allocate to the 3rd block from memStart
    void* memBlock4 = mAllocator->InvokeAllocateObject();
    ASSERT_NE(nullptr, memBlock3);
    
    uintptr_t diff1 = (reinterpret_cast<uintptr_t>(memBlock2) - reinterpret_cast<uintptr_t>(memBlock3));
    EXPECT_EQ(8, diff1);
    
    uintptr_t diff2 = (reinterpret_cast<uintptr_t>(memBlock4) - reinterpret_cast<uintptr_t>(memBlock3));
    EXPECT_EQ(16, diff2);
}

TEST_F(DynamicPoolAllocator_utest, AllocateWithPadding)
{
    void* memBlock1 = mAllocator_Padding->InvokeAllocateObject(); //Requires 3 bits padding
    ASSERT_NE(nullptr, memBlock1);
    
    void* memBlock2 = mAllocator_Padding->InvokeAllocateObject(); //Requires 3 bits padding
    ASSERT_NE(nullptr, memBlock2);
    
    void* memBlock3 = mAllocator_Padding->InvokeAllocateObject(); //Requires 3 bits padding
    ASSERT_NE(nullptr, memBlock3);
    
    // Remove 1x the header size as there should be 1 header between memBlock1 and memBlock2
    uintptr_t diff1 = (reinterpret_cast<uintptr_t>(memBlock2) - reinterpret_cast<uintptr_t>(memBlock1));
    EXPECT_EQ(12, diff1);
    
    // Remove 2x the header size as there should be 2 headers between memBlock1 and memBlock3
    uintptr_t diff2 = (reinterpret_cast<uintptr_t>(memBlock3) - reinterpret_cast<uintptr_t>(memBlock1));
    EXPECT_EQ(24, diff2);
}

TEST_F(DynamicPoolAllocator_utest, AllocateInvalidObjectSize)
{
    //Ensure we run the death test in a threadsafe manner
    ::testing::FLAGS_gtest_death_test_style = "threadsafe";
    
    // Allocate with an allocator that has an object size < sizeof(PoolFreeList*).
    // This should assert as it would break the FreeList
    ASSERT_DEATH(mAllocator_InvalidSize->InvokeAllocateObject(), "");
}

TEST_F(DynamicPoolAllocator_utest, Free)
{
    void* memBlock1 = mAllocator->InvokeAllocateObject();
    void* memBlock2 = mAllocator->InvokeAllocateObject();
    void* memBlock3 = mAllocator->InvokeAllocateObject();
    
    ASSERT_NE(nullptr, memBlock1);
    ASSERT_NE(nullptr, memBlock2);
    ASSERT_NE(nullptr, memBlock3);
    
    // Free everything in an inverse manner
    EXPECT_EQ(3, mAllocator->GetAllocations());
    EXPECT_EQ(24, mAllocator->GetBytesUsed());
    
    mAllocator->InvokeFree(memBlock3);
    EXPECT_EQ(2, mAllocator->GetAllocations());
    EXPECT_EQ(16, mAllocator->GetBytesUsed());
    
    mAllocator->InvokeFree(memBlock2);
    EXPECT_EQ(1, mAllocator->GetAllocations());
    EXPECT_EQ(8, mAllocator->GetBytesUsed());
    
    mAllocator->InvokeFree(memBlock1);
    EXPECT_EQ(0, mAllocator->GetAllocations());
    EXPECT_EQ(0, mAllocator->GetBytesUsed());
}

TEST_F(DynamicPoolAllocator_utest, Free2)
{
    void* memBlock1 = mAllocator->InvokeAllocateObject();
    void* memBlock2 = mAllocator->InvokeAllocateObject();
    void* memBlock3 = mAllocator->InvokeAllocateObject();
    
    ASSERT_NE(nullptr, memBlock1);
    ASSERT_NE(nullptr, memBlock2);
    ASSERT_NE(nullptr, memBlock3);
    
    // Free everything in a random manner
    EXPECT_EQ(3, mAllocator->GetAllocations());
    EXPECT_EQ(24, mAllocator->GetBytesUsed());
    
    mAllocator->InvokeFree(memBlock1);
    EXPECT_EQ(2, mAllocator->GetAllocations());
    EXPECT_EQ(16, mAllocator->GetBytesUsed());
    
    mAllocator->InvokeFree(memBlock3);
    EXPECT_EQ(1, mAllocator->GetAllocations());
    EXPECT_EQ(8, mAllocator->GetBytesUsed());
    
    mAllocator->InvokeFree(memBlock2);
    EXPECT_EQ(0, mAllocator->GetAllocations());
    EXPECT_EQ(0, mAllocator->GetBytesUsed());
}

TEST_F(DynamicPoolAllocator_utest, FreeMultiplePools)
{
    void* memBlocks[20];
    
    // Allocate 2 full pools
    for(u32 i = 0; i < 20; ++i)
    {
        memBlocks[i] = mAllocator->InvokeAllocateObject();
        ASSERT_NE(nullptr, memBlocks[i]);
    }
    
    EXPECT_EQ(2, mAllocator->GetPoolAllocatorCount());
    EXPECT_EQ(20*8, mAllocator->GetBytesUsed());
    EXPECT_EQ(20, mAllocator->GetAllocations());
    
    // Free all the memory blocks
    for(u32 i = 0; i < 20; ++i)
    {
        mAllocator->InvokeFree(memBlocks[i]);
        
        u32 totalBlocksRemaining = 20 - (i + 1);
        EXPECT_EQ(totalBlocksRemaining * 8, mAllocator->GetBytesUsed());
        EXPECT_EQ(totalBlocksRemaining, mAllocator->GetAllocations());
    }
}

TEST_F(DynamicPoolAllocator_utest, FreeWithPadding)
{
    void* memBlock1 = mAllocator_Padding->InvokeAllocateObject();
    void* memBlock2 = mAllocator_Padding->InvokeAllocateObject();
    void* memBlock3 = mAllocator_Padding->InvokeAllocateObject();
    
    ASSERT_NE(nullptr, memBlock1);
    ASSERT_NE(nullptr, memBlock2);
    ASSERT_NE(nullptr, memBlock3);
    
    EXPECT_EQ(3, mAllocator_Padding->GetAllocations());
    EXPECT_EQ(36, mAllocator_Padding->GetBytesUsed()); // Takes padding into account
    
    mAllocator_Padding->InvokeFree(memBlock3);
    EXPECT_EQ(2, mAllocator_Padding->GetAllocations());
    EXPECT_EQ(24, mAllocator_Padding->GetBytesUsed()); // Takes padding into account
    
    mAllocator_Padding->InvokeFree(memBlock2);
    EXPECT_EQ(1, mAllocator_Padding->GetAllocations());
    EXPECT_EQ(12, mAllocator_Padding->GetBytesUsed()); // Takes padding into account
    
    mAllocator_Padding->InvokeFree(memBlock1);
    EXPECT_EQ(0, mAllocator_Padding->GetAllocations());
    EXPECT_EQ(0, mAllocator_Padding->GetBytesUsed());
}

TEST_F(DynamicPoolAllocator_utest, Clear)
{
    void* memBlock1 = mAllocator->InvokeAllocateObject();
    EXPECT_NE(nullptr, memBlock1);
    EXPECT_EQ(8, mAllocator->GetBytesUsed());
    EXPECT_EQ(1, mAllocator->GetAllocations());
    
    //Clear the allocator
    mAllocator->InvokeClear();
    EXPECT_EQ(0, mAllocator->GetBytesUsed());
    EXPECT_EQ(0, mAllocator->GetAllocations());
}

TEST_F(DynamicPoolAllocator_utest, ClearWithPadding)
{
    void* memBlock1 = mAllocator_Padding->InvokeAllocateObject();
    EXPECT_NE(nullptr, memBlock1);
    EXPECT_EQ(12, mAllocator_Padding->GetBytesUsed());
    EXPECT_EQ(1, mAllocator_Padding->GetAllocations());
    
    //Clear the allocator
    mAllocator_Padding->InvokeClear();
    EXPECT_EQ(0, mAllocator_Padding->GetBytesUsed());
    EXPECT_EQ(0, mAllocator_Padding->GetAllocations());
}

TEST_F(DynamicPoolAllocator_utest, Iterator_BeginEqualsEndWhenEmpty)
{
    DynamicPoolAllocatorStub::DynamicPoolIterator begin = mAllocator->begin();
    DynamicPoolAllocatorStub::DynamicPoolIterator end = mAllocator->end();
    
    EXPECT_TRUE(begin == end);
}

TEST_F(DynamicPoolAllocator_utest, Iterator_Begin)
{
    void* memBlock1 = mAllocator->InvokeAllocateObject();
    EXPECT_NE(nullptr, memBlock1);
    EXPECT_EQ(8, mAllocator->GetBytesUsed());
    EXPECT_EQ(1, mAllocator->GetAllocations());
    
    // Get a begin iterator
    DynamicPoolAllocatorStub::DynamicPoolIterator it = mAllocator->begin();
    
    ASSERT_NE(it, mAllocator->end());
    EXPECT_EQ(memBlock1, *it);
}

TEST_F(DynamicPoolAllocator_utest, Iterator_Iterate)
{
    void* memBlock1 = mAllocator->InvokeAllocateObject();
    void* memBlock2 = mAllocator->InvokeAllocateObject();
    void* memBlock3 = mAllocator->InvokeAllocateObject();
    ASSERT_NE(nullptr, memBlock1);
    ASSERT_NE(nullptr, memBlock2);
    ASSERT_NE(nullptr, memBlock3);
    
    // Get a begin iterator
    DynamicPoolAllocatorStub::DynamicPoolIterator it = mAllocator->begin();
    
    ASSERT_TRUE(it != mAllocator->end());
    EXPECT_EQ(memBlock1, *it);
    
    ++it;
    EXPECT_EQ(memBlock2, *it);
    
    ++it;
    EXPECT_EQ(memBlock3, *it);
    
    ++it;
    EXPECT_EQ(mAllocator->end(), it);
}

TEST_F(DynamicPoolAllocator_utest, Iterator_IterateMultipleAllocators)
{
    void* memBlocks[20];
    for(u32 i = 0; i < 20; ++i)
    {
        memBlocks[i] = mAllocator->InvokeAllocateObject();
        ASSERT_NE(nullptr, memBlocks[i]);
    }
    
    // Get a begin iterator
    DynamicPoolAllocatorStub::DynamicPoolIterator it = mAllocator->begin();
    
    // Ensure the iterator is not an end iterator
    ASSERT_TRUE(it != mAllocator->end());
    
    // Verify the iteration logic
    for(u32 i = 0; i < 20; ++i)
    {
        EXPECT_EQ(memBlocks[i], *it);
        ++it;
    }
    
    // Ensure the iterator ends at the end it
    EXPECT_EQ(mAllocator->end(), it);
}

TEST_F(DynamicPoolAllocator_utest, Iterator_IteratePastEnd)
{
    //Ensure we run the death test in a threadsafe manner
    ::testing::FLAGS_gtest_death_test_style = "threadsafe";
    
    void* memBlock1 = mAllocator->InvokeAllocateObject();
    void* memBlock2 = mAllocator->InvokeAllocateObject();
    ASSERT_NE(nullptr, memBlock1);
    ASSERT_NE(nullptr, memBlock2);
    
    // Get a begin iterator
    DynamicPoolAllocatorStub::DynamicPoolIterator it = mAllocator->begin();
    
    ASSERT_TRUE(it != mAllocator->end());
    EXPECT_EQ(memBlock1, *it);
    
    ++it;
    EXPECT_EQ(memBlock2, *it);

    // End of collection - Should not crash on this increment
    ++it;
    EXPECT_EQ(mAllocator->end(), it);
    
    // This will attempt to increment past the end of the collection - The iterator should throw an assert in this case
    ASSERT_DEATH(++it, "");
}
