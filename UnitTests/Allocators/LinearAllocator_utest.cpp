//
//  LinearAllocator_utest.cpp
//  UnitTests
//
//  Created by Ben Hopewell on 27/10/18.
//  Copyright © 2018 Ben Hopewell. All rights reserved.
//

#include <gtest/gtest.h>
#include "Stub/LinearAllocatorStub.h"

using namespace ::testing;

class LinearAllocator_utest : public Test
{
public:
    void SetUp()
    {
        mAllocator = new LinearAllocatorStub(32);
    }
    
    void TearDown()
    {
        delete mAllocator;
    }
    
protected:
    LinearAllocatorStub* mAllocator;
};

TEST_F(LinearAllocator_utest, Allocate)
{
    void* memBlock1 = mAllocator->InvokeAllocate(4, 4);
    ASSERT_NE(nullptr, memBlock1);
    
    void* memBlock2 = mAllocator->InvokeAllocate(8, 4);
    ASSERT_NE(nullptr, memBlock2);
    
    void* memBlock3 = mAllocator->InvokeAllocate(8, 4);
    ASSERT_NE(nullptr, memBlock3);
    
    uintptr_t diff1 = reinterpret_cast<uintptr_t>(memBlock2) - reinterpret_cast<uintptr_t>(memBlock1);
    EXPECT_EQ(4, diff1);
    
    uintptr_t diff2 = reinterpret_cast<uintptr_t>(memBlock3) - reinterpret_cast<uintptr_t>(memBlock1);
    EXPECT_EQ(12, diff2);
}

TEST_F(LinearAllocator_utest, AllocateWithPadding)
{
    void* memBlock1 = mAllocator->InvokeAllocate(3, 4); //Requires 1 bit padding
    ASSERT_NE(nullptr, memBlock1);
    
    void* memBlock2 = mAllocator->InvokeAllocate(5, 4); //Requires 3 bits padding
    ASSERT_NE(nullptr, memBlock2);
    
    void* memBlock3 = mAllocator->InvokeAllocate(3, 4); //Requires 1 bit padding
    ASSERT_NE(nullptr, memBlock3);
    
    // Remove 1x the header size as there should be 1 header between memBlock1 and memBlock2
    uintptr_t diff1 = (reinterpret_cast<uintptr_t>(memBlock2) - reinterpret_cast<uintptr_t>(memBlock1));
    EXPECT_EQ(4, diff1);
    
    // Remove 2x the header size as there should be 2 headers between memBlock1 and memBlock3
    uintptr_t diff2 = (reinterpret_cast<uintptr_t>(memBlock3) - reinterpret_cast<uintptr_t>(memBlock1));
    EXPECT_EQ(12, diff2);
}

TEST_F(LinearAllocator_utest, AllocateZeroBytes)
{
    //Ensure we run the death test in a threadsafe manner
    ::testing::FLAGS_gtest_death_test_style = "threadsafe";
    
    //Allocate 0 bytes
    ASSERT_DEATH(mAllocator->InvokeAllocate(0, 4), "");
}

TEST_F(LinearAllocator_utest, AllocatePastBounds)
{
    //Ensure we run the death test in a threadsafe manner
    ::testing::FLAGS_gtest_death_test_style = "threadsafe";
    
    void* memBlock1 = mAllocator->InvokeAllocate(24, 4);
    EXPECT_NE(nullptr, memBlock1);
    
    //Allocate enough memory to breach the memory bounds of the allocator
    ASSERT_DEATH(mAllocator->InvokeAllocate(24, 4), "");
}

TEST_F(LinearAllocator_utest, Free)
{
    //Ensure we run the death test in a threadsafe manner
    ::testing::FLAGS_gtest_death_test_style = "threadsafe";
    
    void* memBlock1 = mAllocator->InvokeAllocate(24, 4);
    EXPECT_NE(nullptr, memBlock1);
    
    //Attempt to free memory using the Linear Allocator - This should assert.
    ASSERT_DEATH(mAllocator->InvokeFree(memBlock1), "");
}

TEST_F(LinearAllocator_utest, Clear)
{
    void* memBlock1 = mAllocator->InvokeAllocate(24, 4);
    EXPECT_NE(nullptr, memBlock1);
    EXPECT_EQ(24, mAllocator->GetBytesUsed());
    EXPECT_EQ(1, mAllocator->GetAllocations());
    
    //Clear the allocator
    mAllocator->InvokeClear();
    EXPECT_EQ(0, mAllocator->GetBytesUsed());
    EXPECT_EQ(0, mAllocator->GetAllocations());
}
