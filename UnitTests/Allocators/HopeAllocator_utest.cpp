//
//  HopeAllocator_utest.cpp
//  UnitTests
//
//  Created by Ben Hopewell on 27/10/18.
//  Copyright © 2018 Ben Hopewell. All rights reserved.
//

#include <gtest/gtest.h>
#include "Core/Memory/Allocators/HopeAllocator.h"

using namespace Hope::Core;
using namespace ::testing;

TEST(HopeAllocator, CalculateAlignAdjustment)
{
    u8 alignment = HopeAllocator::CalculateAlignAdjustment(4, 4);
    EXPECT_EQ(0, alignment);
    
    alignment = HopeAllocator::CalculateAlignAdjustment(5, 4);
    EXPECT_EQ(3, alignment);
    
    alignment = HopeAllocator::CalculateAlignAdjustment(6, 4);
    EXPECT_EQ(2, alignment);
    
    alignment = HopeAllocator::CalculateAlignAdjustment(7, 4);
    EXPECT_EQ(1, alignment);
    
    alignment = HopeAllocator::CalculateAlignAdjustment(8, 4);
    EXPECT_EQ(0, alignment);
}

TEST(HopeAllocator, CalculateAlignAdjustment_WithExtra)
{
    u8 alignment = HopeAllocator::CalculateAlignAdjustment(4, 4, 0);
    EXPECT_EQ(0, alignment);
    
    alignment = HopeAllocator::CalculateAlignAdjustment(4, 4, 1);
    EXPECT_EQ(4, alignment);
    
    alignment = HopeAllocator::CalculateAlignAdjustment(4, 4, 2);
    EXPECT_EQ(4, alignment);
    
    alignment = HopeAllocator::CalculateAlignAdjustment(4, 4, 3);
    EXPECT_EQ(4, alignment);
    
    alignment = HopeAllocator::CalculateAlignAdjustment(4, 4, 4);
    EXPECT_EQ(4, alignment);
    
    alignment = HopeAllocator::CalculateAlignAdjustment(4, 4, 5);
    EXPECT_EQ(8, alignment);
}
