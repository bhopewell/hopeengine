//
//  DynamicPoolAllocatorStub.h
//  HopeEngine
//
//  Created by Ben Hopewell on 04/05/2019.
//

#pragma once

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "Core/Memory/Allocators/DynamicPoolAllocator.h"

class DynamicPoolAllocatorStub : public Hope::Core::DynamicPoolAllocator
{
public:
    DynamicPoolAllocatorStub(const u32 objectBytes, const u32 alignment) :
    Hope::Core::DynamicPoolAllocator(objectBytes, alignment)
    {
        
    }
    
    u32 GetBytesUsed()
    {
        return mBytesUsed;
    }
    
    u32 GetAllocations()
    {
        return mAllocations;
    }
    
    u32 GetObjectBytes()
    {
        return mObjectBytes;
    }
    
    u32 GetTotalBytes()
    {
        return mTotalMemoryBytes;
    }
    
    u32 GetPoolAllocatorCount()
    {
        return mAllocators.size();
    }
    
    void* InvokeAllocateObject()
    {
        return DynamicPoolAllocator::AllocateObject();
    }
    
    void InvokeFree(void* mem)
    {
        DynamicPoolAllocator::Free(mem);
    }
    
    void InvokeClear()
    {
        DynamicPoolAllocator::Clear();
    }
    
    MOCK_METHOD0(AllocateObject, void*());
    MOCK_METHOD1(Free, void(void* mem));
    MOCK_METHOD0(Clear, void());
};
