//
//  StackAllocatorStub.h
//  UnitTests
//
//  Created by Ben Hopewell on 27/4/19.
//  Copyright © 2019 Ben Hopewell. All rights reserved.
//

#pragma once

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "Core/Memory/Allocators/StackAllocator.h"

class StackAllocatorStub : public Hope::Core::StackAllocator
{
public:
    StackAllocatorStub(const u32 totalMemBytes) :
        Hope::Core::StackAllocator(totalMemBytes)
    {
        
    }
    
    StackAllocatorStub(void* mem, const u32 totalMemBytes) :
        Hope::Core::StackAllocator(mem, totalMemBytes)
    {
        
    }
    
    u32 GetBytesUsed() const
    {
        return mBytesUsed;
    }
    
    u32 GetAllocations() const
    {
        return mAllocations;
    }
    
    u32 GetHeaderSize() const
    {
        return sizeof(StackAllocHeader);
    }
    
    void* InvokeAllocate(u32 memBytes, u32 alignment)
    {
        return StackAllocator::Allocate(memBytes, alignment);
    }
    
    void InvokeFree(void* mem)
    {
        StackAllocator::Free(mem);
    }
    
    void InvokeClear()
    {
        StackAllocator::Clear();
    }
    
    MOCK_METHOD2(Allocate, void*(u32 memBytes, u32 alignment));
    MOCK_METHOD1(Free, void(void* mem));
    MOCK_METHOD0(Clear, void());
};
