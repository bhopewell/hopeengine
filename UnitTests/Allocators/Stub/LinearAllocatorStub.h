//
//  LinearAllocatorStub.h
//  UnitTests
//
//  Created by Ben Hopewell on 8/3/19.
//  Copyright © 2019 Ben Hopewell. All rights reserved.
//

#pragma once

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "Core/Memory/Allocators/LinearAllocator.h"

class LinearAllocatorStub : public Hope::Core::LinearAllocator
{
public:
    LinearAllocatorStub(const u32 totalMemBytes) :
        Hope::Core::LinearAllocator(totalMemBytes)
    {
        
    }
    
    LinearAllocatorStub(void* mem, const u32 totalMemBytes) :
        Hope::Core::LinearAllocator(mem, totalMemBytes)
    {
        
    }
    
    u32 GetBytesUsed()
    {
        return mBytesUsed;
    }
    
    u32 GetAllocations()
    {
        return mAllocations;
    }
    
    void* InvokeAllocate(u32 memBytes, u32 alignment)
    {
        return LinearAllocator::Allocate(memBytes, alignment);
    }
    
    void InvokeFree(void* mem)
    {
        LinearAllocator::Free(mem);
    }
    
    void InvokeClear()
    {
        LinearAllocator::Clear();
    }
    
    MOCK_METHOD2(Allocate, void*(u32 memBytes, u32 alignment));
    MOCK_METHOD1(Free, void(void* mem));
    MOCK_METHOD0(Clear, void());
};
