//
//  PoolAllocatorStub.h
//  UnitTests
//
//  Created by Ben Hopewell on 01/05/2019.
//

#pragma once

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "Core/Memory/Allocators/PoolAllocator.h"

class PoolAllocatorStub : public Hope::Core::PoolAllocator
{
public:
    PoolAllocatorStub(const u32 totalMemBytes, const u32 objectBytes) :
    Hope::Core::PoolAllocator(totalMemBytes, objectBytes)
    {
        
    }
    
    PoolAllocatorStub(void* mem, const u32 totalMemBytes, const u32 objectBytes) :
    Hope::Core::PoolAllocator(mem, totalMemBytes, objectBytes)
    {
        
    }
    
    u32 GetBytesUsed()
    {
        return mBytesUsed;
    }
    
    u32 GetAllocations()
    {
        return mAllocations;
    }
    
    u32 GetObjectBytes()
    {
        return mObjectBytes;
    }
    
    u32 GetTotalBytes()
    {
        return mTotalMemoryBytes;
    }
    
    void* InvokeAllocate(u32 memBytes, u32 alignment)
    {
        return PoolAllocator::Allocate(memBytes, alignment);
    }
    
    void InvokeFree(void* mem)
    {
        PoolAllocator::Free(mem);
    }
    
    void InvokeClear()
    {
        PoolAllocator::Clear();
    }
    
    MOCK_METHOD2(Allocate, void*(u32 memBytes, u32 alignment));
    MOCK_METHOD1(Free, void(void* mem));
    MOCK_METHOD0(Clear, void());
};
