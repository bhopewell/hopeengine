//
//  StackAllocator_utest.cpp
//  UnitTests
//
//  Created by Ben Hopewell on 20/2/19.
//  Copyright © 2019 Ben Hopewell. All rights reserved.
//

#include <gtest/gtest.h>
#include "Stub/StackAllocatorStub.h"

using namespace ::testing;

class StackAllocator_utest : public Test
{
public:
    void SetUp()
    {
        mAllocator = new StackAllocatorStub(32);
    }
    
    void TearDown()
    {
        delete mAllocator;
    }
    
protected:
    StackAllocatorStub* mAllocator;
};

TEST_F(StackAllocator_utest, Allocate)
{
    const u32 headerSize = mAllocator->GetHeaderSize();
    ASSERT_EQ(4, headerSize);
    
    void* memBlock1 = mAllocator->InvokeAllocate(4, 4);
    ASSERT_NE(nullptr, memBlock1);
    
    void* memBlock2 = mAllocator->InvokeAllocate(8, 4);
    ASSERT_NE(nullptr, memBlock2);
    
    void* memBlock3 = mAllocator->InvokeAllocate(8, 4);
    ASSERT_NE(nullptr, memBlock3);
    
    // Remove 1x the header size as there should be 1 header between memBlock1 and memBlock2
    uintptr_t diff1 = (reinterpret_cast<uintptr_t>(memBlock2) - reinterpret_cast<uintptr_t>(memBlock1)) - headerSize;
    EXPECT_EQ(4, diff1);
    
    // Remove 2x the header size as there should be 2 headers between memBlock1 and memBlock3
    uintptr_t diff2 = (reinterpret_cast<uintptr_t>(memBlock3) - reinterpret_cast<uintptr_t>(memBlock1)) - (headerSize * 2);
    EXPECT_EQ(12, diff2);
}

TEST_F(StackAllocator_utest, AllocateWithPadding)
{
    const u32 headerSize = mAllocator->GetHeaderSize();
    ASSERT_EQ(4, headerSize);
    
    void* memBlock1 = mAllocator->InvokeAllocate(3, 4); //Requires 1 bit padding
    ASSERT_NE(nullptr, memBlock1);
    
    void* memBlock2 = mAllocator->InvokeAllocate(5, 4); //Requires 3 bits padding
    ASSERT_NE(nullptr, memBlock2);
    
    void* memBlock3 = mAllocator->InvokeAllocate(3, 4); //Requires 1 bit padding
    ASSERT_NE(nullptr, memBlock3);
    
    // Remove 1x the header size as there should be 1 header between memBlock1 and memBlock2
    uintptr_t diff1 = (reinterpret_cast<uintptr_t>(memBlock2) - reinterpret_cast<uintptr_t>(memBlock1)) - headerSize;
    EXPECT_EQ(4, diff1);
    
    // Remove 2x the header size as there should be 2 headers between memBlock1 and memBlock3
    uintptr_t diff2 = (reinterpret_cast<uintptr_t>(memBlock3) - reinterpret_cast<uintptr_t>(memBlock1)) - (headerSize * 2);
    EXPECT_EQ(12, diff2);
}

TEST_F(StackAllocator_utest, AllocateZeroBytes)
{
    //Ensure we run the death test in a threadsafe manner
    ::testing::FLAGS_gtest_death_test_style = "threadsafe";
    
    //Allocate 0 bytes
    ASSERT_DEATH(mAllocator->InvokeAllocate(0, 4), "");
}

TEST_F(StackAllocator_utest, AllocatePastBounds)
{
    //Ensure we run the death test in a threadsafe manner
    ::testing::FLAGS_gtest_death_test_style = "threadsafe";
    
    void* memBlock1 = mAllocator->InvokeAllocate(24, 4);
    EXPECT_NE(nullptr, memBlock1);
    
    //Allocate enough memory to breach the memory bounds of the allocator
    ASSERT_DEATH(mAllocator->InvokeAllocate(24, 4), "");
}

TEST_F(StackAllocator_utest, Free)
{
    const u32 headerSize = mAllocator->GetHeaderSize();
    ASSERT_EQ(4, headerSize);
    
    void* memBlock1 = mAllocator->InvokeAllocate(8, 4);
    void* memBlock2 = mAllocator->InvokeAllocate(4, 4);
    void* memBlock3 = mAllocator->InvokeAllocate(4, 4);
    
    ASSERT_NE(nullptr, memBlock1);
    ASSERT_NE(nullptr, memBlock2);
    ASSERT_NE(nullptr, memBlock3);
    
    EXPECT_EQ(3, mAllocator->GetAllocations());
    EXPECT_EQ(16 + (headerSize * mAllocator->GetAllocations()), mAllocator->GetBytesUsed());
    
    mAllocator->InvokeFree(memBlock3);
    EXPECT_EQ(2, mAllocator->GetAllocations());
    EXPECT_EQ(12 + (headerSize * mAllocator->GetAllocations()), mAllocator->GetBytesUsed());
    
    mAllocator->InvokeFree(memBlock2);
    EXPECT_EQ(1, mAllocator->GetAllocations());
    EXPECT_EQ(8 + (headerSize * mAllocator->GetAllocations()), mAllocator->GetBytesUsed());
    
    mAllocator->InvokeFree(memBlock1);
    EXPECT_EQ(0, mAllocator->GetAllocations());
    EXPECT_EQ(0, mAllocator->GetBytesUsed());
}

TEST_F(StackAllocator_utest, FreeWithPadding)
{
    // NOTE: BytesUsed = allocAmount + headerSize(4) + padding
    void* memBlock1 = mAllocator->InvokeAllocate(3, 4); // No padding necessary (BytesUsed=7)
    void* memBlock2 = mAllocator->InvokeAllocate(3, 4); // 1 byte padding (BytesUsed=15)
    void* memBlock3 = mAllocator->InvokeAllocate(3, 4); // 1 byte padding (BytesUsed=23)
    
    ASSERT_NE(nullptr, memBlock1);
    ASSERT_NE(nullptr, memBlock2);
    ASSERT_NE(nullptr, memBlock3);
    
    EXPECT_EQ(3, mAllocator->GetAllocations());
    EXPECT_EQ(23, mAllocator->GetBytesUsed());
    
    mAllocator->InvokeFree(memBlock3);
    EXPECT_EQ(2, mAllocator->GetAllocations());
    EXPECT_EQ(15, mAllocator->GetBytesUsed());
    
    mAllocator->InvokeFree(memBlock2);
    EXPECT_EQ(1, mAllocator->GetAllocations());
    EXPECT_EQ(7, mAllocator->GetBytesUsed());
    
    mAllocator->InvokeFree(memBlock1);
    EXPECT_EQ(0, mAllocator->GetAllocations());
    EXPECT_EQ(0, mAllocator->GetBytesUsed());
}

TEST_F(StackAllocator_utest, FreeInvalidOrder)
{
    //Ensure we run the death test in a threadsafe manner
    ::testing::FLAGS_gtest_death_test_style = "threadsafe";
    
    const u32 headerSize = mAllocator->GetHeaderSize();
    
    void* memBlock1 = mAllocator->InvokeAllocate(8, 4);
    void* memBlock2 = mAllocator->InvokeAllocate(4, 4);
    void* memBlock3 = mAllocator->InvokeAllocate(4, 4);
    
    ASSERT_NE(nullptr, memBlock1);
    ASSERT_NE(nullptr, memBlock2);
    ASSERT_NE(nullptr, memBlock3);
    
    EXPECT_EQ(3, mAllocator->GetAllocations());
    EXPECT_EQ(16 + (headerSize * mAllocator->GetAllocations()), mAllocator->GetBytesUsed());
    
    // Free memory that should not be freeable at this moment
    ASSERT_DEATH(mAllocator->InvokeFree(memBlock2), "");
}

TEST_F(StackAllocator_utest, Clear)
{
    const u32 headerSize = mAllocator->GetHeaderSize();
    
    void* memBlock1 = mAllocator->InvokeAllocate(24, 4);
    EXPECT_NE(nullptr, memBlock1);
    EXPECT_EQ(24 + headerSize, mAllocator->GetBytesUsed()); //NOTE: Add header size to get the correct number of bytes used
    EXPECT_EQ(1, mAllocator->GetAllocations());
    
    //Clear the allocator
    mAllocator->InvokeClear();
    EXPECT_EQ(0, mAllocator->GetBytesUsed());
    EXPECT_EQ(0, mAllocator->GetAllocations());
}
