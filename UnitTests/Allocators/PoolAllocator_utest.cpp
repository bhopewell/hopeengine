//
//  PoolAllocator_utest.cpp
//  UnitTests
//
//  Created by Ben Hopewell on 20/2/19.
//  Copyright © 2019 Ben Hopewell. All rights reserved.
//

#include <gtest/gtest.h>
#include "Stub/PoolAllocatorStub.h"

using namespace ::testing;

class PoolAllocator_utest : public Test
{
public:
    void SetUp()
    {
        mAllocator = new PoolAllocatorStub(32, 8);
        mAllocator_Padding = new PoolAllocatorStub(32, 5);
    }
    
    void TearDown()
    {
        delete mAllocator_Padding;
        delete mAllocator;
    }
    
protected:
    u32 AllocSize()
    {
        return 8;
    }
    
protected:
    PoolAllocatorStub* mAllocator;
    PoolAllocatorStub* mAllocator_Padding;
};

TEST_F(PoolAllocator_utest, Allocate)
{
    void* memBlock1 = mAllocator->InvokeAllocate(AllocSize(), 4);
    ASSERT_NE(nullptr, memBlock1);
    
    void* memBlock2 = mAllocator->InvokeAllocate(AllocSize(), 4);
    ASSERT_NE(nullptr, memBlock2);
    
    void* memBlock3 = mAllocator->InvokeAllocate(AllocSize(), 4);
    ASSERT_NE(nullptr, memBlock3);
    
    uintptr_t diff1 = (reinterpret_cast<uintptr_t>(memBlock2) - reinterpret_cast<uintptr_t>(memBlock1));
    EXPECT_EQ(AllocSize(), diff1);
    
    uintptr_t diff2 = (reinterpret_cast<uintptr_t>(memBlock3) - reinterpret_cast<uintptr_t>(memBlock1));
    EXPECT_EQ(AllocSize() * 2, diff2);
}

// Test that allocating, freeing and then reallocating works as expected
TEST_F(PoolAllocator_utest, AllocateFreeAllocate)
{
    void* memBlock1 = mAllocator->InvokeAllocate(AllocSize(), 4);
    ASSERT_NE(nullptr, memBlock1);
    
    void* memBlock2 = mAllocator->InvokeAllocate(AllocSize(), 4);
    ASSERT_NE(nullptr, memBlock2);
    
    // Free the first allocated block of memory
    mAllocator->InvokeFree(memBlock1);
    ASSERT_EQ(1, mAllocator->GetAllocations());
    EXPECT_EQ(AllocSize(), mAllocator->GetBytesUsed());
    
    // This should allocate to the same block as memBlock1
    void* memBlock3 = mAllocator->InvokeAllocate(AllocSize(), 4);
    ASSERT_NE(nullptr, memBlock3);
    
    // This will allocate to the 3rd block from memStart
    void* memBlock4 = mAllocator->InvokeAllocate(AllocSize(), 4);
    ASSERT_NE(nullptr, memBlock3);
    
    uintptr_t diff1 = (reinterpret_cast<uintptr_t>(memBlock2) - reinterpret_cast<uintptr_t>(memBlock3));
    EXPECT_EQ(AllocSize(), diff1);
    
    uintptr_t diff2 = (reinterpret_cast<uintptr_t>(memBlock4) - reinterpret_cast<uintptr_t>(memBlock3));
    EXPECT_EQ(AllocSize() * 2, diff2);
}

TEST_F(PoolAllocator_utest, AllocateWithPadding)
{
    void* memBlock1 = mAllocator_Padding->InvokeAllocate(5, 4); //Requires 3 bits padding
    ASSERT_NE(nullptr, memBlock1);
    
    void* memBlock2 = mAllocator_Padding->InvokeAllocate(5, 4); //Requires 3 bits padding
    ASSERT_NE(nullptr, memBlock2);
    
    void* memBlock3 = mAllocator_Padding->InvokeAllocate(5, 4); //Requires 3 bits padding
    ASSERT_NE(nullptr, memBlock3);
    
    // Remove 1x the header size as there should be 1 header between memBlock1 and memBlock2
    uintptr_t diff1 = (reinterpret_cast<uintptr_t>(memBlock2) - reinterpret_cast<uintptr_t>(memBlock1));
    EXPECT_EQ(8, diff1);
    
    // Remove 2x the header size as there should be 2 headers between memBlock1 and memBlock3
    uintptr_t diff2 = (reinterpret_cast<uintptr_t>(memBlock3) - reinterpret_cast<uintptr_t>(memBlock1));
    EXPECT_EQ(16, diff2);
}

TEST_F(PoolAllocator_utest, AllocateZeroBytes)
{
    //Ensure we run the death test in a threadsafe manner
    ::testing::FLAGS_gtest_death_test_style = "threadsafe";
    
    //Allocate 0 bytes
    ASSERT_DEATH(mAllocator->InvokeAllocate(0, 4), "");
}

TEST_F(PoolAllocator_utest, AllocatePastBounds)
{
    //Ensure we run the death test in a threadsafe manner
    ::testing::FLAGS_gtest_death_test_style = "threadsafe";
    
    //Allocate as many blocks as we can given an object size of 4
    for(u32 i = 0; i < mAllocator->GetTotalBytes(); i += AllocSize())
    {
        void* alloc = mAllocator->InvokeAllocate(AllocSize(), 4);
        EXPECT_NE(nullptr, alloc);
    }
    
    ASSERT_EQ(mAllocator->GetTotalBytes(), mAllocator->GetBytesUsed());
    
    //Allocate 1 too many object blocks causing it to breach the memory bounds
    ASSERT_DEATH(mAllocator->InvokeAllocate(AllocSize(), 4), "");
}

TEST_F(PoolAllocator_utest, AllocateInvalidObjectSize)
{
    //Ensure we run the death test in a threadsafe manner
    ::testing::FLAGS_gtest_death_test_style = "threadsafe";
    
    void* memBlock1 = mAllocator->InvokeAllocate(AllocSize(), 4);
    EXPECT_NE(nullptr, memBlock1);
    
    //Allocate twice the object size - this is invalid for the Pool Allocator so it should assert
    ASSERT_DEATH(mAllocator->InvokeAllocate(AllocSize() * 2, 4), "");
}

TEST_F(PoolAllocator_utest, Free)
{
    void* memBlock1 = mAllocator->InvokeAllocate(AllocSize(), 4);
    void* memBlock2 = mAllocator->InvokeAllocate(AllocSize(), 4);
    void* memBlock3 = mAllocator->InvokeAllocate(AllocSize(), 4);
    
    ASSERT_NE(nullptr, memBlock1);
    ASSERT_NE(nullptr, memBlock2);
    ASSERT_NE(nullptr, memBlock3);
    
    // Free everything in an inverse manner
    EXPECT_EQ(3, mAllocator->GetAllocations());
    EXPECT_EQ(AllocSize() * 3, mAllocator->GetBytesUsed());
    
    mAllocator->InvokeFree(memBlock3);
    EXPECT_EQ(2, mAllocator->GetAllocations());
    EXPECT_EQ(AllocSize() * 2, mAllocator->GetBytesUsed());
    
    mAllocator->InvokeFree(memBlock2);
    EXPECT_EQ(1, mAllocator->GetAllocations());
    EXPECT_EQ(AllocSize(), mAllocator->GetBytesUsed());
    
    mAllocator->InvokeFree(memBlock1);
    EXPECT_EQ(0, mAllocator->GetAllocations());
    EXPECT_EQ(0, mAllocator->GetBytesUsed());
}

TEST_F(PoolAllocator_utest, Free2)
{
    void* memBlock1 = mAllocator->InvokeAllocate(AllocSize(), 4);
    void* memBlock2 = mAllocator->InvokeAllocate(AllocSize(), 4);
    void* memBlock3 = mAllocator->InvokeAllocate(AllocSize(), 4);
    
    ASSERT_NE(nullptr, memBlock1);
    ASSERT_NE(nullptr, memBlock2);
    ASSERT_NE(nullptr, memBlock3);
    
    // Free everything in a random manner
    EXPECT_EQ(3, mAllocator->GetAllocations());
    EXPECT_EQ(AllocSize() * 3, mAllocator->GetBytesUsed());
    
    mAllocator->InvokeFree(memBlock1);
    EXPECT_EQ(2, mAllocator->GetAllocations());
    EXPECT_EQ(AllocSize() * 2, mAllocator->GetBytesUsed());
    
    mAllocator->InvokeFree(memBlock3);
    EXPECT_EQ(1, mAllocator->GetAllocations());
    EXPECT_EQ(AllocSize(), mAllocator->GetBytesUsed());
    
    mAllocator->InvokeFree(memBlock2);
    EXPECT_EQ(0, mAllocator->GetAllocations());
    EXPECT_EQ(0, mAllocator->GetBytesUsed());
}

TEST_F(PoolAllocator_utest, FreeWithPadding)
{
    void* memBlock1 = mAllocator_Padding->InvokeAllocate(5, 4);
    void* memBlock2 = mAllocator_Padding->InvokeAllocate(5, 4);
    void* memBlock3 = mAllocator_Padding->InvokeAllocate(5, 4);
    
    ASSERT_NE(nullptr, memBlock1);
    ASSERT_NE(nullptr, memBlock2);
    ASSERT_NE(nullptr, memBlock3);
    
    EXPECT_EQ(3, mAllocator_Padding->GetAllocations());
    EXPECT_EQ(24, mAllocator_Padding->GetBytesUsed()); // Takes padding into account
    
    mAllocator_Padding->InvokeFree(memBlock3);
    EXPECT_EQ(2, mAllocator_Padding->GetAllocations());
    EXPECT_EQ(16, mAllocator_Padding->GetBytesUsed()); // Takes padding into account
    
    mAllocator_Padding->InvokeFree(memBlock2);
    EXPECT_EQ(1, mAllocator_Padding->GetAllocations());
    EXPECT_EQ(8, mAllocator_Padding->GetBytesUsed()); // Takes padding into account
    
    mAllocator_Padding->InvokeFree(memBlock1);
    EXPECT_EQ(0, mAllocator_Padding->GetAllocations());
    EXPECT_EQ(0, mAllocator_Padding->GetBytesUsed());
}

TEST_F(PoolAllocator_utest, Clear)
{
    void* memBlock1 = mAllocator->InvokeAllocate(AllocSize(), 4);
    EXPECT_NE(nullptr, memBlock1);
    EXPECT_EQ(AllocSize(), mAllocator->GetBytesUsed());
    EXPECT_EQ(1, mAllocator->GetAllocations());
    
    //Clear the allocator
    mAllocator->InvokeClear();
    EXPECT_EQ(0, mAllocator->GetBytesUsed());
    EXPECT_EQ(0, mAllocator->GetAllocations());
}

TEST_F(PoolAllocator_utest, ClearWithPadding)
{
    void* memBlock1 = mAllocator_Padding->InvokeAllocate(5, 4);
    EXPECT_NE(nullptr, memBlock1);
    EXPECT_EQ(8, mAllocator_Padding->GetBytesUsed());
    EXPECT_EQ(1, mAllocator_Padding->GetAllocations());
    
    //Clear the allocator
    mAllocator_Padding->InvokeClear();
    EXPECT_EQ(0, mAllocator_Padding->GetBytesUsed());
    EXPECT_EQ(0, mAllocator_Padding->GetAllocations());
}

TEST_F(PoolAllocator_utest, Iterator_BeginEqualsEndWhenEmpty)
{
    PoolAllocatorStub::PoolIterator begin = mAllocator->begin();
    PoolAllocatorStub::PoolIterator end = mAllocator->end();
    
    EXPECT_TRUE(begin == end);
}


TEST_F(PoolAllocator_utest, Iterator_Begin)
{
    void* memBlock1 = mAllocator->InvokeAllocate(AllocSize(), 4);
    EXPECT_NE(nullptr, memBlock1);
    EXPECT_EQ(8, mAllocator->GetBytesUsed());
    EXPECT_EQ(1, mAllocator->GetAllocations());
    
    // Get a begin iterator
    PoolAllocatorStub::PoolIterator it = mAllocator->begin();
    
    ASSERT_NE(it, mAllocator->end());
    EXPECT_EQ(memBlock1, *it);
}

TEST_F(PoolAllocator_utest, Iterator_Iterate)
{
    void* memBlock1 = mAllocator->InvokeAllocate(AllocSize(), 4);
    void* memBlock2 = mAllocator->InvokeAllocate(AllocSize(), 4);
    void* memBlock3 = mAllocator->InvokeAllocate(AllocSize(), 4);
    ASSERT_NE(nullptr, memBlock1);
    ASSERT_NE(nullptr, memBlock2);
    ASSERT_NE(nullptr, memBlock3);
    
    // Get a begin iterator
    PoolAllocatorStub::PoolIterator it = mAllocator->begin();
    
    ASSERT_NE(it, mAllocator->end());
    EXPECT_EQ(memBlock1, *it);
    
    ++it;
    EXPECT_EQ(memBlock2, *it);
    
    ++it;
    EXPECT_EQ(memBlock3, *it);
    
    ++it;
    EXPECT_EQ(mAllocator->end(), it);
}

TEST_F(PoolAllocator_utest, Iterator_IteratePastEnd)
{
    //Ensure we run the death test in a threadsafe manner
    ::testing::FLAGS_gtest_death_test_style = "threadsafe";
    
    void* memBlock1 = mAllocator->InvokeAllocate(AllocSize(), 4);
    void* memBlock2 = mAllocator->InvokeAllocate(AllocSize(), 4);
    ASSERT_NE(nullptr, memBlock1);
    ASSERT_NE(nullptr, memBlock2);
    
    // Get a begin iterator
    PoolAllocatorStub::PoolIterator it = mAllocator->begin();
    
    ASSERT_NE(it, mAllocator->end());
    EXPECT_EQ(memBlock1, *it);
    
    ++it;
    EXPECT_EQ(memBlock2, *it);
    
    // End of collection - Should not crash on this increment
    ++it;
    EXPECT_EQ(mAllocator->end(), it);
    
    // This will attempt to increment past the end of the collection - The iterator should throw an assert in this case
    ASSERT_DEATH(++it, "");
}
