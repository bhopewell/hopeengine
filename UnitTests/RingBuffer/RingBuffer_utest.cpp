#include <gtest/gtest.h>
#include "Core/Containers/RingBuffer.h"

using namespace Hope::Core;
using namespace ::testing;

class RingBuffer_utest : public Test
{
public:
    void SetUp()
    {
        mBuffer = new RingBuffer<u32>(10);
    }
    
    void TearDown()
    {
        mBuffer->Clear();
        delete mBuffer;
    }
    
protected:
    RingBuffer<u32>* mBuffer;
};

TEST_F(RingBuffer_utest, IsEmpty)
{
    EXPECT_EQ(0, mBuffer->GetSize());
	EXPECT_TRUE(mBuffer->IsEmpty());
}

TEST_F(RingBuffer_utest, IsFull)
{
    for(uint i = 0; i < mBuffer->GetTotalSize(); ++i)
        mBuffer->Push(i);

	//Ensure the Full call returns true when the buffer is full
	EXPECT_TRUE(mBuffer->IsFull());
    EXPECT_EQ(mBuffer->GetTotalSize(), mBuffer->GetSize());
	EXPECT_FALSE(mBuffer->Push(10));
}

TEST_F(RingBuffer_utest, Push)
{
	mBuffer->Push(5);

	EXPECT_EQ(1, mBuffer->GetSize());
	EXPECT_FALSE(mBuffer->IsEmpty());
}

TEST_F(RingBuffer_utest, PushOrReplace)
{
    //Initialise the buffer
    for(uint i = 0; i < mBuffer->GetTotalSize(); ++i)
        mBuffer->PushOrReplace(i);
    
    //Ensure the buffer is full
    EXPECT_TRUE(mBuffer->IsFull());
    
    //Replace the first element in the buffer
    mBuffer->PushOrReplace(mBuffer->GetTotalSize() + 1);
    
    //Verify the buffer overwrote the first element.
    EXPECT_EQ(1, mBuffer->Peek());
    
    //Ensure the last element is correct
    u32 lastElement = (*mBuffer)[mBuffer->GetTotalSize() - 1];
    EXPECT_EQ(mBuffer->GetTotalSize() + 1, lastElement);
}

TEST_F(RingBuffer_utest, Peek)
{
    mBuffer->Push(1);
    mBuffer->Push(2);
    
    EXPECT_EQ(1, mBuffer->Peek());
    EXPECT_EQ(1, mBuffer->Pop());
    EXPECT_EQ(2, mBuffer->Peek());
}

TEST_F(RingBuffer_utest, SubscriptOperator)
{
    mBuffer->Push(0);
    mBuffer->Push(1);
    
    //Verify the subscript operator works when retrieving the elements.
    EXPECT_EQ(0, (*mBuffer)[0]);
    EXPECT_EQ(1, (*mBuffer)[1]);
    
    //Ensure the subscript operator works after popping elements.
    mBuffer->Pop();
    EXPECT_EQ(1, (*mBuffer)[0]);
    
    //NOTE: No need to test setting via the subscript operator as that is unsupported.
}

TEST_F(RingBuffer_utest, Pop)
{
	mBuffer->Push(5);

	u32 val = mBuffer->Pop();

	EXPECT_EQ(5, val);
	EXPECT_TRUE(mBuffer->IsEmpty());
}

TEST_F(RingBuffer_utest, Clear)
{
    for(uint i = 0; i < mBuffer->GetTotalSize(); ++i)
        mBuffer->Push(i);
    
    EXPECT_TRUE(mBuffer->IsFull());
    
    //Clear the buffer and ensure the new size is 0.
    mBuffer->Clear();
    EXPECT_EQ(0, mBuffer->GetSize());
    EXPECT_TRUE(mBuffer->IsEmpty());
}
