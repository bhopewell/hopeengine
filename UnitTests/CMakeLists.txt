# -------------------------------------------------
# VERSION
# -------------------------------------------------
cmake_minimum_required(VERSION 3.12)

# -------------------------------------------------
# INCLUDE FILES
# -------------------------------------------------
include("${CMAKE_CONFIG_DIRECTORY}/source_grouping.cmake")
include_directories(
	${CMAKE_CURRENT_SOURCE_DIR}
	${EXTERNAL_LIBS_DIR}/GLM
	${EXTERNAL_LIBS_DIR}/googletest-1.8.1/googletest/include
	${EXTERNAL_LIBS_DIR}/googletest-1.8.1/googlemock/include
)

# -------------------------------------------------
# SOURCE FILES
# -------------------------------------------------
set(UTEST_SOURCES
	"main.cpp"

	"Allocators/Stub/LinearAllocatorStub.h"
	"Allocators/Stub/StackAllocatorStub.h"
	"Allocators/Stub/PoolAllocatorStub.h"
	"Allocators/Stub/DynamicPoolAllocatorStub.h"
	"Allocators/HopeAllocator_utest.cpp"
	"Allocators/LinearAllocator_utest.cpp"
	"Allocators/StackAllocator_utest.cpp"
	"Allocators/PoolAllocator_utest.cpp"
	"Allocators/DynamicPoolAllocator_utest.cpp"

	"ECS/Stub/EntityStub.h"
	"ECS/Stub/ComponentStub.h"
	"ECS/Entity_utest.cpp"
	"ECS/EntityManager_utest.cpp"
	"ECS/ComponentManager_utest.cpp"

	"RingBuffer/RingBuffer_utest.cpp"
)

# -------------------------------------------------
# GROUPS
# -------------------------------------------------
assign_groups(UTEST_SOURCES)

# -------------------------------------------------
# GENERATE PROJECT TARGETS
# -------------------------------------------------
add_executable(UnitTests ${UTEST_SOURCES})

# -------------------------------------------------
# LINK PROJECT DEPENDENCIES
# -------------------------------------------------
target_link_libraries(UnitTests ${ENGINE_LIB_NAME})

# -------------------------------------------------
# EXTERNALS
# -------------------------------------------------
target_link_libraries(UnitTests gtest gmock gtest_main)