#pragma once

#include "IInputManager.h"

struct SDL_KeyboardEvent;
struct SDL_MouseButtonEvent;
struct SDL_MouseMotionEvent;
struct SDL_MouseWheelEvent;

namespace Hope {

	class SDLInputManager : public IInputManager
	{
	public:
		explicit SDLInputManager();
		~SDLInputManager();

		virtual bool Initialise(Core::IWindow* window) override;
        virtual void PreTick(f32 dt) override { HOPE_UNUSED(dt); }
		virtual void Tick(f32 dt) override;
        virtual void PostTick(f32 dt) override { HOPE_UNUSED(dt); }
		virtual void Shutdown() override;
        
	private:
		void ProcessKeyDown(const SDL_KeyboardEvent& keyEvent);
		void ProcessKeyUp(const SDL_KeyboardEvent& keyEvent);

		void ProcessMouseMove(const SDL_MouseMotionEvent& mouseEvent);
		void ProcessMouseDown(const SDL_MouseButtonEvent& mouseEvent);
		void ProcessMouseUp(const SDL_MouseButtonEvent& mouseEvent);
		void ProcessMouseScroll(const SDL_MouseWheelEvent& mouseEvent);
	};	
}
