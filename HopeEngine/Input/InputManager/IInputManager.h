#pragma once
#include <map>
#include <vector>
#include <functional>

#include "Hope.h"
#include "InputCore.h"

#include "System/ISystem.h"

namespace Hope {
    class IInputComponent;

	namespace Core {
		class IWindow;
	}

    class IInputManager : public EngineCore::ISystem
	{
	public:
        explicit IInputManager();
		virtual ~IInputManager();
        
		virtual bool Initialise(Core::IWindow* window) = 0;
        virtual void PreTick(f32 dt) = 0;
		virtual void Tick(f32 dt) = 0;
        virtual void PostTick(f32 dt) = 0;
		virtual void Shutdown() = 0;

		//Input bindings
		struct KeyBinding& BindKey(const Key& key, InputEvent keyEvent, IInputComponent* inputComponent, std::function<void(IInputComponent*)> func); //Used for keyboard bindings.
		struct MouseBinding& BindMouseButton(const MouseButton& button, const InputEvent buttonEvent, IInputComponent* inputComponent, std::function<void(IInputComponent*)> func); //Used for mouse button bindings
		struct ActionBinding& BindAction(const std::string& actionName, const InputEvent keyEvent, IInputComponent* inputComponent, std::function<void(IInputComponent*)> func); //Used for user defined bindings
		struct AxisBinding& BindAxis(const std::string& axisName, IInputComponent* inputComponent, std::function<void(IInputComponent*, f32)> func); //Used for mouse/joy move bindings

	protected:
		//Store the input map here.

		//TODO(Ben): These can probably be multimaps.
		std::vector<struct KeyBinding> KeyBindings;
		std::vector<struct MouseBinding> MouseBindings;
		std::vector<struct ActionBinding> ActionBindings;
		std::vector<struct AxisBinding> AxisBindings;
	};
}
