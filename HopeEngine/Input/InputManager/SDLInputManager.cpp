#include <iostream>
#include <algorithm>

#ifdef _WIN32
#include <SDL2/include/SDL.h>
#elif __APPLE__
#include "SDL.h"
#elif __linux__
#endif

#include "InputCore.h"
#include "InputBinding.h"
#include "InputManager/SDLInputManager.h"

using namespace Hope;

SDLInputManager::SDLInputManager()
	: IInputManager()
{
    
}

SDLInputManager::~SDLInputManager()
{

}

bool SDLInputManager::Initialise(Core::IWindow* window)
{
    HOPE_UNUSED(window)
    
	//Load the input map
	//TODO(Ben): Add input map support
	//TODO(Ben): Once input map supported, on key events, find the input event via the VirtualKeyMap object and publish that event to a message queue.
    
    return true;
}

//TODO(Ben): Perhaps look into using an input system manager that runs on system events? What would be the pros and cons?
void SDLInputManager::Tick(f32 dt)
{
    HOPE_UNUSED(dt);
    
	SDL_Event sdlEvent;
	while(SDL_PollEvent(&sdlEvent) != 0)
	{
		switch (sdlEvent.type)
		{
		case SDL_QUIT:
			break;
		case SDL_KEYUP:
			ProcessKeyUp(sdlEvent.key);
			break;
		case SDL_KEYDOWN:
			ProcessKeyDown(sdlEvent.key);
			break;
		case SDL_MOUSEBUTTONDOWN:
			ProcessMouseDown(sdlEvent.button);
			break;
		case SDL_MOUSEBUTTONUP:
			ProcessMouseUp(sdlEvent.button);
			break;
		case SDL_MOUSEMOTION:
			ProcessMouseMove(sdlEvent.motion);
			break;
		case SDL_MOUSEWHEEL:
			ProcessMouseScroll(sdlEvent.wheel);
			break;
		}
	}
}

void SDLInputManager::Shutdown()
{
	//Unload the input map
}

void SDLInputManager::ProcessKeyDown(const SDL_KeyboardEvent& keyEvent)
{
	const std::unordered_map<u32, const Key>& virtualKeyMap = Keys::GetInputKeys();
	
	//Protect against publishing events for keys that we don't support
	std::unordered_map<u32, const Key>::const_iterator keyIter = virtualKeyMap.find(keyEvent.keysym.sym);
	if (keyIter == virtualKeyMap.end())
		return;

	if (keyEvent.repeat != 0)
	{
		//Publish Repeat event
		for (const KeyBinding& binding : KeyBindings)
		{
			if (keyIter->second == binding.Key() && binding.Event() == InputEvent::REPEAT)
			{
				binding.Execute();
			}
		}

		//TODO(Ben): Implement Action Bindings
	}
	else
	{
		//Publish KeyDown event
		for (const KeyBinding& binding : KeyBindings)
		{
			if (keyIter->second == binding.Key() && binding.Event() == InputEvent::PRESSED)
			{
				binding.Execute();
			}
		}

		//TODO(Ben): Implement Action Bindings
	}
}

void SDLInputManager::ProcessKeyUp(const SDL_KeyboardEvent& keyEvent)
{
	const std::unordered_map<u32, const Key>& virtualKeyMap = Keys::GetInputKeys();
	//Protect against publishing events for keys that we don't support
	std::unordered_map<u32, const Key>::const_iterator keyIter = virtualKeyMap.find(keyEvent.keysym.sym);
	if (keyIter == virtualKeyMap.end())
		return;

	//Publish KeyUp event
	for (const KeyBinding& binding : KeyBindings)
	{
		if (keyIter->second == binding.Key() && binding.Event() == InputEvent::RELEASED)
		{
			binding.Execute();
		}
	}

	//TODO(Ben): Implement Action Bindings
}

void SDLInputManager::ProcessMouseDown(const SDL_MouseButtonEvent& mouseEvent)
{
	//Publish the MouseDown event
	for (const MouseBinding& binding : MouseBindings)
	{
		if ((u8)binding.Button() == mouseEvent.button && binding.Event() == InputEvent::PRESSED) //TODO(Ben): Avoid the casting if possible.
		{
			binding.Execute();
		}
	}
}

void SDLInputManager::ProcessMouseUp(const SDL_MouseButtonEvent& mouseEvent)
{
	//Publish the MouseUp event
	for (const MouseBinding& binding : MouseBindings)
	{
		if ((u8)binding.Button() == mouseEvent.button && binding.Event() == InputEvent::RELEASED) //TODO(Ben): Avoid the casting if possible.
		{
			binding.Execute();
		}
	}
}

//TODO(Ben): Might be worth re-thinking the axis bindings maybe
void SDLInputManager::ProcessMouseMove(const SDL_MouseMotionEvent& mouseEvent)
{
	//Publish the MouseMove event
	std::cout << "Mouse Move: xrel=" << mouseEvent.xrel << " yrel=" << mouseEvent.yrel << std::endl;

    //Handle the mouse x movement
    if(mouseEvent.xrel != 0)
    {
        auto mouseXIt = std::find_if(AxisBindings.begin(), AxisBindings.end(), [](const AxisBinding& binding){ return binding.Axis() == MouseAxis::MouseX; });
        if(mouseXIt != AxisBindings.end())
        {
            mouseXIt->ExecuteWithValue((f32)mouseEvent.xrel);
        }
    }
    
    //Handle the mouse y movement
    if(mouseEvent.yrel != 0)
    {
        auto mouseYIt = std::find_if(AxisBindings.begin(), AxisBindings.end(), [](const AxisBinding& binding){ return binding.Axis() == MouseAxis::MouseY; });
        if(mouseYIt != AxisBindings.end())
        {
			mouseYIt->ExecuteWithValue((f32)mouseEvent.yrel);
        }
    }
}

void SDLInputManager::ProcessMouseScroll(const SDL_MouseWheelEvent& mouseEvent)
{
	//Publish the MouseScroll event
	std::cout << "Mouse Wheel: y=" << mouseEvent.y << std::endl;

    //Handle the mouse scroll
    if(mouseEvent.y != 0)
    {
        auto wheelIt = std::find_if(AxisBindings.begin(), AxisBindings.end(), [](const AxisBinding& binding){ return binding.Axis() == MouseAxis::MouseScroll; });
        if(wheelIt != AxisBindings.end())
        {
			wheelIt->ExecuteWithValue((f32)mouseEvent.y);
        }
    }
}
