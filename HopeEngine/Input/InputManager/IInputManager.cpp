#include <assert.h>

#include "InputCore.h"
#include "InputBinding.h"
#include "InputManager/SDLInputManager.h"

using namespace Hope;

IInputManager::IInputManager() :
EngineCore::ISystem(EngineCore::ESystemPriority::EHighest),
KeyBindings(),
MouseBindings(),
ActionBindings(),
AxisBindings()
{
    Keys::Initialise();
}

IInputManager::~IInputManager()
{
}

//Used for keyboard bindings.
KeyBinding& IInputManager::BindKey(const Key& key, InputEvent keyEvent, IInputComponent* inputComponent, std::function<void(IInputComponent*)> func)
{
	KeyBinding keyBind(key, keyEvent);
    keyBind.Bind(inputComponent, func);
	KeyBindings.push_back(keyBind);

	return KeyBindings.back();
}

//Used for mouse button bindings
MouseBinding& IInputManager::BindMouseButton(const MouseButton& button, InputEvent buttonEvent, IInputComponent* inputComponent, std::function<void(IInputComponent*)> func)
{
	MouseBinding mouseBind(button, buttonEvent);
    mouseBind.Bind(inputComponent, func);
	MouseBindings.push_back(mouseBind);

	return MouseBindings.back();
}

//Used for user defined bindings
ActionBinding& IInputManager::BindAction(const std::string& actionName, InputEvent keyEvent, IInputComponent* inputComponent, std::function<void(IInputComponent*)> func)
{
	ActionBinding actionBind(actionName, keyEvent);
    actionBind.Bind(inputComponent, func);
	ActionBindings.push_back(actionBind);

	return ActionBindings.back();
}

//Used for mouse/joy move bindings
AxisBinding& IInputManager::BindAxis(const std::string& axisName, IInputComponent* inputComponent, std::function<void(IInputComponent*, f32)> func)
{
	AxisBinding axisBind(axisName);
    axisBind.Bind(inputComponent, func);
	AxisBindings.push_back(axisBind);

	return AxisBindings.back();
}


