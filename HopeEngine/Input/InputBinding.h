#pragma once

#include "InputCore.h"
#include <functional>
#include <string>

namespace Hope {
    class IInputComponent;
    
	struct IInputBinding
	{
		bool bExecuteWhenPaused : 1;

		IInputBinding()
			: bExecuteWhenPaused(true)
		{

		}
        
        //TODO(Ben): Implement the executeWhenPaused within the Execute methods.
        virtual void Execute() const {}
        virtual void ExecuteWithValue(f32 value) const {}
        
    protected:
        IInputComponent* mInputComponent;
	};

	struct KeyBinding : IInputBinding
	{
		//TODO(Ben): Would it be useful to have the Key/KeyChord?
		KeyBinding(const Key key, const InputEvent keyEvent) :
			IInputBinding(),
			mKey(key),
			mEvent(keyEvent),
            mFunc(nullptr)
		{ }
        virtual ~KeyBinding() { }
        
        inline const Key& Key() const { return mKey; }
        inline const InputEvent& Event() const { return mEvent; }
        
        void Bind(IInputComponent* inputComponent, std::function<void(IInputComponent*)> func)
        {
            mInputComponent = inputComponent;
            mFunc = func;
        }
        
        virtual void Execute() const override
        {
            if(mFunc != nullptr)
            {
                if(mInputComponent != nullptr)
                {
                    mFunc(mInputComponent);
                }
            }
        }
        
    protected:
        //Note: KeyChord being Key and Shift/Ctrl/Alt modifier flags.
        struct Key mKey;
        InputEvent mEvent;
        std::function<void(IInputComponent*)> mFunc;
	};

	struct MouseBinding : IInputBinding
	{
		MouseBinding(const MouseButton button, const InputEvent keyEvent) :
			IInputBinding(),
			mButton(button),
			mEvent(keyEvent),
            mFunc(nullptr)
		{ }
        virtual ~MouseBinding() { }
        
        inline const MouseButton& Button() const { return mButton; }
        inline const InputEvent& Event() const { return mEvent; }
        
        void Bind(IInputComponent* inputComponent, std::function<void(IInputComponent*)> func)
        {
            mInputComponent = inputComponent;
            mFunc = func;
        }
        
        virtual void Execute() const override
        {
            if(mFunc != nullptr)
            {
                if(mInputComponent != nullptr)
                {
                    mFunc(mInputComponent);
                }
            }
        }
        
    protected:
        MouseButton mButton;
        InputEvent mEvent;
        std::function<void(IInputComponent*)> mFunc;
	};

	struct AxisBinding : IInputBinding
	{
		AxisBinding(const std::string& axisName) :
			IInputBinding(),
			mAxisName(axisName),
            mFunc(nullptr)
		{ }
        virtual ~AxisBinding() { }
        
        inline const std::string& Axis() const { return mAxisName; }
        
        void Bind(IInputComponent* inputComponent, std::function<void(IInputComponent*, f32)> func)
        {
            mInputComponent = inputComponent;
            mFunc = func;
        }
        
        virtual void ExecuteWithValue(f32 value) const override
        {
            if(mFunc != nullptr)
            {
                if(mInputComponent != nullptr)
                {
                    //TODO(Ben): Calculate the value before calling the function
                    mFunc(mInputComponent, value);
                }
            }
        }
        
    protected:
        std::string mAxisName;
        std::function<void(IInputComponent*, f32)> mFunc;
	};

	struct ActionBinding : IInputBinding
	{
		ActionBinding(const std::string actionName, const InputEvent keyEvent) :
			IInputBinding(),
			mActionName(actionName),
			mEvent(keyEvent),
            mFunc(nullptr)
		{ }
        virtual ~ActionBinding() { }
        
        void Bind(IInputComponent* inputComponent, std::function<void(IInputComponent*)> func)
        {
            mInputComponent = inputComponent;
            mFunc = func;
        }
        
        virtual void Execute() const override
        {
            if(mFunc != nullptr)
            {
                if(mInputComponent != nullptr)
                {
                    mFunc(mInputComponent);
                }
            }
        }
        
    protected:
        //TODO(Ben): Would it be useful to have the ActionName?
        std::string mActionName;
        InputEvent mEvent;
        std::function<void(IInputComponent*)> mFunc;
	};
}
