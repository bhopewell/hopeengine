#include "InputCore.h"
#ifdef _WIN32
#include <SDL2/include/SDL_keycode.h>
#elif __APPLE__
#include <SDL_keycode.h>
#elif __linux__
#endif

using namespace Hope;

std::unordered_map<u32, const Key> Hope::Keys::mInputKeys = std::unordered_map<u32, const Key>();

const std::string MouseAxis::MouseX = "MouseX";
const std::string MouseAxis::MouseY = "MouseY";
const std::string MouseAxis::MouseScroll = "MouseScroll";

const Key Keys::Zero('0', "Zero");
const Key Keys::One('1', "One");
const Key Keys::Two('2', "Two");
const Key Keys::Three('3', "Three");
const Key Keys::Four('4', "Four");
const Key Keys::Five('5', "Five");
const Key Keys::Six('6', "Six");
const Key Keys::Seven('7', "Seven");
const Key Keys::Eight('8', "Eight");
const Key Keys::Nine('9', "Nine");

const Key Keys::A('a', "A");
const Key Keys::B('b', "B");
const Key Keys::C('c', "C");
const Key Keys::D('d', "D");
const Key Keys::E('e', "E");
const Key Keys::F('f', "F");
const Key Keys::G('g', "G");
const Key Keys::H('h', "H");
const Key Keys::I('i', "I");
const Key Keys::J('j', "J");
const Key Keys::K('k', "K");
const Key Keys::L('l', "L");
const Key Keys::M('m', "M");
const Key Keys::N('n', "N");
const Key Keys::O('o', "O");
const Key Keys::P('p', "P");
const Key Keys::Q('q', "Q");
const Key Keys::R('r', "R");
const Key Keys::S('s', "S");
const Key Keys::T('t', "T");
const Key Keys::U('u', "U");
const Key Keys::V('v', "V");
const Key Keys::W('w', "W");
const Key Keys::X('x', "X");
const Key Keys::Y('y', "Y");
const Key Keys::Z('z', "Z");

const Key Keys::SemiColon(';', "Semicolon");
const Key Keys::Equals('=', "Equals");
const Key Keys::Comma(',', "Comma");
const Key Keys::Hyphen('-', "Hyphen");
const Key Keys::Period('.', "Period");
const Key Keys::Slash('/', "Slash");
const Key Keys::Tilde('~', "Tilde");
const Key Keys::LeftBracket('[', "Left Bracket");
const Key Keys::BackSlash('\\', "Back Slash");
const Key Keys::RightBracket(']', "Right Bracket");
const Key Keys::Apostrophe('\'', "Apostrophe");
const Key Keys::Space(' ', "Space");
const Key Keys::Ampersand('&', "Ampersand");
const Key Keys::Asterix('*', "Asterix");
const Key Keys::Caret('^', "Caret");
const Key Keys::Colon(':', "Colon");
const Key Keys::Dollar('$', "Dollar");
const Key Keys::Exclamation('!', "Exclamation");
const Key Keys::LeftParentheses('(', "Left Parentheses");
const Key Keys::RightParentheses(')', "Right Parentheses");
const Key Keys::Quote('"', "Quote");
const Key Keys::Underscore('_', "Underscore");

const Key Keys::NumpadZero(SDLK_KP_0, "Numpad Zero");
const Key Keys::NumpadOne(SDLK_KP_1, "Numpad One");
const Key Keys::NumpadTwo(SDLK_KP_2, "Numpad Two");
const Key Keys::NumpadThree(SDLK_KP_3, "Numpad Three");
const Key Keys::NumPadFour(SDLK_KP_4, "Numpad Four");
const Key Keys::NumpadFive(SDLK_KP_5, "Numpad Five");
const Key Keys::NumpadSix(SDLK_KP_6, "Numpad Six");
const Key Keys::NumpadSeven(SDLK_KP_7, "Numpad Seven");
const Key Keys::NumpadEight(SDLK_KP_8, "Numpad Eight");
const Key Keys::NumpadNine(SDLK_KP_9, "Numpad Nine");
const Key Keys::NumpadPlus(SDLK_KP_PLUS, "Plus");
const Key Keys::NumpadMinus(SDLK_KP_MINUS, "Minus");
const Key Keys::NumpadDivide(SDLK_KP_DIVIDE, "Divide");
const Key Keys::NumpadMultiply(SDLK_KP_MULTIPLY, "Multiply");
const Key Keys::numpadDecimal(SDLK_KP_PERIOD, "Period");
const Key Keys::NumpadEnter(SDLK_KP_ENTER, "Numpad Enter");

const Key Keys::F1(SDLK_F1, "F1");
const Key Keys::F2(SDLK_F2, "F2");
const Key Keys::F3(SDLK_F3, "F3");
const Key Keys::F4(SDLK_F4, "F4");
const Key Keys::F5(SDLK_F5, "F5");
const Key Keys::F6(SDLK_F6, "F6");
const Key Keys::F7(SDLK_F7, "F7");
const Key Keys::F8(SDLK_F8, "F8");
const Key Keys::F9(SDLK_F9, "F9");
const Key Keys::F10(SDLK_F10, "F10");
const Key Keys::F11(SDLK_F11, "F11");
const Key Keys::F12(SDLK_F12, "F12");

const Key Keys::Backspace(SDLK_BACKSPACE, "Backspace");
const Key Keys::Tab(SDLK_TAB, "Tab");
const Key Keys::Enter(SDLK_RETURN, "Enter");
const Key Keys::Escape(SDLK_ESCAPE, "Escape");
const Key Keys::Insert(SDLK_INSERT, "Insert");
const Key Keys::Delete(SDLK_DELETE, "Delete");
const Key Keys::Left(SDLK_LEFT, "Left");
const Key Keys::Right(SDLK_RIGHT, "Right");
const Key Keys::Up(SDLK_UP, "Up");
const Key Keys::Down(SDLK_DOWN, "Down");
const Key Keys::PageUp(SDLK_PAGEUP, "Page Up");
const Key Keys::PageDown(SDLK_PAGEDOWN, "Page Down");

const Key Keys::Home(SDLK_HOME, "Home");
const Key Keys::End(SDLK_END, "End");
const Key Keys::CapsLock(SDLK_CAPSLOCK, "Caps Lock");
const Key Keys::ScrollLock(SDLK_SCROLLLOCK, "Scroll Lock");
const Key Keys::NumLock(SDLK_NUMLOCKCLEAR, "Num Lock");
const Key Keys::Pause(SDLK_PAUSE, "Pause");
const Key Keys::LShift(SDLK_LSHIFT, "Left Shift");
const Key Keys::RShift(SDLK_RSHIFT, "Right Shift");
const Key Keys::LCtrl(SDLK_LCTRL, "Left Control");
const Key Keys::RCtrl(SDLK_RCTRL, "Right Control");
const Key Keys::LAlt(SDLK_LALT, "Left Alt");
const Key Keys::RAlt(SDLK_RALT, "Right Alt");
#ifdef _WIN32
const Key Keys::LWnd(SDLK_LGUI, "Left Windows");
const Key Keys::RWnd(SDLK_RGUI, "Right Windows");
#elif __linux__
#elif __APPLE__
#endif

void Keys::Initialise()
{
#define AddKey(key) mInputKeys.emplace(std::pair<u32, const Key>((u32)key.Value, key))

	//Add numbers
	AddKey(Keys::Zero);
	AddKey(Keys::One);
	AddKey(Keys::Two);
	AddKey(Keys::Three);
	AddKey(Keys::Four);
	AddKey(Keys::Five);
	AddKey(Keys::Six);
	AddKey(Keys::Seven);
	AddKey(Keys::Eight);
	AddKey(Keys::Nine);

	//Add Lowercase
	AddKey(Keys::A);
	AddKey(Keys::B);
	AddKey(Keys::C);
	AddKey(Keys::D);
	AddKey(Keys::E);
	AddKey(Keys::F);
	AddKey(Keys::G);
	AddKey(Keys::H);
	AddKey(Keys::I);
	AddKey(Keys::J);
	AddKey(Keys::K);
	AddKey(Keys::L);
	AddKey(Keys::M);
	AddKey(Keys::N);
	AddKey(Keys::O);
	AddKey(Keys::P);
	AddKey(Keys::Q);
	AddKey(Keys::R);
	AddKey(Keys::S);
	AddKey(Keys::T);
	AddKey(Keys::U);
	AddKey(Keys::V);
	AddKey(Keys::W);
	AddKey(Keys::X);
	AddKey(Keys::Y);
	AddKey(Keys::Z);
	
	//Add symbols
	AddKey(Keys::SemiColon);
	AddKey(Keys::Equals);
	AddKey(Keys::Comma);
	AddKey(Keys::Hyphen);
	AddKey(Keys::Period);
	AddKey(Keys::Slash);
	AddKey(Keys::Tilde);
	AddKey(Keys::LeftBracket);
	AddKey(Keys::BackSlash);
	AddKey(Keys::RightBracket);
	AddKey(Keys::Apostrophe);
	AddKey(Keys::Space);
	AddKey(Keys::Ampersand);
	AddKey(Keys::Asterix);
	AddKey(Keys::Caret);
	AddKey(Keys::Colon);
	AddKey(Keys::Dollar);
	AddKey(Keys::Exclamation);
	AddKey(Keys::LeftParentheses);
	AddKey(Keys::RightParentheses);
	AddKey(Keys::Quote);
	AddKey(Keys::Underscore);

	AddKey(Keys::NumpadZero);
	AddKey(Keys::NumpadOne);
	AddKey(Keys::NumpadTwo);
	AddKey(Keys::NumpadThree);
	AddKey(Keys::NumPadFour);
	AddKey(Keys::NumpadFive);
	AddKey(Keys::NumpadSix);
	AddKey(Keys::NumpadSeven);
	AddKey(Keys::NumpadEight);
	AddKey(Keys::NumpadNine);
	AddKey(Keys::NumpadPlus);
	AddKey(Keys::NumpadMinus);
	AddKey(Keys::NumpadDivide);
	AddKey(Keys::NumpadMultiply);
	AddKey(Keys::numpadDecimal);
	AddKey(Keys::NumpadEnter);

	AddKey(Keys::F1);
	AddKey(Keys::F2);
	AddKey(Keys::F3);
	AddKey(Keys::F4);
	AddKey(Keys::F5);
	AddKey(Keys::F6);
	AddKey(Keys::F7);
	AddKey(Keys::F8);
	AddKey(Keys::F9);
	AddKey(Keys::F10);
	AddKey(Keys::F11);
	AddKey(Keys::F12);

	AddKey(Keys::Backspace);
	AddKey(Keys::Tab);
	AddKey(Keys::Enter);
	AddKey(Keys::Escape);
	AddKey(Keys::Insert);
	AddKey(Keys::Delete);
	AddKey(Keys::Left);
	AddKey(Keys::Right);
	AddKey(Keys::Up);
	AddKey(Keys::Down);
	AddKey(Keys::PageUp);
	AddKey(Keys::PageDown);

	AddKey(Keys::Home);
	AddKey(Keys::End);
	AddKey(Keys::CapsLock);
	AddKey(Keys::ScrollLock);
	AddKey(Keys::NumLock);
	AddKey(Keys::Pause);
	AddKey(Keys::LShift);
	AddKey(Keys::RShift);
	AddKey(Keys::LCtrl);
	AddKey(Keys::RCtrl);
	AddKey(Keys::LAlt);
	AddKey(Keys::RAlt);
#ifdef _WIN32
	AddKey(Keys::LWnd);
	AddKey(Keys::RWnd);
#elif __linux__
#elif __APPLE__
#endif
}
