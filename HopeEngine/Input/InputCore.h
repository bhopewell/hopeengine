#pragma once
#include "Hope.h"
#ifdef _WIN32
#include <SDL2/include/SDL_keycode.h>
#elif __APPLE__
#include <SDL_keycode.h>
#elif __linux__
#endif
#include <unordered_map>
#include <string>

namespace Hope {

	enum class InputEvent : u8
	{
		PRESSED = 0,
		RELEASED = 1,
		REPEAT = 2,
	};

	enum class MouseButton : u8
	{
		LEFT = 1, //SDL_BUTTON_LEFT
		MIDDLE = 2, //SDL_BUTTON_MIDDLE
		RIGHT = 3, //SDL_BUTTON_RIGHT
		X1 = 4, //SDL_BUTTON_X1
		X2 = 5, //SDL_BUTTON_X2
	};
    
    struct MouseAxis
    {
        static const std::string MouseX;
        static const std::string MouseY;
        static const std::string MouseScroll;
    };
    
	struct Key
	{
	public:
		Key(){}
		Key(u32 keyVal, const char* name) : Value(keyVal), Name(name){}

		//ASCII value of the key
		u32 Value;
		inline const char* ToString() const
		{
			return Name;
		}

		bool operator==(const Key& r) const { return this->Value == r.Value; }
		bool operator!=(const Key& r) const { return this->Value != r.Value; }

	private:
		//String representation of the Key
		const char* Name;
	};

	struct Keys
	{
		//ASCII Numbers
		static const Key Zero;
		static const Key One;
		static const Key Two;
		static const Key Three;
		static const Key Four;
		static const Key Five;
		static const Key Six;
		static const Key Seven;
		static const Key Eight;
		static const Key Nine;

		//Keypad Keys
		static const Key NumpadZero;
		static const Key NumpadOne;
		static const Key NumpadTwo;
		static const Key NumpadThree;
		static const Key NumPadFour;
		static const Key NumpadFive;
		static const Key NumpadSix;
		static const Key NumpadSeven;
		static const Key NumpadEight;
		static const Key NumpadNine;
		static const Key NumpadPlus;
		static const Key NumpadMinus;
		static const Key NumpadDivide;
		static const Key NumpadMultiply;
		static const Key numpadDecimal;
		static const Key NumpadEnter;

		//ASCII Letters
		static const Key A;
		static const Key B;
		static const Key C;
		static const Key D;
		static const Key E;
		static const Key F;
		static const Key G;
		static const Key H;
		static const Key I;
		static const Key J;
		static const Key K;
		static const Key L;
		static const Key M;
		static const Key N;
		static const Key O;
		static const Key P;
		static const Key Q;
		static const Key R;
		static const Key S;
		static const Key T;
		static const Key U;
		static const Key V;
		static const Key W;
		static const Key X;
		static const Key Y;
		static const Key Z;

		//ASCII Symbols
		static const Key SemiColon;
		static const Key Equals;
		static const Key Comma;
		static const Key Hyphen;
		static const Key Period;
		static const Key Slash;
		static const Key Tilde;
		static const Key LeftBracket;
		static const Key BackSlash;
		static const Key RightBracket;
		static const Key Apostrophe;
		static const Key Space;
		static const Key Ampersand;
		static const Key Asterix;
		static const Key Caret;
		static const Key Colon;
		static const Key Dollar;
		static const Key Exclamation;
		static const Key LeftParentheses;
		static const Key RightParentheses;
		static const Key Quote;
		static const Key Underscore;

		//F keys
		static const Key F1;
		static const Key F2;
		static const Key F3;
		static const Key F4;
		static const Key F5;
		static const Key F6;
		static const Key F7;
		static const Key F8;
		static const Key F9;
		static const Key F10;
		static const Key F11;
		static const Key F12;

		//Platform Keys
		static const Key Backspace;
		static const Key Tab;
		static const Key Enter;
		static const Key Escape;
		static const Key Insert;
		static const Key Delete;
		static const Key Left;
		static const Key Right;
		static const Key Up;
		static const Key Down;
		static const Key PageUp;
		static const Key PageDown;
		static const Key Home;
		static const Key End;
		static const Key CapsLock;
		static const Key ScrollLock;
		static const Key NumLock;
		static const Key Pause;
		static const Key LShift;
		static const Key RShift;
		static const Key LCtrl;
		static const Key RCtrl;
		static const Key LAlt;
		static const Key RAlt;
		#ifdef _WIN32
		static const Key LWnd;//Windows Key
		static const Key RWnd; //Windows Key
		#elif __linux__
		#elif __APPLE__
		#endif

		static void Initialise();
		static const std::unordered_map<u32, const Key>& GetInputKeys() { return mInputKeys; }

	private:
        //TODO(Ben): Instead of using a map, use an Associated Vector.
        //AssocVector will have faster look up times than a map
        //It also allows cache locality.
        //However the insert and erase functionality are O(n) instead of O(1).
        //That's an okay trade off as well only ever add to the map once at the start of the program
        //and we perform many, many lookups over the lifetime of the application
		static std::unordered_map<u32, const Key> mInputKeys; //Stores the virtual keys by key code.
	};
}
