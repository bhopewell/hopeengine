//
//  Hope.h
//  HopeEngine
//
//  Created by Ben Hopewell on 6/9/17.
//  Copyright � 2017 Ben Hopewell. All rights reserved.
//

#pragma once

#include <stdint.h>
#include <stdbool.h>
#include <cmath>

#include "HopeMacros.h"

//Type defs
typedef uint8_t	u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef int8_t i8;
typedef int16_t i16;
typedef int32_t i32;
typedef int64_t i64;

typedef float f32;
typedef double f64;

typedef u64 StaticTypeId;

//float utilities
const f32 PI = 3.14159265359f;
const f32 HALF_PI = PI / 2;
const f32 TWO_PI = PI * 2;
const f32 FloatEpsilon = 0.0001f;

inline bool AlmostEqual(f32 a, f32 b)
{
	return std::abs(a - b) < FloatEpsilon;
}

inline bool AlmostEqual(f64 a, f64 b)
{
	return std::abs(a - b) < FloatEpsilon;
}

inline f32 DegreesToRad(f32 degrees)
{
	return degrees * (PI / 180.0f);
}

inline f32 RadToDegrees(f32 rad)
{
	return rad * (180.0f / PI);
}
