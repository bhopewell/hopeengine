#include "Engine.h"

#include <iostream>

#include "Memory/Allocators/StackAllocator.h"
#include "Memory/Allocators/LinearAllocator.h"

#include "Time/GameTime.h"
#include "System/SystemManager.h"

#ifdef _WIN32
#include "Window/Windows/Win32Window.h"
#elif __linux__
#elif __APPLE__
#include "Window/MacOS/OSXWindow.h"
#endif

#include "InputManager/SDLInputManager.h"
#include "Threading/ThreadManager.h"
#include "Threading/IRunnableThread.h"

#ifdef HOPE_OPENGL
#ifdef _WIN32
#include "RenderCore/Renderer/OpenGL/Windows/OpenGLRenderer.h"
#elif __linux__
#elif __APPLE__
#include "RenderCore/Renderer/OpenGL/MacOS/OpenGLRenderer.h"
#endif
#elif HOPE_METAL
#include "RenderCore/Renderer/Metal/MetalRenderer.h"
#endif

#include "RenderCore/DeviceContext/DeviceContextFactory.h"
#include "GameWorld.h"

using namespace Hope;
using namespace Hope::EngineCore;
using namespace Hope::Graphics;

Hope::Core::StackAllocator Engine::mGlobalMemoryAllocator(DEFAULT_GLOBAL_MEMORY_BYTES);

Hope::Core::ThreadManager* Engine::mThreadManager = nullptr;
Hope::Core::IWindow* Engine::mWindow = nullptr;
Hope::Graphics::IRenderer* Engine::mRenderer = nullptr;
IInputManager* Engine::mInputManager = nullptr;
Hope::EngineCore::GameWorld* Engine::mGameWorld = nullptr;

Hope::EngineCore::SystemManager* Engine::mSystemManager = nullptr;

bool Game::IsRunning = true;

Engine::Engine()
{
}

Engine::~Engine()
{
    mGlobalMemoryAllocator.Free(mGameWorld);
    mGlobalMemoryAllocator.Free(mWindow);
    mGlobalMemoryAllocator.Free(mThreadManager);
    mGlobalMemoryAllocator.Free(mSystemManager);
    
    mGameWorld = nullptr;
    mWindow = nullptr;
    mThreadManager = nullptr;
    mSystemManager = nullptr;
}

bool Engine::CreateRenderer()
{
    IDeviceContext* deviceContext = Graphics::DeviceContextFactory::CreateContext(mWindow);
    
#ifdef HOPE_OPENGL
    std::cout << "Using OPENGL" << std::endl;
    mRenderer = mSystemManager->AddSystem<OpenGLRenderer>(deviceContext);
#elif HOPE_METAL
    std::cout << "Using METAL" << std::endl;
    mRenderer = mSystemManager->AddSystem<MetalRenderer>(deviceContext);
#else
    std::cout << "Using NONE" << std::endl;
    return false;
#endif
    
    if(!mRenderer->Initialise())
    {
        std::cout << "Failed to initialise the renderer." << std::endl;
        return false;
    }
    
    mRenderer->SetSystemPriority(ESystemPriority::EHigh);
    
    return true;
}

#ifdef _WIN32
bool Engine::StartUp(void* hInstance, i32 nCmdShow)
{
    //Initialise the thread manager before anything else.
    mThreadManager = new (mSystemMemoryManager.GetAllocator()) Core::ThreadManager();
    if (mThreadManager == nullptr)
        return false;
    
	mWindow = new (mSystemMemoryManager.GetAllocator()) Core::Win32Window(hInstance, nCmdShow, "HopeEngine_Win32", 800, 600);
	if (mWindow == nullptr)
		return false;

	if (!mWindow->Initialise())
		return false;

	//Create and start the render thread
	if (!CreateRenderer())
		return false;

	// Create the input manager
    mInputManager = mSystemManager->AddSystem<SDLInputManager>();
	if (mInputManager == nullptr)
		return false;

    //Create the GameWorld
    mGameWorld = GameWorld::ConstructGameWorld(mSystemMemoryManager.GetAllocator());
    
	return true;
}

i32 Engine::Run()
{
    mWindow->Show(true);
    
    while (Game::IsRunning)
    {
        GameTime::Tick();
        
        //SceneManager->Tick();
        
        // Tick all the registered systems if necessary
        mSystemManager->Tick(GameTime::GetDeltaTime());
    }
    
    //This is probably unnecessary for Windows... Just double check that before completely removing it though.
    // Main message loop:
    //MSG msg;
    //while (GetMessage(&msg, NULL, 0, 0))
    //{
    //	TranslateMessage(&msg);
    //	DispatchMessage(&msg);
    //}
    
    //return (i32)msg.wParam;
    return 0;
}

#elif __APPLE__

bool Engine::StartUp()
{
    //mSystemMemoryManager = new Hope::Core::HopeMemoryManager(GLOBAL_MEMORY_BYTES);
    mSystemManager = new (mGlobalMemoryAllocator) EngineCore::SystemManager();
    
    //Initialise the thread manager before anything else
    mThreadManager = new (mGlobalMemoryAllocator) Core::ThreadManager();
    if(mThreadManager == nullptr)
        return false;
    
    mWindow = new (mGlobalMemoryAllocator) Core::OSXWindow("HopeEngine_OSX", 800, 600);
    if(mWindow == nullptr)
        return false;
    
    if(!mWindow->Initialise())
        return false;
    
    //Create and start the render thread
    if(!CreateRenderer())
        return false;
    
    // Create the input manager
    mInputManager = mSystemManager->AddSystem<SDLInputManager>();
    if(!mInputManager->Initialise(mWindow))
        return false;
    
    //Create the game world
    mGameWorld = GameWorld::ConstructGameWorld(&mGlobalMemoryAllocator);
    
    return true;
}

i32 Engine::Run()
{
    mWindow->Show(true);
    
    while (Game::IsRunning)
    {
        // Update the game time
        GameTime::Tick();
        
        // Tick all the registered systems if necessary
        mSystemManager->Tick(GameTime::GetDeltaTime());
    }
    
    return 0;
}

#elif __linux__

#endif

void Engine::Shutdown()
{
    mInputManager->Shutdown();
    mRenderer->Shutdown();
    mWindow->Close();
	mThreadManager->KillAllThreads(true);
    
    //Destroy the Game World - Free the system allocated memory
    mGameWorld->Destroy();
    
    std::cout << "Application closed" << std::endl;
}
