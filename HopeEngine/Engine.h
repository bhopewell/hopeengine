#pragma once

#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#elif __linux__
#elif __APPLE__
#endif

#include "Hope.h"

namespace Hope {
	namespace Core {
		class IWindow;
        class StackAllocator;
		class ThreadManager;
	}
    
    namespace EngineCore {
        class GameWorld;
        class SystemManager;
    }
    
    namespace Graphics {
        class IRenderer;
    }

	class IInputManager;

	class Engine
	{
	public:
		explicit Engine();
		~Engine();

#ifdef _WIN32
		bool StartUp(void* hInstance, i32 nCmdShow);
#else
        bool StartUp();
#endif
		i32 Run();
		void Shutdown();

        inline static IInputManager* GetInputManager() { return mInputManager; }
		inline static Graphics::IRenderer* GetRenderer() { return mRenderer; }
        inline static Core::IWindow* GetWindow() { return mWindow; }
        
        inline static EngineCore::GameWorld* GetGameWorld() { return mGameWorld; }
        
    private:
        bool CreateRenderer();
        
	private:
        #define DEFAULT_GLOBAL_MEMORY_BYTES MegabytesToBytes(4)
        static Core::StackAllocator mGlobalMemoryAllocator;
        
        static Core::ThreadManager* mThreadManager;
        
		static Core::IWindow* mWindow;
        static Graphics::IRenderer* mRenderer;
		static IInputManager* mInputManager;
        
        static EngineCore::GameWorld* mGameWorld;
        
        static EngineCore::SystemManager* mSystemManager;
	};
}

class Game
{
public:
    static bool IsRunning;
};
