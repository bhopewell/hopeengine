//
//  UVBuffer.h
//  HopeEngine
//
//  Created by Ben Hopewell on 26/8/17.
//  Copyright © 2017 Ben Hopewell. All rights reserved.
//

#pragma once

#include <vector>
#include <glm/vec2.hpp>

namespace Hope {
    namespace EngineCore {

        struct TexCoordBuffer
        {
        public:
            //TexCoordBuffer Buffer
			TexCoordBuffer() :
            mUVs{}
            {
                
            }
            
			~TexCoordBuffer() = default;
            
            inline u32 TexCoordCount() const { return (u32)mUVs.size(); }
            void UpdateTexCoords(const glm::vec2* texCoords, u32 count, u32 begin)
            {
                //Resize the UV buffer if necessary
                if(count > TexCoordCount())
                    mUVs.resize(count);
                
                //Overwrite the necessary UVs
                for(u32 i = 0; i < count; ++i)
                    mUVs[begin + i] = *(texCoords + i);
            }
            
        private:
            std::vector<glm::vec2> mUVs;
        };
    }
}
