//
//  IndexBuffer.h
//  HopeEngine
//
//  Created by Ben Hopewell on 5/10/18.
//  Copyright © 2018 Ben Hopewell. All rights reserved.
//

#pragma once

#include <vector>
#include "Hope.h"

namespace Hope {
    namespace EngineCore {
        
        struct IndexBuffer
        {
        public:
            IndexBuffer() :
            mIndicies{}
            {
            }
            
            ~IndexBuffer() = default;
            
            inline u32 IndexCount() const { return (u32)mIndicies.size(); }
            void UpdateIndices(const u32* indices, u32 count, u32 begin)
            {
                //Resize the vertex buffer if necessary
                if(count > IndexCount())
                    mIndicies.resize(count);
                
                //Overwrite the necessary verts
                for(u32 i = 0; i < count; ++i)
                    mIndicies[begin + i] = *(indices + i);
            }
            
        private:
            std::vector<u32> mIndicies;
        };
    }
}
