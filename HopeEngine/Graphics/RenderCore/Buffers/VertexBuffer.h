//
//  VertexBuffer.h
//  HopeEngine
//
//  Created by Ben Hopewell on 23/8/17.
//  Copyright © 2017 Ben Hopewell. All rights reserved.
//

#pragma once

#include <vector>
#include <glm/vec3.hpp>

#include "Hope.h"

namespace Hope {
    namespace EngineCore {
        
        struct VertexBuffer
        {
        public:
            VertexBuffer() :
            mVertices{}
            {
                
            }

			~VertexBuffer() = default;
            
            inline u32 VertexCount() const { return (u32)mVertices.size(); }
            void UpdateVerts(const glm::vec3* verts, u32 count, u32 begin)
            {
                //Resize the vertex buffer if necessary
                if(count > VertexCount())
                    mVertices.resize(count);
                
                //Overwrite the necessary verts
                for(u32 i = 0; i < count; ++i)
                    mVertices[begin + i] = *(verts + i);
            }
            
        private:
            std::vector<glm::vec3> mVertices;
        };
    }
}
