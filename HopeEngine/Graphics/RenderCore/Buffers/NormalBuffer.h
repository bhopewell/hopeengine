//
//  NormalBuffer.h
//  HopeEngine
//
//  Created by Ben Hopewell on 26/8/17.
//  Copyright © 2017 Ben Hopewell. All rights reserved.
//

#pragma once

#include <vector>
#include <glm/vec3.hpp>

namespace Hope {
    namespace EngineCore {
        
        struct NormalBuffer
        {
        public:
            //Normal Buffer
            NormalBuffer() :
            mNormals{}
            {
                
            }
            
			~NormalBuffer() = default;
            
            inline u32 NormalCount() const { return (u32)mNormals.size(); }
            void UpdateNormals(const glm::vec3* normals, u32 count, u32 begin)
            {
                //Resize the normal buffer if necessary
                if(count > NormalCount())
                    mNormals.resize(count);
                
                //Overwrite the necessary normals
                for(u32 i = 0; i < count; ++i)
                    mNormals[begin + i] = *(normals + i);
            }
            
        private:
            std::vector<glm::vec3> mNormals;
        };
    }
}
