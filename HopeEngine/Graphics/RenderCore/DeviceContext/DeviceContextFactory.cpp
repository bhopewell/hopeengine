//
//  DeviceContextFactory.cpp
//  HopeEngine
//
//  Created by Ben Hopewell on 28/11/16.
//  Copyright � 2016 Ben Hopewell. All rights reserved.
//

#include "DeviceContextFactory.h"

#ifdef _WIN32
    #include "Core/Window/Windows/Win32Window.h"

    #ifdef HOPE_OPENGL
        #include "OpenGL/Windows/OpenGLDeviceContext.h"
    #endif

#elif __APPLE__
    #include "Core/Window/MacOS/OSXWindow.h"
    #ifdef HOPE_OPENGL
        #include "OpenGL/MacOS/OpenGLDeviceContext.h"
    #else
        #include "Metal/MetalDeviceContext.h"
    #endif
#endif


using namespace Hope::Core;
using namespace Hope::Graphics;

IDeviceContext* DeviceContextFactory::CreateContext(IWindow* window)
{
#if defined(_WIN32) && defined(HOPE_OPENGL)
	return new OpenGLDeviceContext(static_cast<const Win32Window*>(window));
#elif defined(__APPLE__) && defined(HOPE_OPENGL)
    //No device context for apple devices
    return new OpenGLDeviceContext(static_cast<OSXWindow*>(window));
#elif defined(__APPLE__) && defined(HOPE_METAL)
    return new MetalDeviceContext(static_cast<OSXWindow*>(window));
#endif
    
    return nullptr;
}
