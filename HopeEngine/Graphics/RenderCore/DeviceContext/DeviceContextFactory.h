//
//  DeviceContextFactory.h
//  HopeEngine
//
//  Created by Ben Hopewell on 28/11/16.
//  Copyright � 2016 Ben Hopewell. All rights reserved.
//

#pragma once

namespace Hope {
	namespace Core {
		class IWindow;
	}

	namespace Graphics {

		class IDeviceContext;

		class DeviceContextFactory
		{
		public:
			static IDeviceContext* CreateContext(Core::IWindow* window);
		};
	}
}
