//
//  MetalDeviceContextImpl.h
//  HopeEngine
//
//  Created by Ben Hopewell on 16/2/19.
//  Copyright © 2019 Ben Hopewell. All rights reserved.
//

#pragma once

#ifdef HOPE_METAL

#include <Metal/Metal.h>
#include <MetalKit/MetalKit.h>

#include "RenderCore/Renderer/IOSXRendererImpl.h"

namespace Hope {
    namespace Graphics {
        class IRenderer;
    }
}

@interface MetalDeviceContextImpl : MTKView
{
    id<MTLDevice> mDevice;
    id<MTLCommandQueue> mCommandQueue;
    id<MTLRenderPipelineState> mPipelineState;
    
    id<MTLTexture> mDrawable;
    id<MTLCommandBuffer> mCommandBuffer;
    MTLRenderPassDescriptor* mPassDescriptor;
    id<MTLRenderCommandEncoder> mEncoder;
    
    id<MTLBuffer> positionBuffer;
    id<MTLBuffer> colorBuffer;
}

-(id<MTLDevice>)GetDevice;
-(id)initWithFrame:(NSRect)frame;
-(void)MakeCurrent;
-(void)Clear;
-(void)PushBuffer:(id<MTLBuffer>)vertexBuffer colorBuffer:(id<MTLBuffer>)colorBuffer;
-(void)Flush;

@end

#endif
