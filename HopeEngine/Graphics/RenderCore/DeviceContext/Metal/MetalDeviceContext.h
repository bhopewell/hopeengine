//
//  MetalDeviceContext.h
//  HopeEngine
//
//  Created by Ben Hopewell on 16/2/19.
//  Copyright © 2019 Ben Hopewell. All rights reserved.
//

#pragma once

#include "../IDeviceContext.h"

namespace Hope {
    namespace Core {
        class OSXWindow;
    }
    
    namespace Graphics {
        
        struct MetalDeviceContextProxy;
        
        class MetalDeviceContext : public IDeviceContext
        {
        public:
            MetalDeviceContext(Core::OSXWindow* window);
            ~MetalDeviceContext() = default;
            
            bool Initialise() override;
            void MakeCurrent() override;
            void SwapFrameBuffers() override;
            void Shutdown() override;
            
            id GetDevice();
            void Clear();
            void PushBuffer(id vertBuffer, id colorBuffer);
            
        private:
            Core::OSXWindow* mWindow;
            MetalDeviceContextProxy* mDeviceContext;
        };
    }
}
