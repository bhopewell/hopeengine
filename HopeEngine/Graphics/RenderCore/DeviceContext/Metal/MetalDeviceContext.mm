//
//  MetalDeviceContext.m
//  HopeEngine
//
//  Created by Ben Hopewell on 16/2/19.
//  Copyright © 2019 Ben Hopewell. All rights reserved.
//

#ifdef HOPE_METAL

#include "MetalDeviceContext.h"
#include "MetalDeviceContextImpl.h"
#include "Window/MacOS/OSXWindow.h"
#include "RenderCore/Renderer/IRenderer.h"

#include <iostream>

namespace Hope {
    namespace Graphics {
        
        struct MetalDeviceContextProxy
        {
            MetalDeviceContextImpl* wrapped;
        };
        
        MetalDeviceContext::MetalDeviceContext(Core::OSXWindow* window) :
        mWindow(window),
        mDeviceContext(new MetalDeviceContextProxy())
        {
            mDeviceContext->wrapped = [[MetalDeviceContextImpl alloc] init];
        }
        
        id MetalDeviceContext::GetDevice()
        {
            return [mDeviceContext->wrapped GetDevice];
        }
        
        bool MetalDeviceContext::Initialise()
        {
            return mWindow->RegisterRenderView(mDeviceContext->wrapped);
        }
        
        void MetalDeviceContext::MakeCurrent()
        {
            [mDeviceContext->wrapped MakeCurrent];
        }
        
        void MetalDeviceContext::PushBuffer(id vertBuffer, id colorBuffer)
        {
            [mDeviceContext->wrapped PushBuffer:vertBuffer colorBuffer:colorBuffer];
        }
        
        void MetalDeviceContext::Clear()
        {
            [mDeviceContext->wrapped Clear];
        }
        
        void MetalDeviceContext::SwapFrameBuffers()
        {
            [mDeviceContext->wrapped Flush];
        }
        
        void MetalDeviceContext::Shutdown()
        {
        }
    }
}

@implementation MetalDeviceContextImpl

+(id)layerClass
{
    return [CAMetalLayer class];
}

-(id<MTLDevice>)GetDevice
{
    return mDevice;
}

-(id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if(self == nil)
        return nil;
    
    //Configure for manual rendering
    self.paused = YES;
    self.enableSetNeedsDisplay = NO;
    // self.autoResizeDrawable = NO; // This has something to do with resizing...
    
    //Create the metal device
    mDevice = MTLCreateSystemDefaultDevice();
    if(mDevice == nil)
    {
        //TODO(Ben): Add logging here that tells the user the device failed to be created
        return nil;
    }
    
    NSError* libraryError = NULL;
    NSString* libraryFile = [[NSBundle mainBundle] pathForResource:@"HopeMTLShaders" ofType:@"metallib"];
    id<MTLLibrary> defaultLibrary = [mDevice newLibraryWithFile:libraryFile error:&libraryError];
    
    if(defaultLibrary == nil)
    {
        NSLog(@"Library error: %@", libraryError);
        return nil;
    }
    
    id<MTLFunction> vertexShader = [defaultLibrary newFunctionWithName:@"vertex_main"];
    id<MTLFunction> fragmentShader = [defaultLibrary newFunctionWithName:@"fragment_main"];
    
    MTLRenderPipelineDescriptor* pipelineDesc = [[MTLRenderPipelineDescriptor alloc]init];
    pipelineDesc.label = @"Simple Pipeline";
    pipelineDesc.vertexFunction = vertexShader;
    pipelineDesc.fragmentFunction = fragmentShader;
    pipelineDesc.colorAttachments[0].pixelFormat = MTLPixelFormatBGRA8Unorm_sRGB;
    
    NSError *error = nullptr;
    mPipelineState = [mDevice newRenderPipelineStateWithDescriptor:pipelineDesc error:&error];
    
    if(mPipelineState == nil)
    {
        //TODO(Ben): Add logging here that tells the user the pipeline state failed to be created
        return nil;
    }
    
    if(error != nullptr)
    {
        return nil;
    }
    
    //Create the command queue - This allows us to submit commands and instructions to the GPU
    mCommandQueue = [mDevice newCommandQueue];
    
    self.device = mDevice;
    self.colorPixelFormat = MTLPixelFormatBGRA8Unorm_sRGB;
    self.clearColor = MTLClearColorMake(1,0,1,1);
    
    return self;
}

-(void)MakeCurrent
{
    // mCommandQueue creates a new command buffer - It also waits on a semaphore if being used on a single thread.
    mCommandBuffer = mCommandQueue.commandBuffer;
    mCommandBuffer.label = @"CmdBuffer";
}

-(void)Clear
{
    // Obtain a renderPassDescriptor generated from the view's drawable textures
    mPassDescriptor = self.currentRenderPassDescriptor;
    //mPassDescriptor.colorAttachments[0].texture = framebufferTexture;
    mPassDescriptor.colorAttachments[0].clearColor = self.clearColor;//MTLClearColorMake(1, 0, 1, 1);
    mPassDescriptor.colorAttachments[0].storeAction = MTLStoreActionStore;
    mPassDescriptor.colorAttachments[0].loadAction = MTLLoadActionClear;
    
    //Create the encoder for this render pass
    if(mPassDescriptor != nil)
    {
        mEncoder = [mCommandBuffer renderCommandEncoderWithDescriptor:mPassDescriptor];
        mEncoder.label = @"RenderEncoder";
        
        [mEncoder setViewport:(MTLViewport){ 0, 0, self.frame.size.width, self.frame.size.height, -1, 1 }];
        [mEncoder setRenderPipelineState:mPipelineState];
    }
}

-(void)PushBuffer:(id<MTLBuffer>)vertexBuffer colorBuffer:(id<MTLBuffer>)colorBuffer
{
    [mEncoder setVertexBuffer:vertexBuffer offset:0 atIndex:0];
    [mEncoder setVertexBuffer:colorBuffer offset:0 atIndex:1];
    
    // Draw the 3 vertices of our triangle
    [mEncoder drawPrimitives:MTLPrimitiveTypeTriangle
                vertexStart:0
                vertexCount:3];
}

-(void)Flush
{
    //Finalize the encoding
    [mEncoder endEncoding];
    
    // Schedule a present once the framebuffer is complete using the current drawable
    [mCommandBuffer presentDrawable:self.currentDrawable];
    
    //Commit the command buffer
    [mCommandBuffer commit];
    
    //Update the current drawable
    [self draw];
    
    //Nil out everything so we can ensure the next render pass uses fresh instances.
    mEncoder = nil;
    mPassDescriptor = nil;
    mCommandBuffer = nil;
}

@end

#endif
