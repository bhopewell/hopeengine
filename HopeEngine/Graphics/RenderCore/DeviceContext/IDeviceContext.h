//
//  IDeviceContext.h
//  HopeEngine
//
//  Created by Ben Hopewell on 28/11/16.
//  Copyright � 2016 Ben Hopewell. All rights reserved.
//

#pragma once

namespace Hope {
	namespace Core{
		class IWindow;
	}

	namespace Graphics {
        
        class IRenderer;
		
		class IDeviceContext
		{
		public:
			virtual bool Initialise() = 0;
			virtual void MakeCurrent() = 0;
			virtual void SwapFrameBuffers() = 0;
			virtual void Shutdown() = 0;
		};
	}
}
