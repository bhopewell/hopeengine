//
//  OpenGLDeviceContext.cpp
//  HopeEngine
//
//  Created by Ben Hopewell on 28/11/16.
//  Copyright � 2016 Ben Hopewell. All rights reserved.
//

#if defined(_WIN32) && defined(HOPE_OPENGL)

#include <iostream>
#include <assert.h>

#include <glew2.1\include\GL\glew.h>
#include <glew2.1\include\GL\wglew.h>

#include "OpenGLDeviceContext.h"
#include "Core/Window/Windows/Win32Window.h"

using namespace Hope::Core;
using namespace Hope::Graphics;

OpenGLDeviceContext::OpenGLDeviceContext(const Win32Window* window) :
mWindow{ window },
mDeviceContext{ NULL },
mRenderContext{ NULL }
{
}

bool OpenGLDeviceContext::Initialise()
{
	PIXELFORMATDESCRIPTOR pixelFormatDesc = {
		sizeof(PIXELFORMATDESCRIPTOR),
		1,
		PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER, //Flags
		PFD_TYPE_RGBA,												//Type of framebuffer
		32,															//Colour depth of the frame buffer
		0, 0, 0, 0, 0, 0, 0, 0,										//Bits and shift of RBGA
		0,															//Accum bits
		0, 0, 0, 0,													//Accum bits of RGBA
		24,															//Bits for the depth buffer (Can up this to 32 if we want to store more than just depth data in the depth buffer)
		8,															//Bits for the stencil buffer
		0,															//Number of aux buffers
		PFD_MAIN_PLANE,												//Layer type
		0,															//Reserved
		0, 0, 0														//Masks
	};

	//Create the render context
	mDeviceContext = GetDC(mWindow->GetHWND());

	if (mDeviceContext == nullptr)
	{
		//TODO(Ben): Failed to get the device context for the windows client area
		return false;
	}

	//Set the pixel format of the device context
	i32 pixelFormat = ChoosePixelFormat(mDeviceContext, &pixelFormatDesc);
	if (pixelFormat == 0)
	{
		//TODO(Ben): Pixel format cannot be found or was incorrectly filled out.
		return false;
	}

	if (!SetPixelFormat(mDeviceContext, pixelFormat, &pixelFormatDesc))
	{
		//TODO(Ben): Failed to set the pixel format
		return false;
	}

	//Create the temporary render context
	HGLRC tempRenderContext = wglCreateContext(mDeviceContext);
	wglMakeCurrent(mDeviceContext, tempRenderContext);

	//Initialise the glew API
	glewExperimental = true;
	if (glewInit() != GLEW_OK)
	{
		GLenum err = glGetError();
		const GLubyte* errorString = glewGetErrorString(err);
		std::cout << "GLEW initialisation error " << errorString << std::endl;
		return false;
	}

	//TODO(Ben): In the future we may want to call wglCreateContextAttribsARB in order to specify the exact GL version to run
	//Create the permanent render context and replace the temporary context with the permanent context.
	mRenderContext = wglCreateContext(mDeviceContext); 
	wglMakeCurrent(NULL, NULL);
	wglDeleteContext(tempRenderContext);
	wglMakeCurrent(mDeviceContext, mRenderContext);

	//Get the OpenGL Version we will be using
	glGetIntegerv(GL_MAJOR_VERSION, &mOpenGLVersion[0]);
	glGetIntegerv(GL_MINOR_VERSION, &mOpenGLVersion[1]);

	std::cout << "OpenGL version: " << mOpenGLVersion[0] << "." << mOpenGLVersion[1] << std::endl;

	return mRenderContext != nullptr;
}

void OpenGLDeviceContext::MakeCurrent()
{
	if (!wglMakeCurrent(mDeviceContext, mRenderContext))
	{
		//Failed to make this render context the current context.
		assert(false);
	}
}

void OpenGLDeviceContext::SwapFrameBuffers()
{
	if(!SwapBuffers(mDeviceContext))
	{
		//Failed to swap the buffers
		assert(false);
	}
}

void OpenGLDeviceContext::Shutdown()
{
	wglMakeCurrent(NULL, NULL);
	wglDeleteContext(mRenderContext);
}

#endif