//
//  OpenGLDeviceContext.h
//  HopeEngine
//
//  Created by Ben Hopewell on 28/11/16.
//  Copyright � 2016 Ben Hopewell. All rights reserved.
//

#pragma once

#if defined(_WIN32) && defined(HOPE_OPENGL)

#include <windows.h>
#include "Hope.h"
#include "../../IDeviceContext.h"

namespace Hope {
	namespace Core {
		class Win32Window;
	}

	namespace Graphics {

		class OpenGLDeviceContext : public IDeviceContext
		{
		public:
			OpenGLDeviceContext(const Core::Win32Window* window);
			~OpenGLDeviceContext() = default;

			virtual bool Initialise() override;
			virtual void MakeCurrent() override;
			virtual void SwapFrameBuffers() override;
			virtual void Shutdown() override;

		private:
			
			i32 mOpenGLVersion[2];

			HDC mDeviceContext;
			HGLRC mRenderContext;
			const Core::Win32Window* mWindow;
		};
	}
}

#endif