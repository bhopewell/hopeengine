//
//  OpenGLDeviceContext.mm
//  HopeEngine
//
//  Created by Ben Hopewell on 28/10/18.
//  Copyright © 2018 Ben Hopewell. All rights reserved.
//

#ifdef HOPE_OPENGL

#include "OpenGLDeviceContext.h"
#include "OpenGLDeviceContextImpl.h"
#include "Window/MacOS/OSXWindow.h"
#include "RenderCore/Renderer/IRenderer.h"

namespace Hope {
    namespace Graphics {
        
        struct OpenGLDeviceContextProxy
        {
            OpenGLDeviceContextImpl* wrapped;
        };
        
        OpenGLDeviceContext::OpenGLDeviceContext(Core::OSXWindow* window) :
            mWindow(window),
            mDeviceContext(new OpenGLDeviceContextProxy())
        {
            mDeviceContext->wrapped = [[OpenGLDeviceContextImpl alloc] init];
        }
        
        bool OpenGLDeviceContext::Initialise()
        {
            return mWindow->RegisterRenderView(mDeviceContext->wrapped);
        }
        
        void OpenGLDeviceContext::MakeCurrent()
        {
            [mDeviceContext->wrapped MakeCurrent];
        }
        
        void OpenGLDeviceContext::SwapFrameBuffers()
        {
            [mDeviceContext->wrapped Flush];
        }
        
        void OpenGLDeviceContext::Shutdown()
        {
        }
    }
}

@implementation OpenGLDeviceContextImpl

-(id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    
    if(self)
    {
        NSOpenGLPixelFormatAttribute attrs[] =
        {
            NSOpenGLPFADoubleBuffer,
            NSOpenGLPFADepthSize, 32,
            NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion3_2Core,
            0
        };
        
        //Create the pixel format
        NSOpenGLPixelFormat* pixFmt = [[NSOpenGLPixelFormat alloc] initWithAttributes:attrs];
        if(!pixFmt)
        {
            return nil;
        }
        
        //Create the OpenGL Context
        NSOpenGLContext* context = [[NSOpenGLContext alloc] initWithFormat:pixFmt shareContext:nullptr];
        if(!context)
        {
            return nil;
        }
        
        self.pixelFormat = pixFmt;
        self.openGLContext = context;
        
        [self MakeCurrent];
        
        return self;
    }
    else
    {
        return nil;
    }
}

-(void)MakeCurrent
{
    [self.openGLContext makeCurrentContext];
}

-(void)Flush
{
    [self.openGLContext flushBuffer];
}

@end

#endif

