//
//  OpenGLDeviceContext.h
//  HopeEngine
//
//  Created by Ben Hopewell on 28/10/18.
//  Copyright © 2018 Ben Hopewell. All rights reserved.
//

#pragma once

#include "../../IDeviceContext.h"

namespace Hope {
    namespace Core {
        class OSXWindow;
    }
    
    namespace Graphics {
        
        struct OpenGLDeviceContextProxy;
        
        class OpenGLDeviceContext : public IDeviceContext
        {
        public:
            OpenGLDeviceContext(Core::OSXWindow* window);
            ~OpenGLDeviceContext() = default;
            
            bool Initialise() override;
            void MakeCurrent() override;
            void SwapFrameBuffers() override;
            void Shutdown() override;
        
        private:
            Core::OSXWindow* mWindow;
            OpenGLDeviceContextProxy* mDeviceContext;
        };
    }
}
