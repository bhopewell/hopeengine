//
//  OpenGLDeviceContextImpl.h
//  HopeEngine
//
//  Created by Ben Hopewell on 28/10/18.
//  Copyright © 2018 Ben Hopewell. All rights reserved.
//

#pragma once

#ifdef HOPE_OPENGL

#include <Cocoa/Cocoa.h>

#include "RenderCore/Renderer/IOSXRendererImpl.h"

namespace Hope {
    namespace Graphics {
        class IRenderer;
    }
}

@interface OpenGLDeviceContextImpl : NSOpenGLView
{
}

-(id)initWithFrame:(NSRect)frame;
-(void)MakeCurrent;
-(void)Flush;

@end

#endif
