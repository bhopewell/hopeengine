//
//  IRenderer.h
//  HopeEngine
//
//  Created by Ben Hopewell on 28/11/16.
//  Copyright © 2016 Ben Hopewell. All rights reserved.
//

#pragma once

#include "Hope.h"
#include "EngineCore/System/ISystem.h"

namespace Hope {
    namespace Core {
        class IWindow;
    }
    
    namespace Graphics {
        //The actual renderer definition
        class IRenderer : public EngineCore::ISystem
        {
            HOPE_SYSTEM(IRenderer, ISystem)
        public:
            IRenderer() : EngineCore::ISystem(EngineCore::ESystemPriority::EVeryHigh) {};
            virtual ~IRenderer() = default;
            
			virtual void Resize(u32 w, u32 h) = 0;

			virtual bool Initialise() = 0;
            virtual void Shutdown() = 0;
        };
    }
}
