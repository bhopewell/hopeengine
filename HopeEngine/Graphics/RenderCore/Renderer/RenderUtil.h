//
//  RenderUtil.h
//  HopeEngine
//
//  Created by Ben Hopewell on 27/12/16.
//  Copyright © 2016 Ben Hopewell. All rights reserved.
//

#pragma once

enum class ETextureType
{
    TexType_2D,
    TexType_3D,
    TexType_Cube
};

enum class EPixelFormat
{
    R_u16,
    R_i16,
    R_f16,
    RG_u16,
    RG_i16,
    RG_f16,
    RGB10A2_u32,
    RGBA_u8,
    RGBA_i8,
    RGBA_u16,
    RGBA_i16,
    RGBA_u32,
    RGBA_i32,
    RGBA_f32
};
