//
//  OpenGLRenderer.h
//  HopeEngine
//
//  Created by Ben Hopewell on 06/08/17.
//  Copyright © 2017 Ben Hopewell. All rights reserved.
//

#pragma once

#if defined(_WIN32) && defined(HOPE_OPENGL)

#include "../../IRenderer.h"

namespace Hope {
	namespace Graphics {

		class IDeviceContext;

		class OpenGLRenderer : public IRenderer
		{
		public:
			OpenGLRenderer(IDeviceContext* deviceContext);
			~OpenGLRenderer();

			virtual void Resize(u32 w, u32 h) override;

			virtual bool Initialise() override;
            virtual void PreProcess() override;
			virtual void Render() override;
            virtual void PostProcess() override;
            virtual void Present() override;
			virtual void Shutdown() override;

		protected:
			IDeviceContext* mDeviceContext;
		};
	}
}

#endif
