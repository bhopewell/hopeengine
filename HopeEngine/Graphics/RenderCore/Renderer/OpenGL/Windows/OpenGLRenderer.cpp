//
//  OpenGLRenderer.cpp
//  HopeEngine
//
//  Created by Ben Hopewell on 6/8/17.
//  Copyright © 2017 Ben Hopewell. All rights reserved.
//

#if defined(_WIN32) && defined(HOPE_OPENGL)

#include <iostream>
#include <glew2.1\include\GL\glew.h>

#include "OpenGLRenderer.h"
#include "Window/Windows/Win32Window.h"
#include "RenderCore/DeviceContext/OpenGL/Windows/OpenGLDeviceContext.h"

using namespace Hope;
using namespace Hope::Core;
using namespace Hope::Graphics;

OpenGLRenderer::OpenGLRenderer(IDeviceContext* deviceContext) :
	IRenderer()
{
	mDeviceContext = deviceContext;
}

OpenGLRenderer::~OpenGLRenderer()
{
	if (mDeviceContext != nullptr)
	{
		delete mDeviceContext;
		mDeviceContext = nullptr;
	}
}

bool OpenGLRenderer::Initialise()
{
	//Initialise the device context and set it to the current context
	if (!mDeviceContext->Initialise())
	{
		return false;
	}

	//Set the device context to the current context
	mDeviceContext->MakeCurrent();
	return true;
}

void OpenGLRenderer::Resize(u32 w, u32 h)
{
	glViewport(0, 0, w, h);
	//TODO(Ben): Adjust the projection matrix here...
}

void OpenGLRenderer::PreProcess()
{
    mDeviceContext->MakeCurrent();
    
    glClearColor(1, 0, 1, 1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void OpenGLRenderer::Render()
{
	//TODO(Ben): Draw triangle here.
	glBegin(GL_TRIANGLES);
	glColor3f(1.0f, 0.0f, 0.0f);   
	glVertex2f(0.0f, 0.5f);
	glColor3f(0.0f, 1.0f, 0.0f);   
	glVertex2f(0.5f, -0.5f);
	glColor3f(0.0f, 0.0f, 1.0f);   
	glVertex2f(-0.5f, -0.5f);
	glEnd();
}

void OpenGLRenderer::PostProcess()
{
    
}

void OpenGLRenderer::Present()
{
    mDeviceContext->SwapFrameBuffers();
}

void OpenGLRenderer::Shutdown()
{
	//Shutdown the device context
	mDeviceContext->Shutdown();
}

#endif
