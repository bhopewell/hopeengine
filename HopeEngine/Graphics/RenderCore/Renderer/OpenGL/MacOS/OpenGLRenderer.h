//
//  OpenGLRenderer.h
//  HopeEngine
//
//  Created by Ben Hopewell on 06/08/17.
//  Copyright © 2017 Ben Hopewell. All rights reserved.
//

#pragma once

#if defined(__APPLE__) && defined(HOPE_OPENGL)

#include "../../IRenderer.h"
#include <memory>

namespace Hope {
	namespace Graphics {
        struct OpenGLRendererProxy;
        
        class IDeviceContext;
        
        //Wrapper for Obj-C++ renderer
        class OpenGLRenderer : public IRenderer
		{
		public:
			OpenGLRenderer(IDeviceContext* deviceContext);
            virtual ~OpenGLRenderer();
            
            virtual void Resize(u32 w, u32 h) override;
            
            virtual void PreTick(f32 deltaTime) override;
            virtual void Tick(f32 deltaTime) override;
            virtual void PostTick(f32 deltaTime) override;
            
			virtual bool Initialise() override;
			virtual void Shutdown() override;
            
        private:
            IDeviceContext* mDeviceContext;
            std::unique_ptr<OpenGLRendererProxy> mRenderer;
		};
	}
}

#endif
