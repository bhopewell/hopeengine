//
//  OpenGLRenderer.cpp
//  HopeEngine
//
//  Created by Ben Hopewell on 6/8/17.
//  Copyright © 2017 Ben Hopewell. All rights reserved.
//
#ifdef HOPE_OPENGL

#include "Engine.h"
#include "OpenGLRenderer.h"
#include "OpenGLRendererImpl.h"
#include "Window/IWindow.h"
#include "OpenGLRendererImpl.h"
#include "RenderCore/Shader/OpenGLShader.h"

#include "RenderCore/DeviceContext/IDeviceContext.h"

#include <OpenGL/gl.h>
#include <OpenGL/gl3.h>
#include <iostream>

namespace Hope {
    namespace Graphics {

        struct OpenGLRendererProxy
        {
            OpenGLRendererImpl* wrapped;
        };
        
        OpenGLRenderer::OpenGLRenderer(IDeviceContext* deviceContext) :
            IRenderer(),
            mDeviceContext(deviceContext),
            mRenderer(new OpenGLRendererProxy())
        {
            mRenderer->wrapped = [[OpenGLRendererImpl alloc] init];
        }
        
        OpenGLRenderer::~OpenGLRenderer()
        {
        }
        
        void OpenGLRenderer::Resize(u32 w, u32 h)
        {
                          
        }
                      
        bool OpenGLRenderer::Initialise()
        {
            return [mRenderer->wrapped Initialise: mDeviceContext];
        }
        
        void OpenGLRenderer::PreTick(f32 deltaTime)
        {
            //Ensure the render context is current
            mDeviceContext->MakeCurrent();
            
            [mRenderer->wrapped PreProcess];
        }
        
        void OpenGLRenderer::Tick(f32 deltaTime)
        {
            [mRenderer->wrapped Render];
        }
        
        void OpenGLRenderer::PostTick(f32 deltaTime)
        {
            [mRenderer->wrapped PostProcess];
            
            [mRenderer->wrapped Present];
        }
        
        void OpenGLRenderer::Shutdown()
        {
            [mRenderer->wrapped Shutdown];
        }
    }
}

@implementation OpenGLRendererImpl

-(bool) Initialise:(Hope::Graphics::IDeviceContext*)deviceContext;
{
    mDeviceContext = deviceContext;
    if(!mDeviceContext->Initialise())
    {
        return false;
    }
    
    mWindow = static_cast<Hope::Core::OSXWindow*>(Hope::Engine::GetWindow());
    
    Hope::Graphics::OpenGLShader* shader =
    new Hope::Graphics::OpenGLShader("/Users/ben/Documents/Repositories/HopeEngine/HopeEngine/Graphics/Shaders/GLSL/DefaultVert.vert",
                                     "/Users/ben/Documents/Repositories/HopeEngine/HopeEngine/Graphics/Shaders/GLSL/DefaultFrag.frag");

    delete shader;
    
    return true;
}

-(void) PreProcess
{    
    glClearColor(1, 0, 1, 0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    //TODO(Ben): Implement Z sorting
    //TODO(Ben): Implement deferred rendering
}

-(void) Render
{
    //Currently there is nothing to do because the OpenGLView has control over when the rendering occurs. :/
    //TODO(Ben): Render stuff here.
    
    //glBindFramebuffer(GL_FRAMEBUFFER, 0);
    
    glBegin(GL_TRIANGLES);
    glColor3f(1.0f, 0.0f, 0.0f);
    glVertex2f(0.0f, 0.5f);
    glColor3f(0.0f, 1.0f, 0.0f);
    glVertex2f(0.5f, -0.5f);
    glColor3f(0.0f, 0.0f, 1.0f);
    glVertex2f(-0.5f, -0.5f);
    glEnd();
}

-(void) PostProcess
{
    //TODO(Ben): Implement post-processing pipeline
    
    //TODO(Ben): Perform colour correction here - should be the last thing that's done.
}

-(void) Present
{
    mDeviceContext->SwapFrameBuffers();
}

-(void) Shutdown
{
    mDeviceContext->Shutdown();
    
    if(mWindow != nullptr)
        mWindow = nullptr;
}


@end

#endif
