//
//  OpenGLRendererImpl.h
//  HopeEngine
//
//  Created by Ben Hopewell on 8/8/17.
//  Copyright © 2017 Ben Hopewell. All rights reserved.
//

#pragma once

#if defined(__APPLE__) && defined(HOPE_OPENGL)

#include "Window/MacOS/OSXWindow.h"
#include "RenderCore/Renderer/IOSXRendererImpl.h"
#include "RenderCore/DeviceContext/OpenGL/MacOS/OpenGLDeviceContextImpl.h"

@interface OpenGLRendererImpl : NSObject <IOSXRendererImpl>
{
    Hope::Graphics::IDeviceContext* mDeviceContext;
    const Hope::Core::OSXWindow* mWindow;
}

-(bool) Initialise:(Hope::Graphics::IDeviceContext*)deviceContext;
-(void) PreProcess;
-(void) Render;
-(void) PostProcess;
-(void) Present;
-(void) Shutdown;

@end

#endif
