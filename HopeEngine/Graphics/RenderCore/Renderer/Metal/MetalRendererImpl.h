//
//  MetalRendererImpl.h
//  HopeEngine
//
//  Created by Ben Hopewell on 31/12/16.
//  Copyright © 2016 Ben Hopewell. All rights reserved.
//

#pragma once

#if defined(__APPLE__) && defined(HOPE_METAL)

#import <Metal/Metal.h>
#import <MetalKit/MetalKit.h>

#include "Core/Window/MacOS/OSXWindow.h"
#include "../IOSXRendererImpl.h"
#include "../../DeviceContext/Metal/MetalDeviceContext.h"

@interface MetalRendererImpl : NSObject <IOSXRendererImpl>
{
    const Hope::Core::OSXWindow* mWindow;
    Hope::Graphics::MetalDeviceContext* mDeviceContext;
   
    id<MTLBuffer> positionBuffer;
    id<MTLBuffer> colorBuffer;
}

-(bool) Initialise:(Hope::Graphics::IDeviceContext*)deviceContext;
-(void) PreProcess;
-(void) Render;
-(void) PostProcess;
-(void) Present;
-(void) Shutdown;

-(void) buildBuffers;

@end

#endif
