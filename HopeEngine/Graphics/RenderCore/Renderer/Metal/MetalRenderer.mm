//
//  MetalRenderer.cpp
//  HopeEngine
//
//  Created by Ben Hopewell on 28/11/16.
//  Copyright © 2016 Ben Hopewell. All rights reserved.
//

#include "MetalRenderer.h"
#include "RenderCore/Renderer/Metal/MetalRendererImpl.h"

#include "Engine.h"

#ifdef HOPE_METAL

namespace Hope
{
    namespace Graphics
    {
        struct MetalRendererProxy
        {
            MetalRendererImpl* wrapped;
        };
        
        MetalRenderer::MetalRenderer(IDeviceContext* deviceContext) :
            IRenderer(),
            mDeviceContext(deviceContext),
            mRenderer(new MetalRendererProxy())
        {
            mRenderer->wrapped = [[MetalRendererImpl alloc]init];
        }
        
        MetalRenderer::~MetalRenderer()
        {
        }
        
        void MetalRenderer::Resize(u32 w, u32 h)
        {
            
        }
        
        bool MetalRenderer::Initialise()
        {
            return [mRenderer->wrapped Initialise:mDeviceContext];
        }
        
        void MetalRenderer::PreTick(f32 deltaTime)
        {
            HOPE_UNUSED(deltaTime)
            
            mDeviceContext->MakeCurrent();
        }
        
        void MetalRenderer::Tick(f32 deltaTime)
        {
            HOPE_UNUSED(deltaTime)
            
            [mRenderer->wrapped PreProcess];
            
            [mRenderer->wrapped Render];
            
            [mRenderer->wrapped PostProcess];
        }
        
        void MetalRenderer::PostTick(f32 deltaTime)
        {
            HOPE_UNUSED(deltaTime)
            
            [mRenderer->wrapped Present];
        }
        
        void MetalRenderer::Shutdown()
        {
            [mRenderer->wrapped Shutdown];
        }
    }
}

@implementation MetalRendererImpl

-(bool)Initialise:(Hope::Graphics::IDeviceContext *)deviceContext
{
    mDeviceContext = (Hope::Graphics::MetalDeviceContext*)deviceContext;
    if(!mDeviceContext->Initialise())
    {
        return false;
    }
    
    mWindow = (const Hope::Core::OSXWindow*)Hope::Engine::GetWindow();
    
    [self buildBuffers];
    
    return true;
}

-(void)buildBuffers
{
    static const f32 positions[] =
    {
        0.0,  0.5, 0, 1,
        -0.5, -0.5, 0, 1,
        0.5, -0.5, 0, 1,
    };
    
    static const f32 colors[] =
    {
        1, 0, 0, 1,
        0, 1, 0, 1,
        0, 0, 1, 1,
    };
    
    //Create the vertex buffer
    positionBuffer = [mDeviceContext->GetDevice() newBufferWithBytes:positions
                                        length:sizeof(positions)
                                        options:MTLResourceOptionCPUCacheModeDefault];

    //Create the color buffer
    colorBuffer = [mDeviceContext->GetDevice() newBufferWithBytes:colors
                                    length:sizeof(colors)
                                    options:MTLResourceOptionCPUCacheModeDefault];
}

-(void)PreProcess
{
    mDeviceContext->Clear();
    
    //TODO(Ben): Implement z sorting
    //TODO(Ben): Implement deferred render pipeline
}

-(void)Render
{
    //Render the buffer
    mDeviceContext->PushBuffer(positionBuffer, colorBuffer);
}

-(void)PostProcess
{
    //TODO(Ben): Implement Post-processing pipeline
}

-(void) Present
{
    //Swap the frame buffers
    mDeviceContext->SwapFrameBuffers();
}

-(void)Shutdown
{   
    mDeviceContext->Shutdown();
    
    if(mWindow != nullptr)
        mWindow = nullptr;
}

@end

#endif
