//
//  MetalRenderer.h
//  HopeEngine
//
//  Created by Ben Hopewell on 28/11/16.
//  Copyright © 2016 Ben Hopewell. All rights reserved.
//

#pragma once

#if defined(__APPLE__) && defined(HOPE_METAL)

#include "../IRenderer.h"
#include <memory>

namespace Hope
{
    namespace Graphics
    {
        struct MetalRendererProxy;
        
        class IDeviceContext;
        
        //Wrapper for Obj-C++ renderer
        class MetalRenderer : public IRenderer
        {
        public:
            MetalRenderer(IDeviceContext* deviceContext);
            virtual ~MetalRenderer();
            
            virtual void Resize(u32 w, u32 h) override;
            
            virtual void PreTick(f32 deltaTime) override;
            virtual void Tick(f32 deltaTime) override;
            virtual void PostTick(f32 deltaTime) override;
            
            virtual bool Initialise() override;
            virtual void Shutdown() override;
            
        private:
            IDeviceContext* mDeviceContext;
            std::unique_ptr<MetalRendererProxy> mRenderer;
        };
    }
}

#endif
