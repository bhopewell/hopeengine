//
//  IOSXRendererImpl.h
//  HopeEngine
//
//  Created by Ben Hopewell on 20/8/17.
//  Copyright © 2017 Ben Hopewell. All rights reserved.
//

#ifdef __APPLE__

#pragma once

namespace Hope {
    namespace Graphics {
        class IDeviceContext;
    }
}

@protocol IOSXRendererImpl

-(bool) Initialise:(Hope::Graphics::IDeviceContext*) deviceContext;
-(void) PreProcess;
-(void) Render;
-(void) PostProcess;
-(void) Present;
-(void) Shutdown;

@end

#endif
