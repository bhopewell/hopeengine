//
//  OpenGLShader.hpp
//  HopeEngine
//
//  Created by Ben Hopewell on 10/10/18.
//  Copyright © 2018 Ben Hopewell. All rights reserved.
//

#pragma once

#include "IShader.h"

#include <OpenGL/gl3.h>

namespace Hope {
    namespace Graphics {
        
        class OpenGLShader : public IShader
        {
        public:
            OpenGLShader() = delete;
            OpenGLShader(const char* vertShaderFileName, const char* fragShaderFileName);
            ~OpenGLShader();
            
            virtual bool Link() override;
            
            virtual void Begin() override;
            virtual void End() override;
            
            virtual bool SetUniform() override;
            
            virtual bool SetTexture() override;
            virtual bool SetMultiSampleTexture() override;
            
        private:
            bool LoadCompileAndAttachShader(const char* filename, GLenum shaderType);
            
        private:
            GLuint mProgramId;
            const char* mShaderContent;
        };
    }
}
