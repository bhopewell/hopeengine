//
//  OpenGLShader.cpp
//  HopeEngine
//
//  Created by Ben Hopewell on 10/10/18.
//  Copyright © 2018 Ben Hopewell. All rights reserved.
//

#include "OpenGLShader.h"

#include <iostream>
#include <fstream>

#include "Core/FileManagement/Unix/UnixFileReader.h"

using namespace Hope::Graphics;
using namespace Hope::Core;

static std::string getShaderInfoLog(GLuint obj)
{
    int logLength = 0;
    glGetShaderiv(obj, GL_INFO_LOG_LENGTH, &logLength);
    
    std::string log;
    if (logLength > 0)
    {
        char *tmpLog = new char[logLength];
        int charsWritten  = 0;
        glGetShaderInfoLog(obj, logLength, &charsWritten, tmpLog);
        log = tmpLog;
        delete [] tmpLog;
    }
    
    return log;
}


OpenGLShader::OpenGLShader(const char* vertShaderFileName, const char* fragShaderFileName)
{
    char *version = (char*)glGetString(GL_VERSION);
    std::cout << "OpenGLVersion=" << *version << std::endl;
    
    mProgramId = glCreateProgram();
    
    LoadCompileAndAttachShader(vertShaderFileName, GL_VERTEX_SHADER);
    LoadCompileAndAttachShader(fragShaderFileName, GL_FRAGMENT_SHADER);
}

OpenGLShader::~OpenGLShader()
{
    delete mShaderContent;
}

bool OpenGLShader::LoadCompileAndAttachShader(const char *filename, GLenum shaderType)
{
    GLuint shader = glCreateShader(shaderType);
    
    FileReader fileReader;
    mShaderContent = fileReader.ReadSync(filename);
    
    if(mShaderContent == nullptr)
    {
        std::cout << "Failed to read shader file=" << filename << std::endl;
        return false;
    }
    
    //TODO(Ben): Load and assign the shader source
    
    GLint sourceLen = (GLint)strlen(mShaderContent);
    glShaderSource(shader, 1, &mShaderContent, &sourceLen);
    
    glCompileShader(shader);
    
    GLint compileSuccess = 0;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &compileSuccess);
    
    if(!compileSuccess)
    {
        std::cout << "Shader compile err=" << getShaderInfoLog(shader) << std::endl;
        return false;
    }
    
    glAttachShader(mProgramId, shader);
    glDeleteShader(shader);
    
    return true;
}

bool OpenGLShader::Link()
{
    glLinkProgram(mProgramId);
    
    GLint linkSuccess = 0;
    glGetProgramiv(mProgramId, GL_LINK_STATUS, &linkSuccess);
    
    if(linkSuccess == 0)
    {
        return false;
    }
    
    return true;
}

void OpenGLShader::Begin()
{
    glUseProgram(mProgramId);
}

void OpenGLShader::End()
{
    glUseProgram(0);
}

bool OpenGLShader::SetUniform()
{
    return false;
}

bool OpenGLShader::SetTexture()
{
    return false;
}

bool OpenGLShader::SetMultiSampleTexture()
{
    return false;
}
