//
//  IShaderProgram.h
//  HopeEngine
//
//  Created by Ben Hopewell on 26/8/17.
//  Copyright © 2017 Ben Hopewell. All rights reserved.
//

#pragma once

namespace Hope {
    namespace Graphics {
        
        class IShader
        {
        public:
            virtual bool Link() = 0;
            
            virtual void Begin() = 0;
            virtual void End() = 0;
            
            virtual bool SetUniform() = 0;
            
            virtual bool SetTexture() = 0;
            virtual bool SetMultiSampleTexture() = 0;
        };
    }
}
