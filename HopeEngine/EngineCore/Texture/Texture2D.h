//
//  Texture2D.h
//  HopeEngine
//
//  Created by Ben Hopewell on 26/8/17.
//  Copyright � 2017 Ben Hopewell. All rights reserved.
//

#pragma once

namespace Hope {
	namespace EngineCore{

		class Texture2D
		{
		public:
			Texture2D();
			~Texture2D();
		};
	}
}
