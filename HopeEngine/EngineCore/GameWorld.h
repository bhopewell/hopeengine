//
//  GameWorld.h
//  HopeEngine
//
//  Created by Ben Hopewell on 4/11/18.
//  Copyright © 2018 Ben Hopewell. All rights reserved.
//

#pragma once

#include "Hope.h"
#include <assert.h>

#include "Entities/EntityManager.h"
#include "Components/ComponentManager.h"

namespace Hope {
    namespace Core {
        class HopeAllocator;
        class LinearAllocator;
    }
    
    namespace EngineCore {
        
        class GameWorld
        {
        public:
            
            //Construct the Game World based on the World data file
            static GameWorld* ConstructGameWorld(Core::HopeAllocator* systemAllocator);
            
            //Destroy the Game World
            void Destroy();
            
            //GameWorld destructor
            ~GameWorld();
            
        public:
            
            //Helper method to create Entities
            //Allocates the Entity using the EntityManager Allocator
            //Registers the Entity with the EntityManager
            template<typename T, typename ...P>
            inline T* ConstructEntity(P&& ...params);
            
            //Helper method to find entity of type with entityId
            template<class T>
            inline T* FindEntity(const HEntityId& entityId);
            
            //Helper method to create Components
            //Allocates the Component using the ComponentManager Allocator
            //Registers the Component with the ComponentManager
            template<typename T, typename ...P>
            inline T* ConstructComponent(P&& ...params);
            
            //Helper method to find component of type with componentId
            template<class T>
            inline T* FindComponent(const HComponentId& componentId);
            
            //Helper method to get all components of type
            template<class T>
            inline T* GetComponentsOfType();
            
        private:
            GameWorld(Core::HopeAllocator* systemAllocator);
            
        private:
            
            Core::HopeAllocator* mSystemAllocator;
            
            EntityManager* mEntityManager;
            ComponentManager* mComponentManager;
        };   
    }
}

#include "GameWorld.inl"
