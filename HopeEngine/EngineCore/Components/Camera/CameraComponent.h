//
//  CameraComponent.h
//  HopeEngine
//
//  Created by Ben Hopewell on 12/8/17.
//  Copyright © 2017 Ben Hopewell. All rights reserved.
//

#pragma once

#include <glm/mat4x4.hpp>
#include "../IComponent.h"

namespace Hope {
    namespace EngineCore {
        
        class CameraComponent : public IComponent
        {
            HOPE_COMPONENT(CameraComponent, IComponent);
            
        public:
            CameraComponent();
            CameraComponent(f32 near, f32 far, f32 aspect, f32 fieldOfView);
            CameraComponent(const CameraComponent& other) = delete;
			virtual ~CameraComponent() = default;
            
            virtual void Tick(f32 deltaTime) override;
            
            inline f32 GetNear() const;
            inline f32 GetFar() const;
            inline f32 GetAspect() const;
            inline f32 GetFieldOfView() const;
            
            inline const glm::mat4& GetView() const;
            inline const glm::mat4& GetProjection() const;
            
            inline void SetNear(f32 near);
            inline void SetFar(f32 far);
            inline void SetAspect(f32 aspect);
            inline void SetFieldOfView(f32 fieldOfView);
            
        protected:
            void RecalculateFrustum();
            
        protected:
            bool mIsDirty;
            
            f32 mNearPlane;
            f32 mFarPlane;
            f32 mAspectRatio;
            f32 mFieldOfView;
            
            glm::mat4 mView;
            glm::mat4 mProjection;
        };
    }
}

#include "CameraComponent.inl"
