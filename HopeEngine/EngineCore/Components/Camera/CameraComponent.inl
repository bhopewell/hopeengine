//
//  CameraComponent.inl
//  HopeEngine
//
//  Created by Ben Hopewell on 12/8/17.
//  Copyright © 2017 Ben Hopewell. All rights reserved.
//

#pragma once

//Getters
inline f32 Hope::EngineCore::CameraComponent::GetNear() const
{
    return mNearPlane;
}

inline f32 Hope::EngineCore::CameraComponent::GetFar() const
{
    return mFarPlane;
}

inline f32 Hope::EngineCore::CameraComponent::GetAspect() const
{
    return mAspectRatio;
}

inline f32 Hope::EngineCore::CameraComponent::GetFieldOfView() const
{
    return mFieldOfView;
}

inline const glm::mat4& Hope::EngineCore::CameraComponent::GetView() const
{
    return mView;
}

inline const glm::mat4& Hope::EngineCore::CameraComponent::GetProjection() const
{
    return mProjection;
}

//Setters
//Each setter will flag the camera as dirty.
//On the next update, the camera will recalculate the frustum
inline void Hope::EngineCore::CameraComponent::SetNear(f32 near)
{
    mNearPlane = near;
    mIsDirty = true;
}

inline void Hope::EngineCore::CameraComponent::SetFar(f32 far)
{
    mFarPlane = far;
    mIsDirty = true;
}

inline void Hope::EngineCore::CameraComponent::SetAspect(f32 aspect)
{
    mAspectRatio = aspect;
    mIsDirty = true;
}

inline void Hope::EngineCore::CameraComponent::SetFieldOfView(f32 fieldOfView)
{
    mFieldOfView = fieldOfView;
    mIsDirty = true;
}
