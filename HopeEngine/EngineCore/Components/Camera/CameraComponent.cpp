//
//  CameraComponent.cpp
//  HopeEngine
//
//  Created by Ben Hopewell on 12/8/17.
//  Copyright © 2017 Ben Hopewell. All rights reserved.
//

#include "CameraComponent.h"

using namespace Hope::EngineCore;

CameraComponent::CameraComponent() :
    mIsDirty(false),
    mNearPlane(0.f),
    mFarPlane(0.f),
    mAspectRatio(0.f),
    mFieldOfView(0.f)
{
    
}

CameraComponent::CameraComponent(f32 near, f32 far, f32 aspect, f32 fieldOfView) :
    mIsDirty(true),
    mNearPlane(near),
    mFarPlane(far),
    mAspectRatio(aspect),
    mFieldOfView(fieldOfView)
{
    
}

void CameraComponent::Tick(f32 deltaTime)
{
    HOPE_UNUSED(deltaTime);
    
    if(mIsDirty)
    {
        RecalculateFrustum();
    }
}

void CameraComponent::RecalculateFrustum()
{
    
}
