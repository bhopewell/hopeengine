//
//  ComponentManager.h
//  HopeEngine
//
//  Created by Ben Hopewell on 11/12/18.
//  Copyright © 2018 Ben Hopewell. All rights reserved.
//

#pragma once

#include "Hope.h"
#include "IComponent.h"
#include "Core/Handle/HandleManager.h"
#include "Core/Memory/Allocators/DynamicPoolAllocator.h"

#include <assert.h>
#include <unordered_map>

namespace Hope {
    namespace EngineCore {
        
        //Component Container Interface
        class IComponentContainer
        {
        public:
            virtual ~IComponentContainer() { }
            
            virtual void DestroyComponent(IComponent* component) = 0;
        };
        
        //Define the component iterator
        template <class T>
        class ComponentIterator : public Core::DynamicPoolAllocator::DynamicPoolIterator<T>
        {
        public:
            ComponentIterator<T>(Core::DynamicPoolAllocator::DynamicPoolIterator<T> it) :
                Core::DynamicPoolAllocator::DynamicPoolIterator<T>(it)
            {
            }
        };
        
        //Component Container concrete implementation
        template <class T>
        class ComponentContainer : public IComponentContainer
        {
        public:
            static const u32 DEFAULT_COMPONENT_COUNT = 100;
            
        public:
            ComponentContainer();
            ~ComponentContainer();
            
            void* CreateObjectMemory();
            virtual void DestroyComponent(IComponent* component) override;
            
            inline ComponentIterator<T> begin() { return ComponentIterator<T>(mAllocator.begin<T>()); }
            inline ComponentIterator<T> end() { return ComponentIterator<T>(mAllocator.end<T>()); }
            
        private:
            Core::DynamicPoolAllocator mAllocator;
        };
        
        //Component Manager implementation
        class ComponentManager
        {   
        public:
            ComponentManager();
            ComponentManager(ComponentManager&) = delete;
            ComponentManager(ComponentManager&&) = delete;
            
            ~ComponentManager();
            
            //Helper method to create Components
            //Allocates the Component using the ComponentManager Allocator
            //Registers the Component with the ComponentManager
            template<class T, typename ...P>
            inline T* ConstructComponent(P&& ...params);
            
            //Helper method to find component of type with componentId
            template<class T>
            inline T* FindComponent(const HComponentId& componentId);
            
            IComponent* FindComponent(const HComponentId& componentId);
            
            //Helper method to destroy Components
            //Removes the ComponentId from the ComponentHandleManager and destroys the component
            void DestroyComponent(IComponent* component);
            
            template<class T>
            inline ComponentIterator<T> begin() { return GetComponentContainer<T>()->begin(); }
            
            template<class T>
            inline ComponentIterator<T> end() { return GetComponentContainer<T>()->end(); }
            
        private:
            template <class T>
            ComponentContainer<T>* GetComponentContainer();
            IComponentContainer* GetComponentContainer(IComponent* component);
            
        private:
            std::unordered_map<StaticTypeId, IComponentContainer*> mComponentContainers;
            Core::HHandleManager mComponentHandleManager;
        };
    }
}

#include "ComponentManager.inl"
