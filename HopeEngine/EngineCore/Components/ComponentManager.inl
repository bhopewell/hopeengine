//
//  ComponentManager.inl
//  HopeEngine
//
//  Created by Ben Hopewell on 11/12/18.
//  Copyright © 2018 Ben Hopewell. All rights reserved.
//

#pragma once

//--------------------------------------------------
//              Component Container
//--------------------------------------------------
template<class T>
Hope::EngineCore::ComponentContainer<T>::ComponentContainer() :
//Default the allocator to initialise to support 10 of the provided component
mAllocator(sizeof(T))
{
}

template<class T>
Hope::EngineCore::ComponentContainer<T>::~ComponentContainer()
{
    mAllocator.Clear();
}

template<class T>
void* Hope::EngineCore::ComponentContainer<T>::CreateObjectMemory()
{
    return mAllocator.AllocateObject();
}

template<class T>
void Hope::EngineCore::ComponentContainer<T>::DestroyComponent(IComponent *component)
{
    component->~IComponent();
    mAllocator.Free(component);
}

//--------------------------------------------------
//              Component Manager
//--------------------------------------------------
template<class T>
inline Hope::EngineCore::ComponentContainer<T>* Hope::EngineCore::ComponentManager::GetComponentContainer()
{
    assert(T::GetRTTI().DerivesFrom(IComponent::GetRTTI()));
    
    std::unordered_map<StaticTypeId, IComponentContainer*>::const_iterator it = mComponentContainers.find(T::GetStaticTypeId());
    if(it == mComponentContainers.end())
    {
        //Create a new Component Container
        std::pair<std::unordered_map<StaticTypeId, IComponentContainer*>::const_iterator, bool> result =
            mComponentContainers.emplace(T::GetStaticTypeId(), new ComponentContainer<T>());
        
        //Verify the insertion
        assert(result.second);
        
        it = result.first;
    }
    
    //Ensure the container exists
    assert(it != mComponentContainers.end() && it->second != nullptr);
    
    //Return the Component Container
    return static_cast<ComponentContainer<T>*>(it->second);
}

template<class T, typename ...P>
inline T* Hope::EngineCore::ComponentManager::ConstructComponent(P&& ...params)
{    
    assert(T::GetRTTI().DerivesFrom(IComponent::GetRTTI()));
    
    //Obtain the memory for the component and construct in-place
    void* objMemory = GetComponentContainer<T>()->CreateObjectMemory();
    T* component = new (objMemory) (T)(std::forward<P>(params)...);
    
    //Generate the componentId and initialise the component with the componentId and owner
    HComponentId componentId = mComponentHandleManager.Add(component, T::GetStaticTypeId());
    component->mComponentId = componentId;
    component->mOwner = nullptr;
    
    return component;
}

template<class T>
inline T* Hope::EngineCore::ComponentManager::FindComponent(const HComponentId& componentId)
{
    assert(T::GetRTTI().DerivesFrom(IComponent::GetRTTI()));
    return static_cast<T*>(mComponentHandleManager.Get(componentId));
}
