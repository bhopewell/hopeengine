//
//  ComponentManager.cpp
//  HopeEngine
//
//  Created by Ben Hopewell on 11/12/18.
//  Copyright © 2018 Ben Hopewell. All rights reserved.
//

#include "ComponentManager.h"

using namespace Hope::EngineCore;

ComponentManager::ComponentManager() :
    mComponentContainers(),
    mComponentHandleManager()
{
}

ComponentManager::~ComponentManager()
{
    std::unordered_map<StaticTypeId, IComponentContainer*>::iterator it = mComponentContainers.begin();
    for(; it != mComponentContainers.end(); ++it)
        delete it->second;
    
    mComponentContainers.clear();
}

IComponent* Hope::EngineCore::ComponentManager::FindComponent(const HComponentId& componentId)
{
    return static_cast<IComponent*>(mComponentHandleManager.Get(componentId));
}

//Helper method to destroy Components
//Removes the ComponentId from the ComponentHandleManager and destroys the component
void Hope::EngineCore::ComponentManager::DestroyComponent(IComponent* component)
{
    assert(component->GetRTTI().DerivesFrom(IComponent::GetRTTI()));
    
    mComponentHandleManager.Remove(component->GetComponentId());
    this->GetComponentContainer(component)->DestroyComponent(component);
}

IComponentContainer* Hope::EngineCore::ComponentManager::GetComponentContainer(IComponent* component)
{
    assert(component->GetRTTI().DerivesFrom(IComponent::GetRTTI()));
    
    std::unordered_map<StaticTypeId, IComponentContainer*>::const_iterator it = mComponentContainers.find(component->GetStaticTypeId());
    if(it == mComponentContainers.end())
        return nullptr;
    
    //Ensure the container exists
    assert(it != mComponentContainers.end() && it->second != nullptr);
    
    //Return the Component Container
    return it->second;
}
