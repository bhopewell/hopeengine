//
//  SpotLightComponent.h
//  HopeEngine
//
//  Created by Ben Hopewell on 7/10/18.
//  Copyright © 2018 Ben Hopewell. All rights reserved.
//

#pragma once

#include "LightComponent.h"

namespace Hope {
    namespace EngineCore {
        
        class SpotLightComponent : public LightComponent
        {
            HOPE_COMPONENT(SpotLightComponent, LightComponent);
        public:
            SpotLightComponent() : LightComponent() { }
            SpotLightComponent(const glm::vec3& color) : LightComponent(color) { }
            virtual ~SpotLightComponent() { }
            
            virtual void Initialise() override;
            virtual void Tick(f32 deltaTime) override;
            virtual void Destroy() override;
        };
    }
}
