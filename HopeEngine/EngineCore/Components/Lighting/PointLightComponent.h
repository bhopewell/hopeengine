//
//  PointLightComponent.h
//  HopeEngine
//
//  Created by Ben Hopewell on 7/10/18.
//  Copyright © 2018 Ben Hopewell. All rights reserved.
//

#pragma once

#include "LightComponent.h"

namespace Hope {
    namespace EngineCore {
        
        class PointLightComponent : public LightComponent
        {
            HOPE_COMPONENT(PointLightComponent, LightComponent);
        public:
            PointLightComponent() : LightComponent() { }
            PointLightComponent(const glm::vec3& color) : LightComponent(color) { }
            virtual ~PointLightComponent() { }
            
            virtual void Initialise() override;
            virtual void Tick(f32 deltaTime) override;
            virtual void Destroy() override;
        };
    }
}
