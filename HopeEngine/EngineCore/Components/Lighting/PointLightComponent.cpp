//
//  PointLightComponent.cpp
//  HopeEngine
//
//  Created by Ben Hopewell on 7/10/18.
//  Copyright © 2018 Ben Hopewell. All rights reserved.
//

#include "PointLightComponent.h"

using namespace Hope::EngineCore;

void PointLightComponent::Initialise() { }
void PointLightComponent::Tick(f32 deltaTime) { HOPE_UNUSED(deltaTime) }
void PointLightComponent::Destroy() { }
