//
//  AmbientLightComponent.cpp
//  HopeEngine
//
//  Created by Ben Hopewell on 18/10/18.
//  Copyright © 2018 Ben Hopewell. All rights reserved.
//

#include "AmbientLightComponent.h"

using namespace Hope::EngineCore;

AmbientLightComponent::AmbientLightComponent() :
    LightComponent()
{
    
}

AmbientLightComponent::~AmbientLightComponent()
{
    
}

void AmbientLightComponent::Initialise() {}
void AmbientLightComponent::Tick(f32 deltaTime) { HOPE_UNUSED(deltaTime) }
void AmbientLightComponent::Destroy() {}
