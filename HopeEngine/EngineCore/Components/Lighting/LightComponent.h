//
//  Light.h
//  HopeEngine
//
//  Created by Ben Hopewell on 6/10/18.
//  Copyright © 2018 Ben Hopewell. All rights reserved.
//

#pragma once

#include <glm/vec3.hpp>
#include "Components/IComponent.h"

namespace Hope {
    namespace EngineCore {

        struct Attenuation
        {
            Attenuation()
            {}
            
            Attenuation(float constant, float linear, float quadratic)
            : m_fConstant(constant), m_fLinear(linear), m_fQuadratic(quadratic)
            {}
            
            float m_fConstant;
            float m_fLinear;
            float m_fQuadratic;
        };
        
        //Light class
        //Abstract class defining a simple light
        //template<typename T>
        class LightComponent : public Hope::EngineCore::IComponent
        {
            HOPE_COMPONENT(LightComponent, IComponent);
        public:
            LightComponent() : mEnabled(true), mColor(glm::vec3(1,1,1)) { }
            LightComponent(const glm::vec3& color) : mEnabled(true), mColor(color) { }
            virtual ~LightComponent() = 0;
            
            //Get methods
            inline bool Enabled() const { return mEnabled; }
            inline const glm::vec3& GetColor() const { return mColor; }
            
            //Set methods
            inline void Enable(bool enable) { mEnabled = enable; }
            inline void SetColor(glm::vec3& color) { mColor = color; }
            inline void SetColor(f32 r, f32 g, f32 b) { mColor.r = r; mColor.g = g; mColor.b = b; }
            
        protected:
            bool mEnabled;
            glm::vec3 mColor;
        };
        
        inline LightComponent::~LightComponent() { }
    }
}
