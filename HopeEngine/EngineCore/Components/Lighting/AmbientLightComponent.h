//
//  AmbientLightComponent.hpp
//  HopeEngine
//
//  Created by Ben Hopewell on 18/10/18.
//  Copyright © 2018 Ben Hopewell. All rights reserved.
//

#pragma once

#include "LightComponent.h"

namespace Hope {
    namespace EngineCore {
        //Ambient Light Class
        //This class is a non-abstract implementation of the Light base class
        class AmbientLightComponent : public LightComponent
        {
            HOPE_COMPONENT(AmbientLightComponent, LightComponent);
        public:
            AmbientLightComponent();
            AmbientLightComponent(const glm::vec3& color) : LightComponent(color) { }
            
            virtual ~AmbientLightComponent();
            
            virtual void Initialise() override;
            virtual void Tick(f32 deltaTime) override;
            virtual void Destroy() override;
        };
    }
}
