//
//  SpotLightComponent.cpp
//  HopeEngine
//
//  Created by Ben Hopewell on 7/10/18.
//  Copyright © 2018 Ben Hopewell. All rights reserved.
//

#include "SpotLightComponent.h"

using namespace Hope::EngineCore;

void SpotLightComponent::Initialise() { }
void SpotLightComponent::Tick(f32 deltaTime) { HOPE_UNUSED(deltaTime) }
void SpotLightComponent::Destroy() { }
