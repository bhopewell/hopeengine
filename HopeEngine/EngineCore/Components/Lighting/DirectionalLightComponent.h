//
//  DirectionalLight.h
//  HopeEngine
//
//  Created by Ben Hopewell on 7/10/18.
//  Copyright © 2018 Ben Hopewell. All rights reserved.
//

#pragma once

#include "LightComponent.h"
#include "Core/Math/Vector4.h"

namespace Hope {
    namespace EngineCore {
        class DirectionalLightComponent : public LightComponent
        {
            HOPE_COMPONENT(DirectionalLightComponent, LightComponent);
        public:
            DirectionalLightComponent() : LightComponent() { }
            DirectionalLightComponent(const Vector3& color) : LightComponent(color), mDirection(0,0,0,0) { }
            DirectionalLightComponent(const Vector3& color, const Vector4& dir) : LightComponent(color), mDirection(dir) { }

            virtual ~DirectionalLightComponent() = default;
            
            inline const Vector4& GetDirection() const { return mDirection; }
            inline void SetDirection(const Vector4& dir) { mDirection = dir; }
            
            virtual void Initialise() override {}
            virtual void Tick(f32 deltaTime) override {}
            virtual void Destroy() override {}
            
        private:
            Vector4 mDirection;
        };
    }
}
