//
//  Mesh.cpp
//  HopeEngine
//
//  Created by Ben Hopewell on 26/8/17.
//  Copyright © 2017 Ben Hopewell. All rights reserved.
//

#include "MeshComponent.h"

using namespace Hope::EngineCore;

MeshComponent::MeshComponent(const VertexBuffer& verts, const NormalBuffer& normals,
           const TexCoordBuffer& texCoords, const IndexBuffer& indices) :
mVertices{ verts },
mNormals{ normals },
mTexCoords{ texCoords },
mIndices{ indices }
{
    
}

MeshComponent::~MeshComponent()
{
    
}
