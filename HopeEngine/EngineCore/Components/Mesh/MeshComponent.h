//
//  Mesh.hpp
//  HopeEngine
//
//  Created by Ben Hopewell on 26/8/17.
//  Copyright © 2017 Ben Hopewell. All rights reserved.
//

#pragma once

#include "EngineCore/Material/Material.h"
#include "RenderCore/Buffers/VertexBuffer.h"
#include "RenderCore/Buffers/NormalBuffer.h"
#include "RenderCore/Buffers/TexCoordBuffer.h"
#include "RenderCore/Buffers/IndexBuffer.h"

#include "Components/IComponent.h"

namespace Hope {
    namespace EngineCore {
        
        class MeshComponent : public IComponent
        {
            HOPE_COMPONENT(MeshComponent, IComponent)
            
        public:
			MeshComponent(const VertexBuffer& verts, const NormalBuffer& normals,
                 const TexCoordBuffer& texCoords, const IndexBuffer& indices);
            ~MeshComponent();
            
            inline const VertexBuffer& GetVerts() const { return mVertices; }
            inline const NormalBuffer& GetNormals() const { return mNormals; }
            inline const TexCoordBuffer& GetTexCoords() const { return mTexCoords; }
            inline const IndexBuffer& GetIndices() const { return mIndices; }
            
            inline const Material* GetMaterial() const { return mMaterial; }
            inline void SetMaterial(Material* material) { mMaterial = material; }
            
        private:
            const VertexBuffer mVertices;
            const NormalBuffer mNormals;
			const TexCoordBuffer mTexCoords;
            const IndexBuffer mIndices;
            
            Material* mMaterial;
        };
    }
}
