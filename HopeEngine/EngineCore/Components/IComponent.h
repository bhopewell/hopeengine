//
//  IComponent.h
//  HopeEngine
//
//  Created by Ben Hopewell on 16/10/18.
//  Copyright © 2018 Ben Hopewell. All rights reserved.
//

#pragma once

#include "Core/RTTI/HopeRTTI.h"
#include "Core/Handle/Handle.h"

namespace Hope {
    namespace EngineCore {
        
        class IEntity;
        
        class IComponent
        {
            friend class ComponentManager;
            
            HOPE_BASE_CLASS(IComponent);
        public:
            IComponent() { }
            virtual ~IComponent() { }
            
            virtual void Initialise() = 0;
            virtual void Tick(f32 deltaTime) = 0;
            virtual void Destroy() = 0;
            
            inline const HComponentId GetComponentId() const { return mComponentId; }
            inline const IEntity* GetOwner() const { return mOwner; }
            inline void SetOwner(IEntity* owner) { mOwner = owner; }
            
        private:
            HComponentId mComponentId;
            IEntity* mOwner;
        };
    }
}
