//
//  Material.cpp
//  HopeEngine
//
//  Created by Ben Hopewell on 26/8/17.
//  Copyright © 2017 Ben Hopewell. All rights reserved.
//

#include "Material.h"

#include "RenderCore/Shader/IShader.h"

using namespace Hope::EngineCore;
using namespace Hope::Graphics;

Material::Material() :
mTextureHandle(0) //TODO(Ben): Is 0 an invalid texture handle?!
{
    
}

Material::Material(u32 textureHandle) :
mTextureHandle(textureHandle)
{
    
}

Material::~Material()
{
    
}

