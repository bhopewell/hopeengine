//
//  Material.hpp
//  HopeEngine
//
//  Created by Ben Hopewell on 26/8/17.
//  Copyright © 2017 Ben Hopewell. All rights reserved.
//

#pragma once

#include "Hope.h"
#include <glm/vec3.hpp>

namespace Hope {
    namespace EngineCore {
        
        class Material
        {
        public:
            Material();
            Material(u32 textureHandle);
            ~Material();
            
            inline const u32 GetTexture() const { return mTextureHandle; }
            inline void SetTexture(u32 textureHandle) { mTextureHandle = textureHandle; }
            
        private:
            u32 mTextureHandle;
            
            glm::vec3 mDiffuse;
            //glm::vec3 mMetallic;
            glm::vec3 mSpecular;
            //glm::vec3 mEmmissive;
            
            float mRoughness; //NOTE: Inverse of shininess
        };
    }
}
