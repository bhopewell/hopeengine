//
//  HopeTime.h
//  HopeEngine
//
//  Created by Ben Hopewell on 07/05/2019.
//

#pragma once

#include <chrono>

// Define the time types
typedef std::chrono::duration<f32                                >      Sec;
typedef std::chrono::duration<f32,  std::ratio<1, 1000>          >      MSec;
typedef std::chrono::duration<f32,  std::ratio<1, 1000000>       >      USec;
typedef std::chrono::duration<f32,  std::ratio<1, 1000000000>    >      NSec;

// Define the TimeCast functions
template<typename T>
static T TimeCast(const Sec& sec) { return std::chrono::duration_cast<T>(sec); }
template<typename T>
static T TimeCast(const MSec& msec) { return std::chrono::duration_cast<T>(msec); }
template<typename T>
static T TimeCast(const USec& usec) { return std::chrono::duration_cast<T>(usec); }
template<typename T>
static T TimeCast(const NSec& nsec) { return std::chrono::duration_cast<T>(nsec); }

static NSec TimeSinceEpoch() { return std::chrono::high_resolution_clock::now().time_since_epoch(); }
