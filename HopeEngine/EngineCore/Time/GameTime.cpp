//
//  GameTime.cpp
//  HopeEngine
//
//  Created by Ben Hopewell on 06/05/2019.
//

#include "GameTime.h"

f32 GameTime::mDeltaTime = 0.0f;
NSec GameTime::mLastUpdateTime = TimeSinceEpoch();

void GameTime::Tick()
{
    // Calculate the delta time
    NSec nsecSinceEpoch = TimeSinceEpoch();
    NSec delta = nsecSinceEpoch - mLastUpdateTime;
    
    // Update the stored delta time
    mDeltaTime = TimeCast<Sec>(delta).count();
    mLastUpdateTime = nsecSinceEpoch;
}
