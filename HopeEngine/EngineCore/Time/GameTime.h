//
//  GameTime.h
//  HopeEngine
//
//  Created by Ben Hopewell on 06/05/2019.
//

#pragma once

#include "Hope.h"
#include "HopeTime.h"

// Super basic game timer
class GameTime
{
public:
    static void Tick();
    
    static f32 GetDeltaTime() { return mDeltaTime; };
    
private:
    static f32 mDeltaTime;
    static NSec mLastUpdateTime;
};
