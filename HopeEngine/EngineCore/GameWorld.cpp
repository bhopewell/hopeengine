//
//  GameWorld.cpp
//  HopeEngine
//
//  Created by Ben Hopewell on 4/11/18.
//  Copyright © 2018 Ben Hopewell. All rights reserved.
//

#include "GameWorld.h"
#include "Core/Memory/Allocators/LinearAllocator.h"

using namespace Hope::EngineCore;
using namespace Hope::Core;

GameWorld* GameWorld::ConstructGameWorld(HopeAllocator* allocator)
{
    GameWorld* gameWorld = new (allocator) GameWorld(allocator);
    return gameWorld;
}

#include "EngineCore/Components/Lighting/AmbientLightComponent.h"
#include "EngineCore/Entities/GameObject/GameObject.h"
#include <iostream>

GameWorld::GameWorld(HopeAllocator* allocator) :
    mSystemAllocator(allocator)
{
    mComponentManager = new (mSystemAllocator) ComponentManager();
    mEntityManager = new (mSystemAllocator) EntityManager(mComponentManager);
    
    //Test GameObject creation
    ConstructEntity<GameObject>();
    ConstructEntity<GameObject>();
    
    //Test entity iteration - and attaching components to entities
    for(EntityIterator<GameObject> it = mEntityManager->begin<GameObject>();
        it != mEntityManager->end<GameObject>(); ++it)
    {
        it->AttachComponent(ConstructComponent<AmbientLightComponent>(glm::vec3(0,0,0)));
        std::cout << "EntityId=" << it->GetEntityId() << std::endl;
    }
    
    //Test component iteration.
    for(ComponentIterator<AmbientLightComponent> it = mComponentManager->begin<AmbientLightComponent>();
        it != mComponentManager->end<AmbientLightComponent>();
        ++it)
    {   
        const glm::vec3& color = it->GetColor();
        std::cout << "ComponentId=" << it->GetComponentId() << " R=" << color.r << " G=" << color.g << " B=" << color.b << std::endl;
    }
}

GameWorld::~GameWorld()
{
}

void GameWorld::Destroy()
{
    mSystemAllocator->Free(mEntityManager);
    mSystemAllocator->Free(mComponentManager);
}
