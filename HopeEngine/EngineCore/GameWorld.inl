//
//  GameWorld.inl
//  HopeEngine
//
//  Created by Ben Hopewell on 4/11/18.
//  Copyright © 2018 Ben Hopewell. All rights reserved.
//

#pragma once

template<class T, typename ...P>
inline T* Hope::EngineCore::GameWorld::ConstructEntity(P&& ...params)
{
    return mEntityManager->ConstructEntity<T>(params...);
}

template<class T>
inline T* Hope::EngineCore::GameWorld::FindEntity(const HEntityId& entityId)
{
    return mEntityManager->FindEntity<T>(entityId);
}

template<class T, typename ...P>
inline T* Hope::EngineCore::GameWorld::ConstructComponent(P&& ...params)
{
    return mComponentManager->ConstructComponent<T>(params...);
}

template<class T>
inline T* Hope::EngineCore::GameWorld::FindComponent(const HComponentId& componentId)
{
    return mComponentManager->FindComponent<T>(componentId);
}
