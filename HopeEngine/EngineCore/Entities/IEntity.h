//
//  IEntity.h
//  HopeEngine
//
//  Created by Ben Hopewell on 16/10/18.
//  Copyright © 2018 Ben Hopewell. All rights reserved.
//

#pragma once

#include "Core/Handle/Handle.h"
#include "EngineCore/Components/IComponent.h"
#include <vector>
#include <unordered_map>

namespace Hope {
    namespace EngineCore {
        
        class IComponent;
        class EntityManager;
        
        class IEntity
        {
            friend class EntityManager;
            
            HOPE_BASE_CLASS(IEntity)
        public:
            virtual ~IEntity() { };

            inline const HEntityId GetEntityId() const { return mEntityId; }
            
            template<typename T>
            inline std::vector<T*> GetComponentsOfType();
            inline IComponent* GetComponent(const HComponentId& componentId);
            
            inline std::vector<IComponent*> GetAllComponents() const;
            
            inline void AttachComponent(IComponent* component);
            inline void RemoveComponent(IComponent* component);
            
            virtual void Tick(f32 deltaTime) = 0;
            
        private:
            HEntityId mEntityId;
            std::unordered_map<HComponentId, IComponent*> mComponents;
        };
    }
}

#include "IEntity.inl"
