//
//  EntityManager.inl
//  HopeEngine
//
//  Created by Ben Hopewell on 11/12/18.
//  Copyright © 2018 Ben Hopewell. All rights reserved.
//

#pragma once

#include "EngineCore/Components/ComponentManager.h"

//--------------------------------------------------
//              Entity Container
//--------------------------------------------------
template<class T>
Hope::EngineCore::EntityContainer<T>::EntityContainer() :
//Default the allocator to initialise to support 10 of the provided component
mAllocator(sizeof(T))
{
}

template<class T>
Hope::EngineCore::EntityContainer<T>::~EntityContainer()
{
    mAllocator.Clear();
}

template<class T>
void* Hope::EngineCore::EntityContainer<T>::CreateObjectMemory()
{
    return mAllocator.AllocateObject();
}

template<class T>
void Hope::EngineCore::EntityContainer<T>::DestroyEntity(IEntity* entity)
{
    entity->~IEntity();
    mAllocator.Free(entity);
}

//--------------------------------------------------
//              Entity Manager
//--------------------------------------------------
template<class T>
inline Hope::EngineCore::EntityContainer<T>* Hope::EngineCore::EntityManager::GetEntityContainer()
{
    assert(T::GetRTTI().DerivesFrom(IEntity::GetRTTI()));
    
    std::unordered_map<StaticTypeId, IEntityContainer*>::const_iterator it = mEntityContainers.find(T::GetStaticTypeId());
    if(it == mEntityContainers.end())
    {
        //Create a new Component Container
        std::pair<std::unordered_map<StaticTypeId, IEntityContainer*>::const_iterator, bool> result =
        mEntityContainers.emplace(T::GetStaticTypeId(), new EntityContainer<T>());
        
        //Verify the insertion
        assert(result.second);
        
        it = result.first;
    }
    
    //Ensure the container exists
    assert(it != mEntityContainers.end() && it->second != nullptr);
    
    //Return the Component Container
    return static_cast<EntityContainer<T>*>(it->second);
}

template<class T, typename ...P>
inline T* Hope::EngineCore::EntityManager::ConstructEntity(P&& ...params)
{
    assert(T::GetRTTI().DerivesFrom(IEntity::GetRTTI()));
    
    void* objMemory = GetEntityContainer<T>()->CreateObjectMemory();
    T* entity = new (objMemory) (T)(std::forward<P>(params)...);
    
    //Generate the entityId and initialise the entity with it
    HEntityId entityId = mEntityHandleManager.Add(entity, T::GetStaticTypeId());
    entity->mEntityId = entityId;
    
    return entity;
}

template<class T>
inline T* Hope::EngineCore::EntityManager::FindEntity(const HEntityId& entityId)
{
    assert(T::GetRTTI().DerivesFrom(IEntity::GetRTTI()));
    return static_cast<T*>(mEntityHandleManager.Get(entityId));
}
