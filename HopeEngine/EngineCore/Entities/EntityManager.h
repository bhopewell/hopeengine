//
//  EntityManager.h
//  HopeEngine
//
//  Created by Ben Hopewell on 11/12/18.
//  Copyright © 2018 Ben Hopewell. All rights reserved.
//

#pragma once

#include "Hope.h"
#include "IEntity.h"
#include "Core/Handle/HandleManager.h"
#include "Core/Memory/Allocators/DynamicPoolAllocator.h"

#include <assert.h>
#include <unordered_map>

namespace Hope {
    namespace EngineCore {
        
        class ComponentManager;
        
        //Entity Container Interface
        class IEntityContainer
        {
        public:
            virtual ~IEntityContainer() { }
            
            virtual void DestroyEntity(IEntity* entity) = 0;
        };
        
        //Define the entity iterator
        template <class T>
        class EntityIterator : public Core::DynamicPoolAllocator::DynamicPoolIterator<T>
        {
        public:
            EntityIterator<T>(Core::DynamicPoolAllocator::DynamicPoolIterator<T> it) :
                Core::DynamicPoolAllocator::DynamicPoolIterator<T>(it)
            {
            }
        };
        
        //Entity Container concrete implementation
        template <class T>
        class EntityContainer : public IEntityContainer
        {
        public:
            static const u32 DEFAULT_ENTITY_COUNT = 100;
            
        public:
            EntityContainer();
            ~EntityContainer();
            
            void* CreateObjectMemory();
            virtual void DestroyEntity(IEntity* component) override;
            
            inline EntityIterator<T> begin() { return EntityIterator<T>(mAllocator.begin<T>()); }
            inline EntityIterator<T> end() { return EntityIterator<T>(mAllocator.end<T>()); }
            
        private:
            Core::DynamicPoolAllocator mAllocator;
        };
        
        class EntityManager
        {
        public:
            EntityManager(ComponentManager* componentManager);
            EntityManager(EntityManager&) = delete;
            EntityManager(EntityManager&&) = delete;
            
            ~EntityManager();
            
            //Helper method to create Entities
            //Allocates the Entity using the EntityManager Allocator
            //Registers the Entity with the EntityManager
            template<typename T, typename ...P>
            inline T* ConstructEntity(P&& ...params);
            
            //Helper method to find entity of type with entityId
            template<class T>
            inline T* FindEntity(const HEntityId& entityId);
            
            IEntity* FindEntity(const HEntityId& entityId);
            
            //Helper method to destroy Entities
            //Removes the Entity from the EntityHandleManager and destroys the entity
            void DestroyEntity(IEntity* entity);
            
            template<class T>
            inline EntityIterator<T> begin() { return GetEntityContainer<T>()->begin(); }
            
            template<class T>
            inline EntityIterator<T> end() { return GetEntityContainer<T>()->end(); }
            
        private:
            template <class T>
            EntityContainer<T>* GetEntityContainer();
            IEntityContainer* GetEntityContainer(IEntity* entity);
            
        private:
            std::unordered_map<StaticTypeId, IEntityContainer*> mEntityContainers;
            Core::HHandleManager mEntityHandleManager;
            
            ComponentManager* mComponentManager;
        };
    }
}

#include "EntityManager.inl"
