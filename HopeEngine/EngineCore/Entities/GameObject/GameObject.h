//
//  GameObject.h
//  HopeEngine
//
//  Created by Ben Hopewell on 26/8/17.
//  Copyright � 2017 Ben Hopewell. All rights reserved.
//

#pragma once

#include "EngineCore/Entities/IEntity.h"

namespace Hope {
	namespace EngineCore {
        
        class GameObject : public IEntity
		{
            HOPE_ENTITY(GameObject, IEntity)
		public:
			GameObject();
			~GameObject();
            
            virtual void Tick(f32 deltaTime) override;

		private:
		};
	}
}
