//
//  GameObject.cpp
//  HopeEngine
//
//  Created by Ben Hopewell on 21/10/18.
//  Copyright © 2018 Ben Hopewell. All rights reserved.
//

#include "GameObject.h"

#include "Engine.h"
#include "EngineCore/GameWorld.h"

using namespace Hope::EngineCore;

GameObject::GameObject()
{
}

GameObject::~GameObject()
{
    
}

void GameObject::Tick(f32 deltaTime)
{
    
}
