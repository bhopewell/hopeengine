//
//  EntityManager.cpp
//  HopeEngine
//
//  Created by Ben Hopewell on 11/12/18.
//  Copyright © 2018 Ben Hopewell. All rights reserved.
//

#include "EntityManager.h"

using namespace Hope::EngineCore;

EntityManager::EntityManager(ComponentManager* componentManager) :
    mEntityHandleManager(),
    mComponentManager(componentManager)
{
}

EntityManager::~EntityManager()
{
    std::unordered_map<StaticTypeId, IEntityContainer*>::iterator it = mEntityContainers.begin();
    for(; it != mEntityContainers.end(); ++it)
        delete it->second;
    
    mEntityContainers.clear();
}

IEntity* Hope::EngineCore::EntityManager::FindEntity(const HEntityId& entityId)
{
    return static_cast<IEntity*>(mEntityHandleManager.Get(entityId));
}

//Destroys all components of the entity prior to destroying the entity itself
void Hope::EngineCore::EntityManager::DestroyEntity(IEntity* entity)
{
    assert(entity->GetRTTI().DerivesFrom(IEntity::GetRTTI()));
    
    //If the handle is invalid, don't attempt to destroy anything
    if(!mEntityHandleManager.IsValid(entity->GetEntityId()))
        return;
    
    //Remove and destroy all components attached to the entity
    for(std::pair<const HComponentId, IComponent*>& componentPair : entity->mComponents)
    {
        entity->RemoveComponent(componentPair.second);
        mComponentManager->DestroyComponent(componentPair.second);
    }
    
    mEntityHandleManager.Remove(entity->GetEntityId());
    this->GetEntityContainer(entity)->DestroyEntity(entity);
}

IEntityContainer* Hope::EngineCore::EntityManager::GetEntityContainer(IEntity* entity)
{
    assert(entity->GetRTTI().DerivesFrom(IEntity::GetRTTI()));
    
    std::unordered_map<StaticTypeId, IEntityContainer*>::const_iterator it = mEntityContainers.find(entity->GetStaticTypeId());
    if(it == mEntityContainers.end())
        return nullptr;
    
    //Ensure the container exists
    assert(it != mEntityContainers.end() && it->second != nullptr);
    
    //Return the Component Container
    return it->second;
}
