//
//  IEntity.inl
//  HopeEngine
//
//  Created by Ben Hopewell on 16/10/18.
//  Copyright © 2018 Ben Hopewell. All rights reserved.
//

#pragma once

//Iterate through all the components and return all of type T
template<typename T>
inline std::vector<T*> Hope::EngineCore::IEntity::GetComponentsOfType()
{
    std::vector<T*> components;
    
    for(const std::pair<const HComponentId, IComponent*>& componentPair : mComponents)
    {
        if(T::GetRTTI() == componentPair.second->GetRTTI())
            components.push_back(static_cast<T*>(componentPair.second));
    }
    
    return components;
}

inline std::vector<Hope::EngineCore::IComponent*> Hope::EngineCore::IEntity::GetAllComponents() const
{
    std::vector<Hope::EngineCore::IComponent*> components {};
    
    for(std::pair<HComponentId, IComponent*> p : mComponents)
        components.push_back(p.second);
    
    return components;
}

//Get the component with the provided componentId
inline Hope::EngineCore::IComponent* Hope::EngineCore::IEntity::GetComponent(const HComponentId& componentId)
{
    std::unordered_map<HComponentId, IComponent*>::iterator it = mComponents.find(componentId);
    if(it == mComponents.end())
        return nullptr;
    
    return it->second;
}

//Attach a new component
inline void Hope::EngineCore::IEntity::AttachComponent(IComponent* component)
{
    mComponents.emplace(component->GetComponentId(), component);
    component->SetOwner(this);
}

//Remove a component
inline void Hope::EngineCore::IEntity::RemoveComponent(IComponent* component)
{
    mComponents.erase(component->GetComponentId());
    component->SetOwner(nullptr);
}
