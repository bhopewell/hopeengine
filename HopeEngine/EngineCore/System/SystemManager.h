//
//  SystemManager.h
//  HopeEngine
//
//  Created by Ben Hopewell on 17/2/19.
//  Copyright © 2019 Ben Hopewell. All rights reserved.
//

#pragma once

#include <vector>
#include <unordered_map>

#include "ISystem.h"
#include "Core/Memory/Allocators/LinearAllocator.h"

#define DEFAULT_SYSTEM_MANAGER_MEMORY_BYTES MegabytesToBytes(1)

namespace Hope {
    namespace EngineCore {
        
        class ISystem;
        
        class SystemManager
        {
        public:
            SystemManager();
            SystemManager(SystemManager&) = delete;
            SystemManager(SystemManager&&) = delete;
            
            ~SystemManager();
            
            template<class T>
            T* GetSystem();
            
            // Helper method to create Systems
            // Allocates the System using the SystemManager Allocator
            // Registers the System with the SystemManager
            template<class T, typename ...P>
            T* AddSystem(P&& ...params);
            
            template<class T>
            void EnableSystem();
            
            template<class T>
            void DisableSystem();
            
            template<class T>
            void SetSystemUpdateInterval(float updateTime);
            
            // WARNING: Use sparingly as it will call UpdateSystemWorkOrder
            //          which can be quite an expensive call.
            template<class T>
            void SetSystemPriority(ESystemPriority newPriority);
            
            void Tick(f32 deltaTime);
        
        private:
            // WARNING: This function can be quite expensive
            void UpdateSystemWorkOrder();
            
        private:
            Core::LinearAllocator mAllocator;
            std::vector<ISystem*> mSystemWorkOrder;
            std::unordered_map<StaticTypeId, ISystem*> mSystems;
        };
    }
}

#include "SystemManager.inl"
