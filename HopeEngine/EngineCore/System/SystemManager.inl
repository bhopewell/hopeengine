//
//  SystemManager.inl
//  HopeEngine
//
//  Created by Ben Hopewell on 18/2/19.
//  Copyright © 2019 Ben Hopewell. All rights reserved.
//

#pragma once

template<class T>
T* Hope::EngineCore::SystemManager::GetSystem()
{
    assert(T::GetRTTI().DerivesFrom(ISystem::GetRTTI()));
    
    std::unordered_map<StaticTypeId, ISystem*>::iterator it = mSystems.find(T::GetStaticTypeId());
    if(it == mSystems.end())
        return nullptr;
    
    return static_cast<T*>(it->second);
}

template<class T, typename ...P>
T* Hope::EngineCore::SystemManager::AddSystem(P&& ...params)
{
    assert(T::GetRTTI().DerivesFrom(ISystem::GetRTTI()));
    
    const StaticTypeId staticTypeId = T::GetStaticTypeId();
    
    //Ensure we don't register the same system multiple times
    std::unordered_map<StaticTypeId, ISystem*>::iterator it = mSystems.find(staticTypeId);
    if(it != mSystems.end())
    {
        return static_cast<T*>(it->second);
    }
    
    //Create the system and register with the SystemManager
    void* sysMem = mAllocator.Allocate(sizeof(T));
    T* system = new (sysMem)(T)(std::forward<P>(params)...);
    
    mSystems.emplace(staticTypeId, system);
    
    //TODO(Ben): Resize the dependency matrix? Is it necessary to have a dependency matrix?
    
    //Add to the work order vector
    mSystemWorkOrder.push_back(system);
    
    return system;
}

template<class T>
void Hope::EngineCore::SystemManager::EnableSystem()
{
    assert(T::GetRTTI().DerivesFrom(ISystem::GetRTTI()));
    
    const StaticTypeId staticTypeId = T::GetStaticTypeId();
    
    std::unordered_map<StaticTypeId, ISystem*>::iterator it = mSystems.find(staticTypeId);
    if(it != mSystems.end())
    {
        it->second->Enable();
    }
    else
    {
        //TODO(Ben): Log a warning, throw an assert. Something to inform the user that the system has not been registered
    }
}

template<class T>
void Hope::EngineCore::SystemManager::DisableSystem()
{
    assert(T::GetRTTI().DerivesFrom(ISystem::GetRTTI()));
    
    const StaticTypeId staticTypeId = T::GetStaticTypeId();
    
    std::unordered_map<StaticTypeId, ISystem*>::iterator it = mSystems.find(staticTypeId);
    if(it != mSystems.end())
    {
        it->second->Disable();
    }
    else
    {
        //TODO(Ben): Log a warning, throw an assert. Something to inform the user that the system has not been registered
    }
}

template<class T>
void Hope::EngineCore::SystemManager::SetSystemUpdateInterval(float newUpdateInterval)
{
    const StaticTypeId staticTypeId = T::GetStaticTypeId();
    
    std::unordered_map<StaticTypeId, ISystem*>::iterator it = mSystems.find(staticTypeId);
    if(it != mSystems.end())
    {
        it->second->SetUpdateInterval(newUpdateInterval);
    }
    else
    {
        //TODO(Ben): Log a warning, throw an assert. Something to inform the user that the system has not been registered
    }
}

template<class T>
void Hope::EngineCore::SystemManager::SetSystemPriority(ESystemPriority newPriority)
{
    const StaticTypeId staticTypeId = T::GetStaticTypeId();
    
    std::unordered_map<StaticTypeId, ISystem*>::iterator it = mSystems.find(staticTypeId);
    if(it != mSystems.end())
    {
        if(it->second->GetSystemPriority() == newPriority)
            return;
        
        //Update the system priority and work order
        it->second->SetSystemPriority(newPriority);
        UpdateSystemWorkOrder();
    }
    else
    {
        //TODO(Ben): Log a warning, throw an assert. Something to inform the user that the system has not been registered
    }
}
