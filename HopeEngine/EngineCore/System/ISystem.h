//
//  ISystem.h
//  HopeEngine
//
//  Created by Ben Hopewell on 17/10/18.
//  Copyright © 2018 Ben Hopewell. All rights reserved.
//

#pragma once

#include "Hope.h"
#include "Core/RTTI/HopeRTTI.h"

namespace Hope {
    namespace EngineCore {
        
        class SystemManager;
        
        enum class ESystemPriority : u16
        {
            ELowest             = 0,
            EVeryLow            = 99,
            ELow                = 100,
            ENormal             = 300,
            EHigh               = 400,
            EVeryHigh           = 401,
            EHighest            = std::numeric_limits<u16>::max()
        };
        
        class ISystem
        {
            friend class SystemManager;
            
            HOPE_BASE_CLASS(ISystem)
        public:
            ISystem(ESystemPriority priority = ESystemPriority::ENormal, f32 updateInterval = -1) :
                mEnabled(true),
                mNeedsUpdate(false),
                mPriority(priority),
                mLastUpdateTime(0.f),
                mUpdateInterval(updateInterval)
            {
            }
            virtual ~ISystem() { };
            
            virtual void PreTick(f32 deltaTime) = 0;
            virtual void Tick(f32 deltaTime) = 0;
            virtual void PostTick(f32 deltaTime) = 0;
            
            inline bool IsEnabled() const { return mEnabled; }
            inline void Enable() { mEnabled = true; }
            inline void Disable() { mEnabled = false; }
            
            inline ESystemPriority GetSystemPriority() const { return mPriority; }
            inline void SetSystemPriority(ESystemPriority newPriority) { mPriority = newPriority; }
            
            inline void SetUpdateInterval(f32 newUpdateInterval) { mUpdateInterval = newUpdateInterval; }
            
            inline void TickLastUpdateTime(f32 dt)
            {
                mLastUpdateTime += dt;
                mNeedsUpdate = mUpdateInterval < 0.f || (mUpdateInterval > 0.f && mLastUpdateTime >= mUpdateInterval);
            }
            inline void ResetLastUpdateTime(f32 dt) { mLastUpdateTime -= dt; }
            inline bool NeedsUpdate() const { return mNeedsUpdate; }
            
        protected:
            bool mEnabled;
            bool mNeedsUpdate;
            
            ESystemPriority mPriority;
            
            f32 mLastUpdateTime;
            f32 mUpdateInterval;
        };
    }
}
