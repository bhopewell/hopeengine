//
//  SystemManager.cpp
//  HopeEngine
//
//  Created by Ben Hopewell on 17/2/19.
//  Copyright © 2019 Ben Hopewell. All rights reserved.
//

#include "SystemManager.h"


using namespace Hope::EngineCore;

SystemManager::SystemManager() :
mAllocator(DEFAULT_SYSTEM_MANAGER_MEMORY_BYTES),
mSystemWorkOrder(),
mSystems()
{
}

SystemManager::~SystemManager()
{
    //Destroy all systems in reverse order
    for(std::vector<ISystem*>::reverse_iterator it = mSystemWorkOrder.rbegin(); it != mSystemWorkOrder.rend(); ++it)
    {
        (*it)->~ISystem();
        (*it) = nullptr;
    }
    
    mSystemWorkOrder.clear();
    mSystems.clear();
    
    //Clear the allocator of all allocated memory.
    //The allocator will free the memory when it is destroyed.
    mAllocator.Clear();
}

void SystemManager::Tick(f32 deltaTime)
{
    //Invoke the PreTick event on each System
    for(ISystem* system : mSystemWorkOrder)
    {
        system->TickLastUpdateTime(deltaTime);

        if(system->IsEnabled() && system->NeedsUpdate())
        {
            system->PreTick(deltaTime);
        }
    }
    
    //Invoke the Tick event on each System
    for(ISystem* system : mSystemWorkOrder)
    {
        if(system->IsEnabled() && system->NeedsUpdate())
        {
            system->Tick(deltaTime);
            system->ResetLastUpdateTime(deltaTime);
        }
    }
    
    //Invoke the PostTick event on each System
    for(ISystem* system : mSystemWorkOrder)
    {
        if(system->IsEnabled() && system->NeedsUpdate())
        {
            system->PostTick(deltaTime);
        }
    }
}

void SystemManager::UpdateSystemWorkOrder()
{
    //Sort by system priority, highest to lowest
    std::sort(mSystemWorkOrder.begin(), mSystemWorkOrder.end(),
              [](const ISystem* lhs, const ISystem* rhs){
                  return lhs->GetSystemPriority() > rhs->GetSystemPriority();
              }
    );
}
