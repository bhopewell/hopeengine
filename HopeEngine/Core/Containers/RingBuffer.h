#pragma once

#include <vector>
#include <mutex>
#include "Hope.h"

namespace Hope {
	namespace Core {

		template<typename T>
		class RingBuffer
		{
		public:
			//Default constructor
			explicit RingBuffer(u32 totalSize) :
                mSize(0),
				mTotalSize(totalSize),
                mHead(0),
                mTail(0)
			{
				mBuffer = (T*)malloc(sizeof(T) * totalSize);
			}

			RingBuffer(const RingBuffer&) = delete;
			RingBuffer& operator=(const RingBuffer&) = delete;

			//Default destructor
			~RingBuffer()
			{
				free(mBuffer);
			}

			//Push a new value onto the ring buffer
			bool Push(T val)
			{
				//If the buffer is full, return false
				if (mSize == mTotalSize)
					return false;

				//Lock the thread
				mLock.lock();

				//Store the value, move the head and increment the size.
				mBuffer[mHead] = val;
				++mHead %= mTotalSize;
				++mSize;

				//Unlock the thread
				mLock.unlock();

				return true;
			}

			//Push a new value onto the ring buffer. If the buffer is full, overwrite the oldest value.
			void PushOrReplace(T val)
			{
				//Lock the thread
				mLock.lock();

				//Set the value and move the head
				mBuffer[mHead] = val;
				++mHead %= mTotalSize;

				//If the buffer isn't full increment the size, otherwise move the tail to the head.
				if (mSize != mTotalSize)
					++mSize;
				else
					mTail = mHead;

				//Unlock the thread
				mLock.unlock();
			}

            inline T Peek() const
            {
                if(mSize == 0)
                    return NULL;
                
                return mBuffer[mTail];
            }
            
			//Pop the earliest value from the ring buffer
			T Pop()
			{
				//Return null if the buffer is empty.
				if (mSize == 0)
					return NULL;

				//Lock the thread
				mLock.lock();

				//Move the tail, decrement the size and return the value.
				T itemToPop = mBuffer[mTail];
				++mTail %= mTotalSize;
				--mSize;

				//Unlock the thread
				mLock.unlock();

				return itemToPop;
			}

            const T& operator[](const u32 index) const
            {
                assert(index < mSize);
                
                u32 actualIndex = (mTail + index) % mTotalSize;
                return mBuffer[actualIndex];
            }
            
			inline const bool IsFull() const
			{
				return mSize == mTotalSize;
			}

			//Return true if RingBuffer is empty
			inline const bool IsEmpty() const
			{
				return mSize == 0;
			}

			inline void Clear()
			{
				mTail = mHead;
				mSize = 0;
			}

			//Return the total reserved size of the ring buffer
			inline const u32 GetTotalSize() const
			{
				return mTotalSize;
			}

			//Return the total used size of the ring buffer
			inline const u32 GetSize() const
			{
				return mSize;
			}

		private:
			u32 mSize;
			const u32 mTotalSize;

			u32 mHead;
			u32 mTail;

			std::mutex mLock;
			T* mBuffer;
		};
	}
}
