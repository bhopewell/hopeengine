//
//  HopeRTTI.h
//  HopeEngine
//
//  Created by Ben Hopewell on 20/10/18.
//  Copyright © 2018 Ben Hopewell. All rights reserved.
//

#pragma once

#include <assert.h>

#include "Hope.h"

namespace Hope
{
    namespace Core
    {
        class HopeRTTI
        {
        public:
#ifndef DEBUG
            HopeRTTI();
            HopeRTTI(const HopeRTTI& rtti);
#else
            HopeRTTI( const char* pTypeName );
            HopeRTTI( const char* pTypeName, const HopeRTTI& rtti );
#endif
            
            bool DerivesFrom(const HopeRTTI& rtti) const;
            
            inline bool operator==(const HopeRTTI& rhs) const
            {
                return this->mStaticTypeId == rhs.mStaticTypeId;
            }
            
            inline bool operator!=(const HopeRTTI& rhs) const
            {
                return this->mStaticTypeId != rhs.mStaticTypeId;
            }
            
            inline StaticTypeId GetStaticTypeId() const
            {
                return mStaticTypeId;
            }
            
        private:
            static u64 GenerateStaticTypeId()
            {
                return STATIC_TYPE_ID_COUNT++;
            }
            
        private:
#ifdef DEBUG
            const char* mTypeName;
#endif
            const HopeRTTI& mParentRTTI;
            StaticTypeId mStaticTypeId;
            
            static u64 STATIC_TYPE_ID_COUNT;
        };
        
#define PRIVATE_RTTI_FUNCTION_SET( TYPE, BASE_CLASS )             \
static inline StaticTypeId GetStaticTypeId()                      \
{                                                                 \
    return TYPE::GetRTTI().GetStaticTypeId();                     \
}                                                                 \
inline bool DerivesFrom( const Hope::Core::HopeRTTI& rtti ) const \
{                                                                 \
    return TYPE::GetRTTI().DerivesFrom( rtti );                   \
}                                                                 \
static inline TYPE& GetSaveType( BASE_CLASS& Object )             \
{                                                                 \
    assert( Object.DerivesFrom(TYPE::GetRTTI()) );                \
    return *((TYPE*)&Object);                                     \
}                                                                 \
static inline const TYPE& GetSaveType( const BASE_CLASS& Object ) \
{                                                                 \
    assert( Object.DerivesFrom(TYPE::GetRTTI()) );                \
    return *((const TYPE*)&Object);                               \
}
        
#ifndef DEBUG //Macros for non-DEBUG builds
        
//Macro to define Base Class RTTI
#define HOPE_BASE_CLASS( TYPE ) public:                           \
static inline const Hope::Core::HopeRTTI& GetRTTI( void )         \
{                                                                 \
    static Hope::Core::HopeRTTI s_RTTI( );                        \
    return s_RTTI;                                                \
}                                                                 \
PRIVATE_RTTI_FUNCTION_SET(TYPE,TYPE)                              \
private:
        
//Macro to define Derived Component RTTI
#define HOPE_COMPONENT( TYPE, TYPE_PARENT ) public:               \
static inline const Hope::Core::HopeRTTI& GetRTTI( void )         \
{                                                                 \
    static Hope::Core::HopeRTTI s_RTTI( TYPE_PARENT::GetRTTI() ); \
    return s_RTTI;                                                \
}                                                                 \
PRIVATE_RTTI_FUNCTION_SET(TYPE, IComponent)                       \
private:
        
//Macro to define Derived Entity RTTI
#define HOPE_ENTITY( TYPE, TYPE_PARENT ) public:                  \
static inline const Hope::Core::HopeRTTI& GetRTTI( void )         \
{                                                                 \
    static Hope::Core::HopeRTTI s_RTTI( TYPE_PARENT::GetRTTI() ); \
    return s_RTTI;                                                \
}                                                                 \
PRIVATE_RTTI_FUNCTION_SET(TYPE, IEntity)                          \
private:
        
//Macro to define Derived System RTTI
#define HOPE_SYSTEM( TYPE, TYPE_PARENT ) public:                  \
static inline const Hope::Core::HopeRTTI& GetRTTI( void )         \
{                                                                 \
    static Hope::Core::HopeRTTI s_RTTI( TYPE_PARENT::GetRTTI() ); \
    return s_RTTI;                                                \
}                                                                 \
PRIVATE_RTTI_FUNCTION_SET(TYPE, ISystem)                          \
private:
        
//Macro to define Derived Class RTTI
#define HOPE_DERIVED_CLASS( TYPE, TYPE_PARENT, BASE_CLASS ) public:    \
static inline const Hope::Core::HopeRTTI& GetRTTI( void )         \
{                                                                 \
    static Hope::Core::HopeRTTI s_RTTI( TYPE_PARENT::GetRTTI() ); \
    return s_RTTI;                                                \
}                                                                 \
PRIVATE_RTTI_FUNCTION_SET(TYPE,BASE_CLASS)                        \
private:
        
#else //Macros for DEBUG builds
        
//Macro to define Base Class RTTI
#define HOPE_BASE_CLASS( TYPE ) public:                           \
static inline const Hope::Core::HopeRTTI& GetRTTI( void )         \
{                                                                 \
    static Hope::Core::HopeRTTI s_RTTI( #TYPE );                  \
    return s_RTTI;                                                \
}                                                                 \
PRIVATE_RTTI_FUNCTION_SET(TYPE,TYPE)                              \
private:
        
//Macro to define Derived Component RTTI
#define HOPE_COMPONENT( TYPE, TYPE_PARENT ) public:               \
static inline const Hope::Core::HopeRTTI& GetRTTI( void )         \
{                                                                 \
    static Hope::Core::HopeRTTI s_RTTI( #TYPE, TYPE_PARENT::GetRTTI() );          \
    return s_RTTI;                                                \
}                                                                 \
PRIVATE_RTTI_FUNCTION_SET(TYPE, IComponent)                       \
private:
        
//Macro to define Derived Entity RTTI
#define HOPE_ENTITY( TYPE, TYPE_PARENT ) public:                  \
static inline const Hope::Core::HopeRTTI& GetRTTI( void )         \
{                                                                 \
    static Hope::Core::HopeRTTI s_RTTI( #TYPE, TYPE_PARENT::GetRTTI() );          \
    return s_RTTI;                                                \
}                                                                 \
PRIVATE_RTTI_FUNCTION_SET(TYPE, IEntity)                          \
private:
        
//Macro to define Derived System RTTI
#define HOPE_SYSTEM( TYPE, TYPE_PARENT ) public:                  \
static inline const Hope::Core::HopeRTTI& GetRTTI( void )         \
{                                                                 \
    static Hope::Core::HopeRTTI s_RTTI( #TYPE, TYPE_PARENT::GetRTTI() );          \
    return s_RTTI;                                                \
}                                                                 \
PRIVATE_RTTI_FUNCTION_SET(TYPE, ISystem)                          \
private:
        
//Macro to define Derived Class RTTI
#define HOPE_DERIVED_CLASS( TYPE, TYPE_PARENT, BASE_CLASS ) public:    \
static inline const Hope::Core::HopeRTTI& GetRTTI( void )         \
{                                                                 \
    static Hope::Core::HopeRTTI s_RTTI( #TYPE, TYPE_PARENT::GetRTTI() );      \
    return s_RTTI;                                                \
}                                                                 \
PRIVATE_RTTI_FUNCTION_SET(TYPE,BASE_CLASS)                        \
private:
        
#endif //End of Macros for DEBUG builds
        
    }
}
