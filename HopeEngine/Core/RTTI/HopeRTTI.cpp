//
//  HopeRTTI.cpp
//  HopeEngine
//
//  Created by Ben Hopewell on 21/10/18.
//  Copyright © 2018 Ben Hopewell. All rights reserved.
//

#include "Hope.h"
#include "HopeRTTI.h"

using namespace Hope::Core;

u64 HopeRTTI::STATIC_TYPE_ID_COUNT = 0;

#ifndef DEBUG
HopeRTTI::HopeRTTI( ) :
mParentRTTI( *this ),
mStaticTypeId(GenerateStaticTypeId())
{
    
}

HopeRTTI::HopeRTTI( const HopeRTTI& rtti ) :
mParentRTTI( rtti ),
mStaticTypeId(GenerateStaticTypeId())
{
    
}
#else
HopeRTTI::HopeRTTI( const char* pTypeName ) :
    mTypeName(pTypeName),
    mParentRTTI( *this ),
    mStaticTypeId(GenerateStaticTypeId())
{
    
}

HopeRTTI::HopeRTTI( const char* pTypeName, const HopeRTTI& rtti ) :
    mTypeName(pTypeName),
    mParentRTTI( rtti ),
    mStaticTypeId(GenerateStaticTypeId())
{
    
}
#endif

bool HopeRTTI::DerivesFrom(const HopeRTTI& rtti) const
{
    const HopeRTTI* p = this;
    do
    {
        if( p == &rtti )
            return true;
        
        if( p == &p->mParentRTTI )
            break;
        
        p = &p->mParentRTTI;
        
    } while(1);
    return false;
}
