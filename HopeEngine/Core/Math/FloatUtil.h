//
//  Float.h
//  HopeEngine
//
//  Created by Ben Hopewell on 10/6/17.
//  Copyright © 2017 Ben Hopewell. All rights reserved.
//

#pragma once

bool AlmostEqual(float a, float b);
