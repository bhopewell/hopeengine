//
//  Plane.cpp
//  HopeEngine
//
//  Created by Ben Hopewell on 3/9/17.
//  Copyright � 2017 Ben Hopewell. All rights reserved.
//

#include "Plane.h"

Plane::Plane() :
	Distance{ 0 },
Normal{ glm::vec3() }
{

}

Plane::Plane(f32 distance, glm::vec3 normal) :
	Distance{ distance },
	Normal{ normal }
{
	
}

Plane::Plane(const Plane& other) :
	Distance{ other.Distance },
	Normal{ other.Normal }
{
}
