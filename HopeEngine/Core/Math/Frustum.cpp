//
//  Frustum.cpp
//  HopeEngine
//
//  Created by Ben Hopewell on 3/9/17.
//  Copyright � 2017 Ben Hopewell. All rights reserved.
//

#include "Frustum.h"

Frustum::Frustum() :
	mPlanes{}
{

}

Frustum::Frustum(const glm::mat4& viewProjMat) :
	mPlanes{}
{

}

//NOTE: The below calculation only works for a Row-Major matrix.
void Frustum::RecalculateFrustum(const glm::mat4& viewProjMat)
{
	//Vector4(viewProjMat[0][3] - viewProjMat[0][1], viewProjMat[1][3] - viewProjMat[1][1], viewProjMat[2][3] - viewProjMat[2][1], -viewProjMat[3][3] + viewProjMat[3][1]); //Top
	//Vector4(viewProjMat[0][3] + viewProjMat[0][1], viewProjMat[1][3] + viewProjMat[1][1], viewProjMat[2][3] + viewProjMat[2][1], -viewProjMat[3][3] - viewProjMat[3][1]); //Bottom
	//Vector4(viewProjMat[0][3] + viewProjMat[0][0], viewProjMat[1][3] + viewProjMat[1][0], viewProjMat[2][3] + viewProjMat[2][0], -viewProjMat[3][3] - viewProjMat[3][0]); //Left
	//Vector4(viewProjMat[0][3] - viewProjMat[0][0], viewProjMat[1][3] - viewProjMat[1][0], viewProjMat[2][3] - viewProjMat[2][0], -viewProjMat[3][3] + viewProjMat[3][0]); //Right
	//Vector4(viewProjMat[0][3] + viewProjMat[0][2], viewProjMat[1][3] + viewProjMat[1][2], viewProjMat[2][3] + viewProjMat[2][2], -viewProjMat[3][3] - viewProjMat[3][2]); //Near
	//Vector4(viewProjMat[0][3] - viewProjMat[0][2], viewProjMat[1][3] - viewProjMat[1][2], viewProjMat[2][3] - viewProjMat[2][2], -viewProjMat[3][3] + viewProjMat[3][2]); //Far
}

bool Contains(const glm::vec2& point)
{
	return false;
}

bool Contains(const glm::vec3& point)
{
	return false;
}

bool Contains(const glm::vec4& point)
{
	return false;
}

bool Contains(const glm::mat4& transform)
{
	return false;
}

bool Intersects(const glm::vec2& point)
{
	return false;
}

bool Intersects(const glm::vec3& point)
{
	return false;
}

bool Intersects(const glm::vec4& point)
{
	return false;
}

bool Intersects(const glm::mat4& transform)
{
	return false;
}
