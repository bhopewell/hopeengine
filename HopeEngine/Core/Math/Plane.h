//
//  Plane.h
//  HopeEngine
//
//  Created by Ben Hopewell on 3/9/17.
//  Copyright � 2017 Ben Hopewell. All rights reserved.
//

#pragma once

#include <glm/vec3.hpp>
#include "Hope.h"

struct Plane
{
public:
	f32 Distance;
    glm::vec3 Normal;

public:
	Plane();
    Plane(f32 distance, glm::vec3 normal);
	Plane(const Plane& other); //Copy constructor
	~Plane() = default;
};
