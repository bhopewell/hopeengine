//
//  Frustum.h
//  HopeEngine
//
//  Created by Ben Hopewell on 3/9/17.
//  Copyright � 2017 Ben Hopewell. All rights reserved.
//

#pragma once

//http://gamedevs.org/uploads/fast-extraction-viewing-frustum-planes-from-world-view-projection-matrix.pdf

#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <glm/mat4x4.hpp>

#include "Plane.h"

class Frustum
{
public:
	Frustum();
    Frustum(const glm::mat4& viewProjMat);
	~Frustum() = default;

    void RecalculateFrustum(const glm::mat4& viewProjMat);

    bool Contains(const glm::vec2& point);
    bool Contains(const glm::vec3& point);
    bool Contains(const glm::vec4& point);
    bool Contains(const glm::mat4& transform);

    bool Intersects(const glm::vec2& point);
    bool Intersects(const glm::vec3& point);
    bool Intersects(const glm::vec4& point);
    bool Intersects(const glm::mat4& transform);

private:
	Plane mPlanes[6];
};
