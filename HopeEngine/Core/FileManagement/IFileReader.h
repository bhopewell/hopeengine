#pragma once

namespace Hope
{
	namespace Core
	{
		class IFileReader
		{
		public:
			IFileReader() { }

			virtual const char* ReadSync(const char* filepath) = 0;
			virtual void ReadAsync() = 0;
		};
	}
}
