#pragma once

#ifdef _WIN32

#include <stdio.h>
#include "../IFileWriter.h"

namespace Hope
{
	namespace Core
	{
		class FileWriter : public IFileWriter
		{
		public:
			FileWriter(char* filepath);

			/*void ReadSync();
			void ReadAsync();*/

		protected:
			FILE* mFile;
		};
	}
}

#endif //_WIN32