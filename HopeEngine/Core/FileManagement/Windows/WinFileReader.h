#pragma once

#ifdef _WIN32

#include <stdio.h>
#include "../IFileReader.h"

namespace Hope
{
	namespace Core
	{
		class FileReader : public IFileReader
		{
		public:
			FileReader(char* filepath);

			void ReadSync();
			void ReadAsync();

		protected:
			FILE* mFile;
		};
	}
}

#endif //_WIN32