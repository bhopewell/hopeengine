//
//  OBJLoader.hpp
//  HopeEngine
//
//  Created by Ben Hopewell on 26/8/17.
//  Copyright © 2017 Ben Hopewell. All rights reserved.
//

#pragma once

#include <string>

//#error
//TODO(Ben): What file formats do I want to support?
//OBJ? FBX? COLLADA?
//Could use FBX SDK for FBX.
//Is assimp worth it?
//Does assimp work alongside DirectX?
//What's a good way to load meshes on background threads?
	//Is the IRunnableThread good enough or should I have a WorkerThreadPool/WorkerThread?
		//WorkerThreadPool/WorkerThread could pull from a queue, load, and upon completion invoke a function.

namespace Hope {
    namespace Core {
        
        class OBJLoader
        {
            OBJLoader();
            ~OBJLoader();
            
            bool LoadOBJ(const std::string& filepath);
        };
    }
}
