//
//  OBJLoader.cpp
//  HopeEngine
//
//  Created by Ben Hopewell on 26/8/17.
//  Copyright © 2017 Ben Hopewell. All rights reserved.
//

#include "Hope.h"
#include "OBJLoader.h"

using namespace Hope::Core;

OBJLoader::OBJLoader()
{
    
}

OBJLoader::~OBJLoader()
{
    
}

bool OBJLoader::LoadOBJ(const std::string& filepath)
{
    HOPE_UNUSED(filepath)
    HOPE_UNIMPLEMENTED()
    //TODO(Ben): Validate file is OBJ. Return false if it isn't.
    //TODO(Ben): Load OBJ.
    return false;
}
