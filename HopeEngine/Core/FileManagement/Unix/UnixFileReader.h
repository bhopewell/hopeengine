#pragma once

#include "../IFileReader.h"
#include <fstream>

namespace Hope
{
	namespace Core
	{
		class FileReader : public IFileReader
		{
		public:
			FileReader();
            
			virtual const char* ReadSync(const char* filepath) override;
			virtual void ReadAsync() override;

		private:
            std::ifstream::pos_type GetFileSize(std::ifstream& streamReader) const;
		};
	}
}
