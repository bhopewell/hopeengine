#include "UnixFileReader.h"
#include <iostream>

using namespace Hope::Core;

FileReader::FileReader() :
IFileReader()
{
}

const char* FileReader::ReadSync(const char* filepath)
{
    //Open the file
    std::ifstream streamReader(filepath, std::ifstream::in);
    if(!streamReader.is_open())
    {
        std::cout << "Failed to open file=" << filepath << std::endl;
        return nullptr;
    }
    
    //Get the file length
    std::ifstream::pos_type fileLen = GetFileSize(streamReader);
    
    char* content = new char[fileLen];
    streamReader.read(content, fileLen);
    
    if(!streamReader)
    {
        std::cout << "Failed to read file=" << filepath << std::endl;
        delete[] content;
        streamReader.close();
        return nullptr;
    }
    
    std::cout << "File read successfully - " << filepath << std::endl;
    streamReader.close();
    return content;
}

void FileReader::ReadAsync()
{

}

std::ifstream::pos_type FileReader::GetFileSize(std::ifstream& streamReader) const
{
    streamReader.seekg(0, streamReader.end);
    std::ifstream::pos_type fileLen = streamReader.tellg();
    streamReader.seekg(0, streamReader.beg);
    
    return fileLen;
}
