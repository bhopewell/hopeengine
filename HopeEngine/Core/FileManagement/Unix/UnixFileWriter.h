#pragma once

#ifdef __linux__

#include <stdio.h>
#include "../IFileWriter.h"

namespace Core
{
	namespace IO
	{
		class FileWriter : public IFileWriter
		{
		public:
			FileWriter(char* filepath);

			/*virtual void ReadSync() override;
			virtual void ReadAsync() override;*/

		protected:
			FILE* mFile;
		};
	}
}

#endif //__linux__