#pragma once

#include <string>
#include <atomic>
#include "Hope.h"

namespace Hope {
	namespace Core {
		class IThreadTask;

		class IRunnableThread
		{
		public:
			virtual ~IRunnableThread() = default;
            
			virtual bool Construct(IThreadTask* task, const char* threadName, u32 stackSize) = 0;

			//virtual void Suspend() = 0;
			virtual void Kill(bool waitUntilComplete = false) = 0;

            u32 GetThreadId() { return mThreadId; }
            const std::string& GetThreadName() { return mThreadName; }

			virtual void WaitForCompletion() = 0;

		protected:
            
			virtual i32 Run() = 0;

		protected:
            std::atomic_bool mRunning;
            u32 mThreadId;
            std::string mThreadName;
            
			IThreadTask* mTask;
		};
	}
}
