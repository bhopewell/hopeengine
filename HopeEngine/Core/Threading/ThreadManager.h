#pragma once

#include <unordered_map>

#include "Hope.h"

namespace Hope {
	namespace Core {

		class IThreadTask;
		class IRunnableThread;

        static u32 NullThreadId = 0;
        static u32 ThreadId = 1;
        
        extern u32 GetNextThreadId();
        
		class ThreadManager
		{
		public:
			ThreadManager();
			~ThreadManager();

			u32 CreateThread(IThreadTask* task, const char* threadName, u32 stackSize);
			IRunnableThread* GetThread(u32 threadId);

			void KillAllThreads(bool waitUntilComplete);
			void KillThread(u32 threadId, bool waitUntilComplete);

		protected:
			static IRunnableThread* CreatePlatformSpecificThread();

		private:
			std::unordered_map<u32, IRunnableThread*> mThreadMap;
		};
	}
}
