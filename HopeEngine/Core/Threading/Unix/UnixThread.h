//
//  PThread.hpp
//  HopeEngine
//
//  Created by Ben Hopewell on 24/11/16.
//  Copyright © 2016 Ben Hopewell. All rights reserved.
//

#pragma once

#if defined(__APPLE__) || defined(__linux__)

#include <assert.h>
#include <pthread.h>
#include "../IRunnableThread.h"

namespace Hope {
    namespace Core {

        class UnixRunnableThread : public IRunnableThread
        {
        public:
            static const u32 MIN_STACK_SIZE = 16382;
            static void* _ThreadProc(void* pThis)
            {
                assert(pThis != nullptr);
                
                UnixRunnableThread* runnable = (UnixRunnableThread*)pThis;
                
                //TODO(Ben): Find a better way to return the result.
                //Firstly, the pointer is uninitialised so it could be pointing anywhere.
                //Secondly, we're returning a pointer to a LOCAL variable which will be junk if used outside this function.
                runnable->Run();
                
                return NULL;
            }
            
            virtual ~UnixRunnableThread() { };
            
            virtual bool Construct(IThreadTask* task, const char* threadName, u32 stackSize) override;
            
            //virtual void Suspend() = 0;
            virtual void Kill(bool waitUntilComplete = false) override;
            
            virtual void WaitForCompletion() override;
            
        protected:
            
            virtual i32 Run() override;
            
        private:
            
            pthread_t mThreadHandle;
        };
    }
}

#endif
