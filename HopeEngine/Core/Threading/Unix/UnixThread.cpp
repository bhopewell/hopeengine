//
//  PThread.cpp
//  HopeEngine
//
//  Created by Ben Hopewell on 24/11/16.
//  Copyright © 2016 Ben Hopewell. All rights reserved.
//

#if defined(__APPLE__) || defined(__linux__)

#include "UnixThread.h"
#include "Core/Threading/IThreadTask.h"

#include "Core/Threading/ThreadManager.h"

#include <iostream>

using namespace Hope::Core;

bool UnixRunnableThread::Construct(IThreadTask* task, const char* threadName, u32 stackSize)
{
    if(stackSize < MIN_STACK_SIZE)
        return false;
    
    pthread_attr_t attr;
    //Attempt the initialise the thread attributes. If it fails, return false
    if(pthread_attr_init(&attr) != 0)
        return false;
        
    //Attempt to set the stack size of the thread. If it fails, return false
    if(pthread_attr_setstacksize(&attr, stackSize) != 0)
        return false;
    
    mTask = task;
    //Create the pthread
    if(pthread_create(&mThreadHandle, &attr, _ThreadProc, this))
    {
        mTask = nullptr;
        return false;
    }
    else
    {
        mThreadId = Hope::Core::GetNextThreadId();
        mThreadName = threadName ? threadName : "";
    }
    
    //Destroy the now obsolete thread attributes object
    pthread_attr_destroy(&attr);
    
    return mThreadHandle != nullptr;
}

void UnixRunnableThread::WaitForCompletion()
{
    //Wait until the thread is complete
    pthread_join(mThreadHandle, NULL);
    std::cout << "Thread complete: " << this->mThreadName << std::endl;
}

i32 UnixRunnableThread::Run()
{
    assert(mTask != nullptr);
    
    //Initialise the task. If initialisation fails, return error code.
    if (!mTask->Init())
    {
        pthread_exit(NULL);
        return -1;
    }
    
    //Start running the task
    mRunning = true;
    
    while(mRunning)
    {
        //Run the task
        mTask->Tick();
    }
        
    //Clean up any allocated resources
    mTask->Exit();
    
    pthread_exit(NULL);
    return 0;
}

void UnixRunnableThread::Kill(bool waitUntilComplete)
{
    assert(mThreadHandle);
    
    //Stop running the task
    mRunning = false;
    
    //Wait until the thread is complete before closing the thread.
    //This is done to prevent potential deadlocks.
    if (waitUntilComplete)
        WaitForCompletion();
    
    //pthread_join(mThreadHandle, nullptr);
    
    mThreadHandle = NULL;
}

#endif
