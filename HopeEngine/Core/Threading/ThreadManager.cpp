#include "ThreadManager.h"
#include "IRunnableThread.h"

#ifdef _WIN32
#include "Windows/WinThread.h"
#else
#include "Unix/UnixThread.h"
#endif

using namespace Hope::Core;

u32 Hope::Core::GetNextThreadId() { return ThreadId++; }

ThreadManager::ThreadManager()
	: mThreadMap(std::unordered_map<u32, IRunnableThread*>())
{
}

ThreadManager::~ThreadManager()
{
	//Force kill all threads
	while (mThreadMap.size() > 0)
		KillThread(mThreadMap.begin()->first, false);

	mThreadMap.clear();
}

IRunnableThread* ThreadManager::CreatePlatformSpecificThread()
{
#ifdef _WIN32
	return new WinRunnableThread();
#elif __APPLE__
    return new UnixRunnableThread();
#endif

	return nullptr;
}

IRunnableThread* ThreadManager::GetThread(u32 threadId)
{
	//Find the thread in the thread map and return it's pointer.
	//Return nullptr if not found.
	auto iter = mThreadMap.find(threadId);
	if (iter != mThreadMap.end())
		return iter->second;

	return nullptr;
}

u32 ThreadManager::CreateThread(IThreadTask* task, const char* threadName, u32 stackSize)
{
	IRunnableThread* newThread = nullptr;

	//Create the platform specific thread
	newThread = ThreadManager::CreatePlatformSpecificThread();
	if (newThread != nullptr)
	{
		//Construct the thread. If it fails, delete if
		bool succeeded = newThread->Construct(task, threadName, stackSize);
		if (!succeeded)
		{
			delete newThread;
			newThread = nullptr;
		}
		else
		{
			//Register the thread in the thread manager
			mThreadMap[newThread->GetThreadId()] = newThread;
		}
	}
    
    if(newThread == nullptr)
        return NullThreadId;
    
	return newThread->GetThreadId();
}

void ThreadManager::KillAllThreads(bool waitUntilComplete)
{
	//Force kill all threads
	while (mThreadMap.size() > 0)
		KillThread(mThreadMap.begin()->first, waitUntilComplete);
}

void ThreadManager::KillThread(u32 threadId, bool waitUntilComplete)
{
	IRunnableThread* thread = GetThread(threadId);
	if (thread == nullptr)
		return;

	//Kill the thread
	thread->Kill(waitUntilComplete);

	thread = nullptr;
	mThreadMap.erase(threadId);
}
