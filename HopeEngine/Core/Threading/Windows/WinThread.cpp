#ifdef _WIN32

#include <Windows.h>

#include "WinThread.h"
#include "Core/Threading/IThreadTask.h"
#include "Core/Threading/ThreadManager.h"

using namespace Hope::Core;

bool WinRunnableThread::Construct(IThreadTask* task, const char* threadName, u32 stackSize)
{
	mTask = task;

	mThreadHandle = CreateThread(NULL, stackSize, _ThreadProc, this, STACK_SIZE_PARAM_IS_A_RESERVATION, (::DWORD*)&mThreadId);
	if (mThreadHandle == NULL)
	{
		mTask = nullptr;
	}
	else
	{
		//Start up the thread here.
        mThreadId = Hope::Core::GetNextThreadId();
		mThreadName = threadName ? threadName : "Unnamed Thread";
	}

	return mThreadHandle != NULL;
}

void WinRunnableThread::WaitForCompletion()
{
	WaitForSingleObject(mThreadHandle, INFINITE);
}

i32 WinRunnableThread::Run()
{
	assert(mTask != nullptr);
    
	//Initialise the task
	if (!mTask->Init())
        return -1;
    
    //Start running the task
    mRunning = true;
    
    while(mRunning)
    {
        //Run the task
        mTask->Tick();
    }
    
    //Clean up any allocated resources
    mTask->Exit();
	
	return 0;
}

void WinRunnableThread::Kill(bool waitUntilComplete)
{
	assert(mThreadHandle);
    
    //Stop running the task
    mRunning = false;

	//Wait until the thread is complete before closing the thread.
	//This is done to prevent potential deadlocks.
	if (waitUntilComplete)
		WaitForCompletion();

	CloseHandle(mThreadHandle);
	mThreadHandle = NULL;
}

#endif
