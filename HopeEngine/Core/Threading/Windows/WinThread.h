#pragma once

#ifdef _WIN32

#include <thread>
#include <assert.h>
#include "../IRunnableThread.h"

namespace Hope {
	namespace Core {

		class WinRunnableThread : public IRunnableThread
		{
		public:
			static ::DWORD __stdcall _ThreadProc(void* pThis)
			{
				assert(pThis != nullptr);
				return ((WinRunnableThread*)pThis)->Run();
			}

			~WinRunnableThread() { }

            virtual bool Construct(IThreadTask* task, const char* threadName, u32 stackSize) override;
			
			//virtual void Suspend() override;
            virtual void Kill(bool waitUntilComplete) override;

			virtual void WaitForCompletion() override;

		protected:
			virtual i32 Run() override;

		private:
			HANDLE mThreadHandle;
		};
	}
}

#endif
