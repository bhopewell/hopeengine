#pragma once

#include "Hope.h"

namespace Hope {
	namespace Core {

		class IThreadTask
		{
		public:
			virtual bool Init() = 0;
			virtual void Tick() = 0;
			virtual void Exit() = 0;
		};
	}
}
