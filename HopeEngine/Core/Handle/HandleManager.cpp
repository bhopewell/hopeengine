#include <assert.h>
#include "HandleManager.h"

using namespace Hope::Core;

HHandleManager::HHandleEntry::HHandleEntry() :
	mNextFreeIndex(0),
	mCounter(1),
	mActive(0),
	mEndOfList(0),
	mEntry(nullptr)
{
}

HHandleManager::HHandleEntry::HHandleEntry(u32 nextFreeIndex) :
	mNextFreeIndex(nextFreeIndex),
	mCounter(1),
	mActive(0),
	mEndOfList(0),
	mEntry(nullptr)
{

}

HHandleManager::HHandleManager()
{
	Reset();
}

HHandleManager::HHandleManager(const HHandleManager& other)
{
    HOPE_UNUSED(other)
    HOPE_UNIMPLEMENTED()
}

void HHandleManager::Reset()
{
	mActiveEntryCount = 0;
	mFirstFreeEntry = 0;

	for (u32 i = 0; i < MAX_ENTRIES - 1; ++i)
		mEntries[i] = HHandleEntry(i + 1);

	mEntries[MAX_ENTRIES - 1] = HHandleEntry();
	mEntries[MAX_ENTRIES - 1].mEndOfList = true;
}

HHandle HHandleManager::Add(void* ptr, StaticTypeId type)
{
	//Ensure the active entry count is within the valid range
	assert(mActiveEntryCount < MAX_ENTRIES - 1);
	//Ensure the type is within the accepted range
	assert(type <= 31);

	const u32 newIndex = mFirstFreeEntry;

	//Ensure the new index is within the valid range
	assert(newIndex < MAX_ENTRIES);
	//Ensure the entry at the new index is not active and its not the end of the list.
	assert(mEntries[newIndex].mActive == false);
	assert(mEntries[newIndex].mEndOfList == false);

	//Add the entry to the entries array
	mFirstFreeEntry = mEntries[newIndex].mNextFreeIndex;
	mEntries[newIndex].mNextFreeIndex = 0;
	mEntries[newIndex].mCounter += 1;

	if (mEntries[newIndex].mCounter == 0)
		mEntries[newIndex].mCounter = 1;

	mEntries[newIndex].mActive = true;
	mEntries[newIndex].mEntry = ptr;

	++mActiveEntryCount;

	//Return a handle to the entry
	return HHandle(newIndex, mEntries[newIndex].mCounter, type);
}

void HHandleManager::Update(HHandle handle, void* ptr)
{
	const u32 index = handle.mIndex;

	assert(mEntries[index].mCounter == handle.mCounter);
	assert(mEntries[index].mActive == true);

	mEntries[index].mEntry = ptr;
}

void HHandleManager::Remove(HHandle handle)
{
	const u32 index = handle.mIndex;

	assert(mEntries[index].mCounter == handle.mCounter);
	assert(mEntries[index].mActive == true);

	mEntries[index].mNextFreeIndex = mFirstFreeEntry;
	mEntries[index].mActive = false;
	mFirstFreeEntry = index;

	--mActiveEntryCount;
}

void* HHandleManager::Get(HHandle handle) const
{
	void* ptr = nullptr;

	if (!Get(handle, ptr))
		return nullptr;

	return ptr;
}

bool HHandleManager::Get(HHandle handle, void*& outPtr) const
{
	const i32 index = handle.mIndex;
	if (mEntries[index].mCounter != handle.mCounter || !mEntries[index].mActive)
		return false;

	outPtr = mEntries[index].mEntry;
	return true;
}

i32 HHandleManager::GetCount() const
{
	return mActiveEntryCount;
}

bool HHandleManager::IsValid(HHandle handle) const
{
    const i32 index = handle.mIndex;
    return mEntries[index].mCounter == handle.mCounter && mEntries[index].mActive;
}
