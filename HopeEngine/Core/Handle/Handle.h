#pragma once
#include "Hope.h"
#include <functional>

namespace Hope {
	namespace Core {

		struct HHandle
		{
            friend struct std::hash<Hope::Core::HHandle>;
            
			HHandle() :
				mIndex(0),
				mCounter(0),
				mType(0)
			{
			}

			HHandle(u32 index, u32 counter, StaticTypeId type) :
				mIndex(index),
				mCounter(counter),
				mType(type)
			{
			}

            inline operator u32() const { return mType << 26 | mCounter << 12 | mIndex; }

			u64 mIndex : 12;
			u64 mCounter : 14;
			u64 mType : 6;
		};
	}
}

typedef Hope::Core::HHandle HComponentId;
typedef Hope::Core::HHandle HEntityId;

namespace std {
    
    template <>
    struct hash<Hope::Core::HHandle>
    {
        std::size_t operator()(const Hope::Core::HHandle& k) const
        {
            using std::size_t;
            using std::hash;
            
            // Compute individual hash values for first,
            // second and third and combine them using XOR
            // and bit shifting:
            
            return ((std::hash<u32>()(k.mType)
                     ^ (std::hash<u32>()(k.mCounter) << 1)) >> 1)
                     ^ (std::hash<u32>()(k.mIndex) << 1);
        }
    };
    
}
