#pragma once
#include <stdint.h>
#include "Handle.h"

namespace Hope {
	namespace Core {

		class HHandleManager
		{
		public:
			static const u32 MAX_ENTRIES = 4096; //2 ^ 12;

			HHandleManager();

			void Reset();
			HHandle Add(void* ptr, StaticTypeId type);
			void Update(HHandle handle, void* ptr);
			void Remove(HHandle handle);

			void* Get(HHandle handle) const;
			bool Get(HHandle handle, void*& outPtr) const;

			i32 GetCount() const;
            bool IsValid(HHandle handle) const;

		private:
			HHandleManager(const HHandleManager&);
			HHandleManager& operator=(const HHandleManager&);

			struct HHandleEntry
			{
				HHandleEntry();
				explicit HHandleEntry(u32 nextFreeIndex);

                //TODO(Ben): Resize these values?
				u64 mNextFreeIndex : 12;
				u64 mCounter : 14;
				u64 mActive : 1;
				u64 mEndOfList : 1;
				void* mEntry;
			};

			HHandleEntry mEntries[MAX_ENTRIES];

			u32 mActiveEntryCount;
			u32 mFirstFreeEntry;
		};
	}
}
