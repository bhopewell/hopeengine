#pragma once

#include "Hope.h"

namespace Hope {
	namespace Core {

        class HopeAllocator;
        
		enum class EDisplayMode : u8
		{
			EWindowed,
			EBorderlessWindow,
			EWindowedFullscreen,
			EFullscreen
		};

		class IWindow
		{
		public:
			IWindow(const char* windowName, u32 w, u32 h) :
				mWidth(w),
				mHeight(h),
				mWindowName(windowName),
				mDisplayMode(EDisplayMode::EWindowed)
			{ }
			virtual ~IWindow() = default;
            
			virtual bool Initialise() = 0;
			
			virtual void Show(bool show) = 0;
            virtual void Close() = 0;

			virtual void Resize(u32 w, u32 h) = 0;
			//virtual void Resize(const vec2& size) = 0;

			inline bool IsFullscreen() const { return mDisplayMode == EDisplayMode::EFullscreen || mDisplayMode == EDisplayMode::EWindowedFullscreen; }
			virtual void SetDisplayMode(EDisplayMode displayMode) = 0;

		protected:
			virtual void Destroy() = 0; //This may be unnecessary.

		protected:
			u32 mWidth;
			u32 mHeight;
			const char* mWindowName;

			EDisplayMode mDisplayMode;
		};
	}
}
