//
//  OSXWindow.h
//  HopeEngine
//
//  Created by Ben Hopewell on 18/11/16.
//  Copyright © 2016 Ben Hopewell. All rights reserved.
//

#pragma once

#include "Hope.h"

#include "../IWindow.h"
#include <memory>

#if __OBJC__
#include <Cocoa/Cocoa.h>

#ifdef HOPE_OPENGL
typedef NSOpenGLView* RenderViewPtr;
#elif HOPE_METAL
#include <MetalKit/MetalKit.h>
typedef MTKView* RenderViewPtr;
#endif

typedef NSWindow* NSWindowPtr;
typedef id<NSWindowDelegate> OSXWindowDelegatePtr;
#else
#include <objc/objc.h>

typedef id RenderViewPtr;

typedef id NSWindowPtr;
typedef id OSXWindowDelegatePtr;
#endif

namespace Hope {
    
    namespace Graphics {
        class IDeviceContext;
    }
    
    namespace Core {

        class OSXWindow : public IWindow
        {
        public:
            OSXWindow(const char* windowName, u32 w, u32 h);
            ~OSXWindow();
            
            virtual bool Initialise() override;
            
            virtual void Show(bool show) override;
            virtual void Close() override;
            
            void ResizeToFrame();
            virtual void Resize(u32 w, u32 h) override;

            virtual void SetDisplayMode(EDisplayMode displayMode) override;
            
            bool RegisterRenderView(RenderViewPtr renderView);
            
            void* GetWindowHandle() const;
            
        protected:
            virtual void Destroy() override;
            
        private:
            NSWindowPtr mWindow;
            RenderViewPtr mRenderView;
            OSXWindowDelegatePtr mWindowDelegate;
        };
    }
}
