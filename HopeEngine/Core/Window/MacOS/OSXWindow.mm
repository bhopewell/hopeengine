//
//  OSXWindow.mm
//  HopeEngine
//
//  Created by Ben Hopewell on 20/11/16.
//  Copyright © 2016 Ben Hopewell. All rights reserved.
//

//TODO(Ben): Move all View class to Graphics::Renderer directory -
//              View can be created by the renderer and registered with the Window
//              Don't need to create the view within the Window initialisation

#import "OSXWindow.h"
#import <Cocoa/Cocoa.h>
#import <Foundation/Foundation.h>

#include <SDL.h>
#include <iostream>

using namespace Hope::Core;

@interface OSXWindowDelegate : NSObject<NSWindowDelegate>
{
    Hope::Core::OSXWindow* window;
}
@end

@implementation OSXWindowDelegate

-(void)handleQuit:(__unused id)sender
{
    window->Close();
}

-(id)initWithWindow:(Hope::Core::OSXWindow*)wnd
{
    if(self = [super init])
    {
        window = wnd;
    }
    
    return self;
}

-(void)windowWillClose:(__unused NSNotification *)notification
{
    window->Close();
}

-(void)windowDidResize:(__unused NSNotification *)notification
{
    window->ResizeToFrame();
}

@end

OSXWindow::OSXWindow(const char* windowName, u32 w, u32 h) :
    Hope::Core::IWindow(windowName, w, h)
{
    
}

OSXWindow::~OSXWindow()
{
}

void* OSXWindow::GetWindowHandle() const
{
    return (__bridge void*)mWindow;
}

bool OSXWindow::Initialise()
{
    //Initialise SDL
    SDL_Init(SDL_INIT_EVERYTHING);
    
    NSScreen* screen = [NSScreen mainScreen];
    
    //Calculate the positioning of the window
    f32 xPos = (screen.frame.size.width / 2.0f) - (mWidth / 2.0f);
    f32 yPos = (screen.frame.size.height / 2.0f) - (mHeight / 2.0f);
    
    NSRect frame = NSMakeRect(xPos, yPos, mWidth, mHeight);
    u32 styleMask = NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable;
    
    //TODO(Ben): Implement resize flag as parameter
    //Is having resize functionality worth implementing?
    bool canResize = true;
    if(canResize)
        styleMask |= NSWindowStyleMaskResizable;
    
    //Create the window
    mWindow = [[NSWindow alloc] initWithContentRect:frame styleMask:styleMask backing:NSBackingStoreBuffered defer:false screen:screen];
    mWindow.title = [NSString stringWithUTF8String:mWindowName];
    mWindow.releasedWhenClosed = true;
    
    mWindow.acceptsMouseMovedEvents = true;
    mWindowDelegate = [[OSXWindowDelegate alloc] initWithWindow:this];
    mWindow.delegate = mWindowDelegate;
    
    [mWindow setCollectionBehavior:NSWindowCollectionBehaviorFullScreenPrimary];
    
    //Toggle fullscreen according to initial mIsFullscreen value.
    //SetFullscreen(mIsFullscreen);

    
    //Make the window the key window and show it
    [mWindow makeKeyWindow];
    
    //Initialise SDL with the Window in order to correctly support input.
    SDL_CreateWindowFrom((__bridge void*)mWindow);
    
    return true;
}

bool OSXWindow::RegisterRenderView(RenderViewPtr renderView)
{
    if(mRenderView != nullptr)
        return false;
    
    mRenderView = renderView;
    mWindow.contentView = renderView;
    return true;
}

void OSXWindow::Destroy()
{
    if(mRenderView != nil)
        mRenderView = nil;
    
    if(mWindow != nil)
    {
        mWindow.delegate = nil;
        [mWindow close];
        mWindow = nil;
    }
    
    if(mWindowDelegate != nil)
    {
        mWindowDelegate = nil;
    }
}

void OSXWindow::Show(bool show)
{
    if(show)
        [mWindow orderFront:nil];
    else
        [mWindow orderOut:nil];
}

#include "Engine.h" //This only exists until there is proper quit code for when the window is closed...
void OSXWindow::Close()
{
    Game::IsRunning = false; //This only exists until there is proper quit code for when the window is closed...
    
    //Shutdown SDL
    SDL_Quit();
    
    //Destroy the window and release all referencea
    Destroy();
}

void OSXWindow::ResizeToFrame()
{
    CGSize newSize = mWindow.frame.size;
    Resize(newSize.width, newSize.height);
}

void OSXWindow::Resize(u32 w, u32 h)
{
    //TODO(Ben):    Implement a broadcast system. This should then broadcast a
    //              Window Resize event allowing any and all listeners to handle the event
    printf("Resize: w=%u h=%u x=%f y=%f\n", w, h, mRenderView.frame.size.width, mRenderView.frame.size.height);
}

void OSXWindow::SetDisplayMode(Hope::Core::EDisplayMode displayMode)
{    
//    //Ensure the application is fullscreen with the OS before making the change
//    NSApplicationPresentationOptions options = [[NSApplication sharedApplication] presentationOptions];
//    bool currentlyFullscreen = options & NSApplicationPresentationFullScreen;
//    
//    if(currentlyFullscreen != fullscreen)
//    {
//        [mWindow toggleFullScreen:nil];
//    }
//    
//    //Update the fullscreen value. This should be synced upon initialisation
//    IWindow::SetDisplayMode(displayMode);
}
