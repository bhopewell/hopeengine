#ifdef _WIN32

#include <iostream>
#include <assert.h>
#include <SDL2/include/SDL.h>

#include "Win32Window.h"
#include "RenderCore/Renderer/IRenderer.h"
#include "RenderCore/DeviceContext/IDeviceContext.h"
#include "RenderCore/DeviceContext/DeviceContextFactory.h"

using namespace Hope::Core;

Win32Window::Win32Window(void* hInstance, i32 cmdShow, const char* windowName, u32 w, u32 h) :
	IWindow(windowName, w, h),
	mHInstance((HINSTANCE)hInstance),
	mHWnd(nullptr),
	mCmdShow(cmdShow),
	mWindowedStyle(WS_OVERLAPPEDWINDOW, WS_EX_OVERLAPPEDWINDOW | WS_EX_APPWINDOW),
	mBorderlessStyle(WS_POPUP, WS_EX_APPWINDOW),
	mWindowRect{}
{
}

Win32Window::~Win32Window()
{
	mHInstance = nullptr;
	mHWnd = nullptr;
}

bool Win32Window::Initialise()
{
    //Initialise SDL
    SDL_Init(SDL_INIT_EVERYTHING);
    
	//Create the window
	if (!CreateWin32Window(mWindowName, mWidth, mHeight, false))
	{
		//Failed to create the window
		return false;
	}

	if (!GetWindowRect(mHWnd, &mWindowRect))
	{
		//Failed to get the window rect that we use for fullscreen toggling
		return false;
	}

    SDL_CreateWindowFrom(mHWnd);

#if _DEBUG
	CreateDebugConsole();
#endif
    
	return true;
}

void Win32Window::Destroy()
{
#ifdef _DEBUG
	FreeDebugConsole();
#endif

	DestroyWindow(mHWnd);
	UnregisterClass(mClassName, mHInstance);
}

void Win32Window::Show(bool show)
{
	if (show)
		ShowWindow(mHWnd, SW_SHOW);
	else
		ShowWindow(mHWnd, SW_HIDE);
}

#include "Engine.h" //This only exists until there is proper quit code for when the window is closed...
void Win32Window::Close()
{
	//Shutdown SDL
	SDL_Quit();
    
    //Destroy the window and release all referencea
    Destroy();
}

void Win32Window::Resize(u32 w, u32 h)
{
	//Overwrite the local window width/height
	mWidth = w;
	mHeight = h;

	//Resize the renderer
	Engine::GetRenderer()->Resize(mWidth, mHeight);
}

void Win32Window::SetDisplayMode(EDisplayMode displayMode)
{
	//Windowed: WindowedStyle + winRectSize
	//BorderlessWindow: BorderlessStyle + winRectSize
	//WindowedFullscreen: WindowedStyle + fullscreen
	//Fullscreen: BorderlessStyle + fullscreen

	i32 w = 0;
	i32 h = 0;
	i32 x = 0;
	i32 y = 0;

	//Change the window style
	switch (displayMode)
	{
	case EDisplayMode::EWindowed:
	case EDisplayMode::EWindowedFullscreen:
	{
		SetWindowLong(mHWnd, GWL_STYLE, mWindowedStyle.Style);
		SetWindowLong(mHWnd, GWL_EXSTYLE, mWindowedStyle.ExtendedStyle);
	}
	break;
	case EDisplayMode::EBorderlessWindow:
	case EDisplayMode::EFullscreen:
	{
		SetWindowLong(mHWnd, GWL_STYLE, mBorderlessStyle.Style);
		SetWindowLong(mHWnd, GWL_EXSTYLE, mBorderlessStyle.ExtendedStyle);
	}
	break;
	}

	//Get the desired position and size of the window
	switch (displayMode)
	{
	case EDisplayMode::EWindowed:
	case EDisplayMode::EBorderlessWindow:
	{
		//Get the desired window pos and size
		x = mWindowRect.left;
		y = mWindowRect.top;
		w = mWindowRect.right - mWindowRect.left;
		h = mWindowRect.bottom - mWindowRect.top;
	}
	break;
	case EDisplayMode::EFullscreen:
	case EDisplayMode::EWindowedFullscreen:
	{
		//Get the desired window size
		w = GetSystemMetrics(SM_CXSCREEN);
		h = GetSystemMetrics(SM_CYSCREEN);
	}
	break;
	default:
		//This should never happen
		std::cout << "DisplayMode=" << (u8)displayMode << std::endl;
		assert(false);
	}

	//Change the window size and position
	SetWindowPos(mHWnd, 0, x, y, w, h, SWP_SHOWWINDOW);

	//Update the width and height of the window.
	mWidth = w;
	mHeight = h;

	mDisplayMode = displayMode;
}

#ifdef _DEBUG
void Win32Window::CreateDebugConsole()
{
	AllocConsole();
	FILE* pCout = NULL;
	freopen_s(&pCout, "conout$", "w", stdout);
}

void Win32Window::FreeDebugConsole()
{
	FreeConsole();
}
#endif

bool Win32Window::CreateWin32Window(const char* windowName, u32 w, u32 h, bool fullscreen)
{
	//Create the class description
	WNDCLASSEX wnd;
	wnd.cbSize = sizeof(WNDCLASSEX);
	wnd.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wnd.lpfnWndProc = WndProc;
	wnd.cbClsExtra = 0;
	wnd.cbWndExtra = 0;
	wnd.hInstance = mHInstance;
	wnd.hIcon = LoadIcon(mHInstance, MAKEINTRESOURCE(IDI_APPLICATION));
	wnd.hCursor = LoadCursor(NULL, IDC_ARROW);
	wnd.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wnd.lpszMenuName = NULL;
	wnd.lpszClassName = mClassName;
	wnd.hIconSm = LoadIcon(mHInstance, MAKEINTRESOURCE(IDI_APPLICATION));

	//Register the class description
	if (!RegisterClassEx(&wnd))
	{
		DWORD err = GetLastError();
		return false;
	}

	HWND newHWnd;
	if (fullscreen)
	{
		//Overwrite the Width and Height of the window.
		mWidth = GetSystemMetrics(SM_CXSCREEN);
		mHeight = GetSystemMetrics(SM_CYSCREEN);

		//Create the window
		newHWnd = CreateWindowEx(
			mBorderlessStyle.ExtendedStyle,
			mClassName,
			mWindowName,
			mBorderlessStyle.Style,
			0, 0, //X and Y position of the Window
			mWidth, mHeight,
			NULL,
			NULL,
			mHInstance,
			NULL
			);
	}
	else
	{
		//Default position of window
		i32 x = (GetSystemMetrics(SM_CXSCREEN) - mWidth) / 2;
		i32 y = (GetSystemMetrics(SM_CYSCREEN) - mHeight) / 2;

		//Create the window
		newHWnd = CreateWindowEx(
			mWindowedStyle.ExtendedStyle,
			mClassName,
			mWindowName,
			mWindowedStyle.Style,
			x, y, //X and Y position of the Window
			w, h,
			NULL,
			NULL,
			mHInstance,
			NULL
			);
	}

	//Failed to create the window, return false
	if (newHWnd == 0)
	{
		DWORD err = GetLastError();
		return false;
	}

	mHWnd = newHWnd;

	return true;
}

LRESULT __stdcall Hope::Core::WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg)
	{
	case WM_CLOSE:
		Game::IsRunning = false; //This only exists until there is proper quit code for when the window is closed...
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	case WM_SIZE:
		//Resize the Window/Renderer
		Engine::GetWindow()->Resize(LOWORD(lParam), HIWORD(lParam));
		break;
	// not handled
	default:
		return DefWindowProc(hWnd, msg, wParam, lParam);
	}

	return 0;
}

#endif
