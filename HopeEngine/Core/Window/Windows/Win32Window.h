#pragma once

#ifdef _WIN32

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

#include "Hope.h"
#include "../IWindow.h"

namespace Hope {
	namespace Graphics {
		class IDeviceContext;
	}

	namespace Core {

		struct WindowStyle
		{
			WindowStyle(DWORD style, DWORD exStyle) :
				Style{ style },
				ExtendedStyle{ exStyle }
			{
			}

			DWORD Style;
			DWORD ExtendedStyle;
		};

		class Win32Window : public IWindow
		{
		public:
			Win32Window(void* hInstance, i32 cmdShow, const char* windowName, u32 w, u32 h);
			~Win32Window();

			inline const HWND GetHWND() const
			{
				return mHWnd;
			}

			virtual bool Initialise() override;
			
			virtual void Show(bool show) override;
			virtual void Close() override;
			
			virtual void Resize(u32 w, u32 h) override;

			virtual void SetDisplayMode(EDisplayMode displayMode) override;

		protected:
			virtual void Destroy() override;

#if _DEBUG
			void CreateDebugConsole();
			void FreeDebugConsole();
#endif

		private:
			bool CreateWin32Window(const char* windowName, u32 w, u32 h, bool fullscreen);

		private:
			HINSTANCE mHInstance;
			HWND mHWnd;
			i32 mCmdShow;

			const char* mClassName = "Win32 Window";

			const WindowStyle mWindowedStyle;
			const WindowStyle mBorderlessStyle;
			RECT mWindowRect;
		};

		LRESULT __stdcall WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
	}
}

#endif
