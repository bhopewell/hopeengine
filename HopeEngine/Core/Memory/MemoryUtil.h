//
//  MemoryUtil.h
//  HopeEngine
//
//  Created by Ben Hopewell on 20/12/16.
//  Copyright © 2016 Ben Hopewell. All rights reserved.
//

#pragma once

//TODO(Ben): Probably move these macros to a util header.
#define BytesToKilobytes(b) b / 1024
#define KilobytesToMegabytes(kb) kb / 1024
#define BytesToMegabytes(b) KilobytesToMegabytes(BytesToKilobytes(b))

#define KilobytesToBytes(kb) kb * 1024
#define MegabytesToKilobytes(mb) mb * 1024
#define MegabytesToBytes(mb) KilobytesToBytes(MegabytesToKilobytes(mb))
