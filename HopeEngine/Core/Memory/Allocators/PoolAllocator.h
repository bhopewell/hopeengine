//
//  PoolAllocator.h
//  HopeEngine
//
//  Created by Ben Hopewell on 22/10/18.
//  Copyright © 2018 Ben Hopewell. All rights reserved.
//

#pragma once

#include "HopeAllocator.h"
#include "PoolFreeList.h"

namespace Hope
{
    namespace Core
    {
        class PoolAllocator : public HopeAllocator
        {
        public:
            
            template<typename T = void>
            class PoolIterator
            {
            public:
                PoolIterator() :
                    mCurrentObjAsVoid(nullptr),
                    mEnd(nullptr),
                    mFreeList(),
                    mFreeListIt(),
                    mBlockSize(0)
                {
                    
                }
                
                PoolIterator(void* begin, void* end, PoolFreeList freeList, const u32 blockSize) :
                    mCurrentObjAsVoid(begin),
                    mEnd(end),
                    mFreeList(freeList),
                    mFreeListIt(freeList.begin()),
                    mBlockSize(blockSize)
                {
                }
                
                //Up to O(n)
                inline PoolIterator& operator++()
                {
                    //Move the current obj iterator
                    mCurrentObjAsChar += mBlockSize;
                    
                    //Ensure it is not within the free list
                    while(mFreeListIt != mFreeList.end() &&
                          *mFreeListIt == mCurrentObjAsVoid)
                    {
                        mCurrentObjAsChar += mBlockSize;
                        ++mFreeListIt;
                    }
                    
                    //Ensure we never breach the bounds of the Pool
                    assert(mCurrentObjAsVoid <= mEnd);
                    return *this;
                }
                
                inline T* operator*() const { return mCurrentObjAsType; }
                
                inline bool operator==(const PoolIterator<T>& it) const { return it.mCurrentObjAsVoid == this->mCurrentObjAsVoid; }
                inline bool operator!=(const PoolIterator<T>& it) const { return it.mCurrentObjAsVoid != this->mCurrentObjAsVoid; }
                
            private:
                union
                {
                    char* mCurrentObjAsChar; //Used for iteration of individual bytes
                    void* mCurrentObjAsVoid;
                    T* mCurrentObjAsType;
                };
                
                void* mEnd;
                
                PoolFreeList mFreeList;
                PoolFreeList::PoolFreeListIterator mFreeListIt;
                
                u32 mBlockSize;
            };
            
        public:
            //Allocates the heap memory upon construction - The PoolAllocator will then manage the new memory.
            explicit PoolAllocator(const u32 totalMemBytes, const u32 objectBytes, const u32 alignment = 4);
            //Heap memory already allocated - PoolAllocator simply manages the pre-existing memory.
            explicit PoolAllocator(void* mem, const u32 totalMemBytes, const u32 objectBytes, const u32 alignment = 4);
            ~PoolAllocator();
            
            void* Allocate(u32 memBytes, const u32 alignment = 4) override;
            void Free(void *mem) override;
            
            inline bool Full() const { return mBytesUsed == mTotalMemoryBytes; }
            
            template <typename T>
            inline PoolIterator<T> begin()
            {
                // If the Pool is empty, return end
                if(mBytesUsed == 0)
                    return end<T>();
                
                return PoolIterator<T>(mMemory,
                                    reinterpret_cast<char*>(mMemory) + mTotalMemoryBytes,
                                    mFreeList,
                                    mObjectBytes + mPoolElementAlign);
            }
            inline PoolIterator<> begin() { return begin<void>(); }
            
            template <typename T>
            inline PoolIterator<T> end() { return PoolIterator<T>(reinterpret_cast<char*>(mMemory) + mTotalMemoryBytes,
                                                        reinterpret_cast<char*>(mMemory) + mTotalMemoryBytes,
                                                        mFreeList,
                                                        mObjectBytes + mPoolElementAlign); }
            inline PoolIterator<> end() { return end<void>(); }
            
        protected:
            const u32 mObjectBytes;
            const u32 mAlignment;
            
            // The calculated alignment of each individual element in the pool
            u32 mPoolElementAlign;
            
            PoolFreeList mFreeList;
            
        private:
            friend class DynamicPoolAllocator;
        };
    }
}
