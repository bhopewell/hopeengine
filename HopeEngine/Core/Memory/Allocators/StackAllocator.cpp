//
//  StackAllocator.cpp
//  HopeEngine
//
//  Created by Ben Hopewell on 27/10/18.
//  Copyright © 2018 Ben Hopewell. All rights reserved.
//

#include "StackAllocator.h"
#include <iostream>

using namespace Hope::Core;

StackAllocator::StackAllocator(const u32 totalMemBytes) :
HopeAllocator(totalMemBytes)
{
    
}

StackAllocator::StackAllocator(void* mem, const u32 totalMemBytes) :
HopeAllocator(mem, totalMemBytes)
{
    
}

StackAllocator::~StackAllocator()
{
    Clear();
}

void* StackAllocator::Allocate(u32 memBytes, const u32 alignment)
{
    if(memBytes <= 0)
    {
        //Cannot allocate <= 0 bytes
        assert(false);
        return nullptr;
    }
    
    uintptr_t nextMemAddr = reinterpret_cast<uintptr_t>(mMemory) + mBytesUsed;
    
    // AlignAdjustment includes the size of the header so there is no need to add it to again after this call
    u8 alignAdjustment = HopeAllocator::CalculateAlignAdjustment(nextMemAddr, alignment, sizeof(StackAllocHeader));
    
    //Exceeded the memory bounds
    if(mBytesUsed + memBytes + alignAdjustment > mTotalMemoryBytes)
    {
        //Cannot exceed memory bounds
        assert(false);
        return nullptr;
    }
    
    mBytesUsed += memBytes + alignAdjustment;
    ++mAllocations;
    
    //Align the memory
    nextMemAddr += alignAdjustment;
    
    //Write the allocation header
    StackAllocHeader* allocHeader = reinterpret_cast<StackAllocHeader*>(nextMemAddr - sizeof(StackAllocHeader));
    allocHeader->mAlignment = alignAdjustment;
    allocHeader->mAllocSize = memBytes;
    
    return reinterpret_cast<void*>(nextMemAddr);
}

void StackAllocator::Free(void *mem)
{
    uintptr_t nextMemAddr = reinterpret_cast<uintptr_t>(mMemory) + mBytesUsed;
    void* nextMemAddrVoidPtr = reinterpret_cast<void*>(nextMemAddr);
    
    //Assert the mem is within the valid memory range
    assert(mMemory <= mem);
    assert(mem <= nextMemAddrVoidPtr);
    
    //Get the allocated header and the expected alloc size based on the mem* and the nextMemAddr
    StackAllocHeader* header = reinterpret_cast<StackAllocHeader*>(reinterpret_cast<unsigned char*>(mem) - sizeof(StackAllocHeader));
    uintptr_t currentMemAddr = reinterpret_cast<uintptr_t>(mem);
    uintptr_t expectedAllocSize = nextMemAddr - currentMemAddr;
    
    //Verify the memory being freed is the most recently allocated memory.
    assert(expectedAllocSize == header->mAllocSize);
    
    //Deallocate the memory
    mBytesUsed -= nextMemAddr - (currentMemAddr - header->mAlignment);
    //mCurrentMarker = mBytesUsed;
    --mAllocations;
}

void StackAllocator::FreeToMarker(u32 marker)
{
    assert(mBytesUsed >= marker);
    
    //Deallocate to the marker
    //mCurrentMarker = marker;
    mBytesUsed = marker;
}
