//
//  DynamicPoolAllocator.h
//  HopeEngine
//
//  Created by Ben Hopewell on 17/1/19.
//  Copyright © 2019 Ben Hopewell. All rights reserved.
//

#pragma once

#include "HopeAllocator.h"
#include "PoolAllocator.h"

#include <list>

namespace Hope {
    namespace Core {
        
        class DynamicPoolAllocator
        {
        public:
            
            template<typename T = void>
            class DynamicPoolIterator
            {
            public:
               
                // Create an end iterator
                DynamicPoolIterator(std::list<PoolAllocator*>& allocators) :
                    mCurrentItAsVoid(), // Create an default(end) PoolIterator
                    mAllocatorCollection(allocators)
                {
                    mCurrentPoolAllocator = mAllocatorCollection.end();
                }
                
                // Create a standard iterator
                DynamicPoolIterator(PoolAllocator::PoolIterator<T> begin,
                                    std::list<PoolAllocator*>& allocators) :
                    mCurrentItAsType(begin),
                    mAllocatorCollection(allocators)
                {
                    if(mAllocatorCollection.size() > 0)
                    {
                        // Find the allocator that mCurrentItAsType belongs to
                        bool currentAllocatorFound = false;
                        for(std::list<PoolAllocator*>::iterator it = mAllocatorCollection.begin();
                            it != mAllocatorCollection.end();
                            ++it)
                        {
                            PoolAllocator::PoolIterator allocBegin = (*it)->begin();
                            PoolAllocator::PoolIterator allocEnd = (*it)->end();
                            
                            if(*mCurrentItAsVoid >= *allocBegin && *mCurrentItAsVoid <= *allocEnd)
                            {
                                mCurrentPoolAllocator = it;
                                currentAllocatorFound = true;
                                break;
                            }
                        }
                        
                        assert(currentAllocatorFound);
                    }
                }
                
                inline DynamicPoolIterator& operator++()
                {
                    // Do not permit iterating past the end
                    if(mCurrentItAsVoid == PoolAllocator::PoolIterator())
                    {
                        assert(false && "Cannot iterate past the end of the DynamicPoolAllocator");
                    }
                    
                    //Move to the next object
                    ++mCurrentItAsType;
                    
                    //If we've reached the end of the allocators memory, move to the next allocator
                    if(mCurrentItAsVoid == (*mCurrentPoolAllocator)->end())
                    {
                        // Move to the next allocator
                        ++mCurrentPoolAllocator;
                        if(mCurrentPoolAllocator != mAllocatorCollection.end())
                        {
                            mCurrentItAsVoid = (*mCurrentPoolAllocator)->begin();
                        }
                        else
                        {
                            // Null out the iterator as it is now at the end of the collection
                            mCurrentItAsVoid = PoolAllocator::PoolIterator();
                        }
                    }
                    
                    return *this;
                }
                
                inline T* operator*() const { return *mCurrentItAsType; }
                inline T* operator->() const { return *mCurrentItAsType; }
                
                inline bool operator==(const DynamicPoolIterator<T>& it) const
                {
                    return *(it.mCurrentPoolAllocator) == *(this->mCurrentPoolAllocator) &&
                                it.mCurrentItAsVoid == this->mCurrentItAsVoid;
                }
                
                inline bool operator!=(const DynamicPoolIterator<T>& it) const
                {
                    return *(it.mCurrentPoolAllocator) != *(this->mCurrentPoolAllocator) ||
                                it.mCurrentItAsVoid != this->mCurrentItAsVoid;
                }
                
            private:
                union
                {
                    PoolAllocator::PoolIterator<T> mCurrentItAsType;
                    PoolAllocator::PoolIterator<void> mCurrentItAsVoid;
                };
                
                std::list<PoolAllocator*>::iterator mCurrentPoolAllocator;
                std::list<PoolAllocator*>& mAllocatorCollection;
            };
            
        public:
            //Allocates the heap memory upon construction - The PoolAllocator will then manage the new memory.
            explicit DynamicPoolAllocator(const u32 objectBytes, const u32 alignment = 4);
            //Delete the constructor that provides the allocator with memory - Unsupported
            explicit DynamicPoolAllocator(void* mem, const u32 totalMemBytes, const u32 objectBytes, const u32 alignment = 4) = delete;
            
            ~DynamicPoolAllocator();
            
            void* AllocateObject();
            void Free(void *mem);
            void Clear();
            
            template <typename T>
            inline DynamicPoolIterator<T> begin() {
                
                std::list<PoolAllocator*>::iterator begin = mAllocators.begin();
                
                // Create an end iterator
                if(begin == mAllocators.end())
                    return DynamicPoolIterator<T>(mAllocators);
                
                // Create a valid iterator
                return DynamicPoolIterator<T>((*begin)->begin<T>(),
                                              mAllocators);
            }
            inline DynamicPoolIterator<> begin() { return begin<void>(); }
            
            template <typename T>
            inline DynamicPoolIterator<T> end() {
                // Create an end iterator
                return DynamicPoolIterator<T>(mAllocators);
            }
            inline DynamicPoolIterator<> end() { return end<void>(); }
            
        protected:
            static const u32 mMaxObjectsPerAllocator = 10;
            
            const u32 mObjectBytes;
            const u32 mAlignment;
            
            u32 mTotalMemoryBytes;
            u32 mBytesUsed;
            u32 mAllocations;
            
            std::list<PoolAllocator*> mAllocators;
        };
    }
}
