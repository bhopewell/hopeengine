//
//  HopeAllocator.cpp
//  HopeEngine
//
//  Created by Ben Hopewell on 25/10/18.
//  Copyright © 2018 Ben Hopewell. All rights reserved.
//

#include "HopeAllocator.h"
#include <cstdlib>

using namespace Hope::Core;

/*
 GameWorld Class
 - ComponentManager - Pool Allocator?
    - ConstructComponent<T>
 - EntityManager - Pool Allocator?
    - ConstructEntity<T>
 */

void* operator new (std::size_t size, Hope::Core::HopeAllocator* allocator)
{
    return allocator->Allocate((u32)size);
}

void* operator new (std::size_t size, Hope::Core::HopeAllocator& allocator)
{
    return allocator.Allocate((u32)size);
}

void operator delete (void* p, Hope::Core::HopeAllocator* allocator)
{
    allocator->Free(p);
}

HopeAllocator::HopeAllocator(const u32 totalMemoryBytes) :
mBytesUsed(0),
mAllocations(0),
mFreeable(true),
mTotalMemoryBytes(totalMemoryBytes)
{
    mMemory = malloc(totalMemoryBytes);
}

HopeAllocator::HopeAllocator(void* mem, const u32 totalMemoryBytes) :
mMemory(mem),
mBytesUsed(0),
mAllocations(0),
mFreeable(true),
mTotalMemoryBytes(totalMemoryBytes)
{
}

HopeAllocator::~HopeAllocator()
{
    if(mFreeable)
    {
        free(mMemory);
        mMemory = nullptr;
    }
}

void HopeAllocator::Clear()
{
    mBytesUsed = 0;
    mAllocations = 0;
}

u8 HopeAllocator::CalculateAlignAdjustment(const uintptr_t ptr, const u32 alignment)
{
    if(alignment == 0)
        return 0;
    
    assert(alignment >= 1);
    assert(alignment <= 128);
    assert((alignment & (alignment - 1)) == 0); //Ensure the alignment is power of 2.
    
    return (alignment - (ptr & (alignment - 1))) & (alignment - 1);
}

u8 HopeAllocator::CalculateAlignAdjustment(const uintptr_t ptr, const u32 alignment, const u32 additionalBytes)
{
    u8 finalAlignment = CalculateAlignAdjustment(ptr, alignment);
    u8 extraAlignBytes = additionalBytes;
    
    if (finalAlignment < additionalBytes)
    {
        extraAlignBytes -= finalAlignment;
        
        //Increase adjustment to fit header
        finalAlignment += alignment * (additionalBytes / alignment);
        if (additionalBytes % alignment > 0)
            finalAlignment += alignment;
    }
    
    return finalAlignment;
}
