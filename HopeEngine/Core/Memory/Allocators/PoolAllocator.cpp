//
//  PoolAllocator.cpp
//  HopeEngine
//
//  Created by Ben Hopewell on 22/10/18.
//  Copyright © 2018 Ben Hopewell. All rights reserved.
//

#include "PoolAllocator.h"

using namespace Hope::Core;

PoolAllocator::PoolAllocator(const u32 totalMemBytes, const u32 objectBytes, const u32 alignment) :
HopeAllocator(totalMemBytes),
mObjectBytes(objectBytes),
mAlignment(alignment),
mFreeList(mMemory, mTotalMemoryBytes, objectBytes, alignment)
{
    //Calculate the alignment per element - this will be the same for all elements in the pool
    uintptr_t nextAsUIntPtr = reinterpret_cast<uintptr_t>(mMemory);
    mPoolElementAlign = HopeAllocator::CalculateAlignAdjustment(nextAsUIntPtr + objectBytes, alignment);
}

PoolAllocator::PoolAllocator(void* mem, const u32 totalMemBytes, const u32 objectBytes, const u32 alignment) :
HopeAllocator(mem, totalMemBytes),
mObjectBytes(objectBytes),
mAlignment(alignment),
mFreeList(mMemory, mTotalMemoryBytes, objectBytes, alignment)
{
    //Calculate the alignment per element - this will be the same for all elements in the pool
    uintptr_t nextAsUIntPtr = reinterpret_cast<uintptr_t>(mMemory);
    mPoolElementAlign = HopeAllocator::CalculateAlignAdjustment(nextAsUIntPtr + objectBytes, alignment);
}

PoolAllocator::~PoolAllocator()
{
    Clear();
}

void* PoolAllocator::Allocate(u32 memBytes, const u32 alignment)
{
    if(alignment != mAlignment)
    {
        //Cannot allocate to different alignment
        assert(false);
        return nullptr;
    }
    
    if(memBytes <= 0)
    {
        //Cannot allocate <= 0 bytes
        assert(false);
        return nullptr;
    }
    
    if(memBytes != mObjectBytes)
    {
        //Cannot allocate memory that is not equal to mObjectBytes
        assert(false);
        return nullptr;
    }
    
    // Calculate the total bytes used for the allocation - This will ensure that the alignment is taken into account
    u32 totalBytesForAllocation = memBytes + mPoolElementAlign;
    
    //Exceeded the memory bounds
    if(mBytesUsed + totalBytesForAllocation > mTotalMemoryBytes)
    {
        //Cannot exceed memory bounds
        assert(false);
        return nullptr;
    }
    
    mBytesUsed += totalBytesForAllocation;
    ++mAllocations;
    return mFreeList.Get();
}

void PoolAllocator::Free(void *mem)
{
    mBytesUsed -= (mObjectBytes + mPoolElementAlign);
    --mAllocations;
    mFreeList.Release(mem);
}
