//
//  PoolFreeList.h
//  HopeEngine
//
//  Created by Ben Hopewell on 4/1/19.
//  Copyright © 2019 Ben Hopewell. All rights reserved.
//

#pragma once

#include "HopeAllocator.h"

namespace Hope
{
    namespace Core
    {
        class PoolFreeListIterator;
        
        class PoolFreeList
        {
        public:
            
            class PoolFreeListIterator
            {
            public:
                PoolFreeListIterator() :
                    mCurrentObjAsFreeList(nullptr)
                {
                }
                
                PoolFreeListIterator(PoolFreeList* begin) :
                    mCurrentObjAsFreeList(begin)
                {
                }
                
                //O(1)
                inline PoolFreeListIterator& operator++()
                {
                    mCurrentObjAsFreeList = mCurrentObjAsFreeList->mNext;
                    return *this;
                }
                
                inline void* operator*() const { return mCurrentObjAsFreeList; }
                
                inline bool operator==(const PoolFreeListIterator& it) const { return it.mCurrentObjAsFreeList == this->mCurrentObjAsFreeList; }
                inline bool operator!=(const PoolFreeListIterator& it) const { return it.mCurrentObjAsFreeList != this->mCurrentObjAsFreeList; }
                
            private:
                PoolFreeList* mCurrentObjAsFreeList;
            };
            
        public:
            PoolFreeList() :
                mNext(nullptr)
            {
            }
            
            PoolFreeList(void* start, const u32 totalMemBytes, const u32 objectBytes, const u32 alignment)
            {
                union
                {
                    char* nextAsChar;
                    uintptr_t nextAsUIntPtr;
                    PoolFreeList* nextAsFreeList;
                };
                
                nextAsFreeList = static_cast<PoolFreeList*>(start);
                
                //Initialise to the first element in the Linked List
                mNext = nextAsFreeList;
                
                //Calculate the alignment - this will be the same for all elements in the pool
                u8 alignAdjustment = HopeAllocator::CalculateAlignAdjustment(nextAsUIntPtr + objectBytes, alignment);
                
                //Move and align the pointer
                nextAsChar += objectBytes + alignAdjustment;
                
                //Initialise the rest of the Linked List
                PoolFreeList* it = mNext;
                
                uintptr_t end = reinterpret_cast<uintptr_t>(start) + totalMemBytes;
                while(nextAsUIntPtr < end)
                {
                    it->mNext = nextAsFreeList;
                    it = it->mNext;
                    
                    nextAsChar += (objectBytes + alignAdjustment);
                }
                
                //Initialise the end of the list
                it->mNext = nullptr;
            }
            
            void* Get()
            {
                //If there are no further entries in the PoolNode list, return nullptr
                if(mNext == nullptr)
                    return nullptr;
                
                //Return the head memory block
                PoolFreeList* head = mNext;
                mNext = head->mNext;
                return head;
            }
            
            void Release(void* mem)
            {
                //Release the memory block back to the allocator
                PoolFreeList* head = static_cast<PoolFreeList*>(mem);
                head->mNext = mNext;
                mNext = head;
            }
            
            PoolFreeListIterator begin() { return PoolFreeListIterator(mNext); }
            PoolFreeListIterator end() { return PoolFreeListIterator(nullptr); }
            
        private:
            PoolFreeList* mNext;
        };
    }
}
