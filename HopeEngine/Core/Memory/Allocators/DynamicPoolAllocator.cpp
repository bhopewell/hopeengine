//
//  DynamicPoolAllocator.cpp
//  HopeEngine
//
//  Created by Ben Hopewell on 17/1/19.
//  Copyright © 2019 Ben Hopewell. All rights reserved.
//

#include "DynamicPoolAllocator.h"
#include <iostream>

using namespace Hope::Core;

DynamicPoolAllocator::DynamicPoolAllocator(const u32 objectBytes, const u32 alignment) :
mObjectBytes(objectBytes),
mAlignment(alignment),
mTotalMemoryBytes(0),
mBytesUsed(0),
mAllocations(0)
{
}

DynamicPoolAllocator::~DynamicPoolAllocator()
{
    //Clear the allocator memory blocks
    Clear();
    
    //Clear up the linked list of allocators
    for(PoolAllocator* allocator : mAllocators)
        delete allocator;
    
    mAllocators.clear();
}

void* DynamicPoolAllocator::AllocateObject()
{
    // Assert if the object being allocated is smaller than the PoolFreeList pointer.
    // The PoolFreeList cannot handle such a case
    assert(mObjectBytes >= sizeof(PoolFreeList*));
    
    //Find the first allocator that is not full.
    PoolAllocator* freeAllocator = nullptr;
    for(PoolAllocator* allocator : mAllocators)
    {
        if(!allocator->Full())
        {
            freeAllocator = allocator;
            break;
        }
    }
    
    //Create a new allocator that will be used to allocate the memory
    if(freeAllocator == nullptr)
    {
        u32 totalAllocatorBytes = mMaxObjectsPerAllocator * mObjectBytes;
        
        //Create the new Pool Allocator
        mAllocators.emplace_back(new PoolAllocator(totalAllocatorBytes, mObjectBytes, mAlignment));
        freeAllocator = mAllocators.back();
        
        mTotalMemoryBytes += totalAllocatorBytes;
    }
    
    //Allocate the memory
    void* mem = freeAllocator->Allocate(mObjectBytes, mAlignment);
    assert(mem != nullptr);
    
    // Take into account the alignment of the freeAllocator when updating the bytes used.
    mBytesUsed += mObjectBytes + freeAllocator->mPoolElementAlign;
    ++mAllocations;
    return mem;
}

void DynamicPoolAllocator::Free(void *mem)
{
    //Find the pool that the memory belongs to and free it.
    for(PoolAllocator* allocator : mAllocators)
    {
        if(mem >= *(allocator->begin()) && mem < *(allocator->end()))
        {
            allocator->Free(mem);
            
            // Take into account the alignment of the free allocator when updating the bytes used.
            mBytesUsed -= (mObjectBytes + allocator->mPoolElementAlign);
            --mAllocations;
            return;
        }
    }
    
    std::cout << "Dynamic Pool Allocator could not Free memory. Memory may be corrupted." << std::endl;
    assert(false);
}

void DynamicPoolAllocator::Clear()
{
    for(PoolAllocator* allocator : mAllocators)
        allocator->Clear();
    
    mAllocations = 0;
    mBytesUsed = 0;
    mTotalMemoryBytes = 0;
}
