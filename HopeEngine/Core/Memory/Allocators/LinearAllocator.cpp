//
//  LinearAllocator.cpp
//  HopeEngine
//
//  Created by Ben Hopewell on 22/10/18.
//  Copyright © 2018 Ben Hopewell. All rights reserved.
//

#include "LinearAllocator.h"
#include <iostream>

using namespace Hope::Core;

LinearAllocator::LinearAllocator(const u32 totalMemBytes) :
HopeAllocator(totalMemBytes)
{
}

LinearAllocator::LinearAllocator(void* mem, const u32 totalMemBytes) :
HopeAllocator(mem, totalMemBytes)
{
}

LinearAllocator::~LinearAllocator()
{
    Clear();
}

void* LinearAllocator::Allocate(u32 memBytes, const u32 alignment)
{
    if(memBytes <= 0)
    {
        //Cannot allocate <= 0 bytes
        assert(false);
        return nullptr;
    }
    
    uintptr_t nextMemAddr = reinterpret_cast<uintptr_t>(mMemory) + mBytesUsed;
    u8 alignAdjustment = HopeAllocator::CalculateAlignAdjustment(nextMemAddr, alignment);
    
    //Exceeded the memory bounds
    if(mBytesUsed + memBytes + alignAdjustment > mTotalMemoryBytes)
    {
        //Cannot exceed memory bounds
        assert(false);
        return nullptr;
    }
    
    mBytesUsed += memBytes + alignAdjustment;
    ++mAllocations;
    
    //Align the memory address and return
    nextMemAddr += alignAdjustment;
    return reinterpret_cast<void*>(nextMemAddr);
}

void LinearAllocator::Free(void *mem)
{
    HOPE_UNUSED(mem)
    
    std::cout << "Free is unsupported. Use Clear method instead." << std::endl;
    assert(false);
}
