//
//  StackAllocator.h
//  HopeEngine
//
//  Created by Ben Hopewell on 27/10/18.
//  Copyright © 2018 Ben Hopewell. All rights reserved.
//

#pragma once

#include "HopeAllocator.h"

namespace Hope {
    namespace Core {
        
        class StackAllocator : public HopeAllocator
        {
        public:
            //Allocates the heap memory upon construction - StackAllocator will then manage the new memory.
            explicit StackAllocator(const u32 totalMemBytes);
            //Heap memory already allocated - StackAllocator simply manages the pre-existing memory.
            explicit StackAllocator(void* mem, const u32 totalMemBytes);
            ~StackAllocator();
            
            inline u32 GetMarker() const { return mBytesUsed; }
            
            void* Allocate(u32 memBytes, const u32 alignment = 4) override;
            void Free(void *mem) override;
            void FreeToMarker(u32 marker);
            
        protected:
            // StackAllocHeader Size should be 4 bytes
            struct StackAllocHeader
            {
                u8 mAlignment   : 8; // Max of 255
                u32 mAllocSize  : 24; // Max of ~16 million bytes (~16mb)
            };
        };
    }
}
