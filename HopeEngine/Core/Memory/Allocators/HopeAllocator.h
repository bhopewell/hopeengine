//
//  IHopeAllocator.h
//  HopeEngine
//
//  Created by Ben Hopewell on 22/10/18.
//  Copyright © 2018 Ben Hopewell. All rights reserved.
//

#pragma once

#include "Hope.h"
#include "Core/Memory/MemoryUtil.h"
#include <cassert>

namespace Hope
{
    namespace Core
    {
        class HopeAllocator
        {
        public:
            explicit HopeAllocator(const u32 totalMemoryBytes);
            explicit HopeAllocator(void* mem, const u32 totalMemoryBytes);
            virtual ~HopeAllocator();
            
            virtual void* Allocate(u32 memBytes, const u32 alignment = 4) = 0;
            virtual void Free(void* mem) = 0;
            virtual void Clear();
            
            static u8 CalculateAlignAdjustment(const uintptr_t ptr, const u32 alignment);
            static u8 CalculateAlignAdjustment(const uintptr_t ptr, const u32 alignment, const u32 additionalBytes);
            
        protected:
            void* mMemory;
            u32 mBytesUsed;
            u32 mAllocations;
            
            const bool mFreeable;
            const u32 mTotalMemoryBytes;
        };
    }
}

void* operator new (std::size_t size, Hope::Core::HopeAllocator* allocator);
void* operator new (std::size_t size, Hope::Core::HopeAllocator& allocator);
void operator delete (void* p, Hope::Core::HopeAllocator* allocator);
