//
//  LinearAllocator.h
//  HopeEngine
//
//  Created by Ben Hopewell on 22/10/18.
//  Copyright © 2018 Ben Hopewell. All rights reserved.
//

#pragma once

#include "HopeAllocator.h"

namespace Hope
{
    namespace Core
    {
        class LinearAllocator : public HopeAllocator
        {
        public:
            //Allocates the heap memory upon construction - LinearAllocator will then manage the new memory.
            explicit LinearAllocator(const u32 totalMemBytes);
            //Heap memory already allocated - LinearAllocator simply manages the pre-existing memory.
            explicit LinearAllocator(void* mem, u32 totalMemBytes);
            ~LinearAllocator();
            
            virtual void* Allocate(u32 memBytes, const u32 alignment = 4) override;
            virtual void Free(void* mem) override;
        };
    }
}
