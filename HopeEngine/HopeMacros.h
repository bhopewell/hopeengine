//
//  HopeMacros.h
//  HopeEngine
//
//  Created by Ben Hopewell on 18/04/2019.
//

#pragma once

#define HOPE_UNUSED(expr) do { (void)(expr); } while (0);

#define HOPE_UNIMPLEMENTED() printf("[WARN] %s unimplemented\n", __func__);
