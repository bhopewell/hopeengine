#include "Engine.h"

#ifdef _WIN32

int __stdcall WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
				LPSTR lpCmdLine, int nCmdShow)
{
	//Create the engine
	Hope::Engine* engine = new Hope::Engine();

	//Start up all the subsystems
	if (!engine->StartUp(hInstance, nCmdShow))
		return -1;

	//Run the main loop
	i32 result = engine->Run();
	//Shutdown all the subsystems
	engine->Shutdown();
	delete engine;

	return result;
}

#elif __APPLE__

int main(int argc, char** argv)
{
    HOPE_UNUSED(argc)
    HOPE_UNUSED(argv)
    
    //Create the engine
    Hope::Engine* engine = new Hope::Engine();
    
    //Start up all the subsystems
    if(!engine->StartUp())
        return -1;
    
    //Run the main loop
    i32 result = engine->Run();
    //Shutdown all the subsystems
    engine->Shutdown();
    delete engine;
    
    return result;
}

#endif
