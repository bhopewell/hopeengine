# -------------------------------------------------
# VERSION
# -------------------------------------------------
cmake_minimum_required(VERSION 3.12)

# -------------------------------------------------
# METAL LIB TARGET
# -------------------------------------------------
function(add_metal_shader_target shader_sources)

	set(METAL_SHADER_OUTPUT_DIRECTORY ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/${CMAKE_CFG_INTDIR})

	# Generate *.air file for each shader source
	foreach(_src IN LISTS shader_sources)
		get_filename_component(_srcName ${_src} NAME_WE)

		add_custom_command(OUTPUT MetalLib_AIRCMD
			COMMAND xcrun -sdk macosx metal -c ${_src} -o ${_srcName}.air
			WORKING_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}"
			COMMENT "Creating ${_srcName}.air"
			VERBATIM
		)

		set(AIR_FILES ${AIR_FILES} ${_srcName}.air)
	endforeach()

	# Compile all *.air files into a single *metallib file
	add_custom_command(OUTPUT HopeMTLShadersLibCmd
		COMMAND xcrun -sdk macosx metallib ${AIR_FILES} -o "${METAL_SHADER_OUTPUT_DIRECTORY}/HopeMTLShaders.metallib"
		WORKING_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}"
		COMMENT "Creating HopeMTLShaders.metallib"
		DEPENDS MetalLib_AIRCMD
		VERBATIM)

	add_custom_command(OUTPUT CreateShaderOutDirIfNotExist
		COMMAND mkdir -p ${METAL_SHADER_OUTPUT_DIRECTORY}
		WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
		COMMENT "Creating ${METAL_SHADER_OUTPUT_DIRECTORY}"
		VERBATIM)

	add_custom_target(Hope_MetalLib
		ALL
		DEPENDS CreateShaderOutDirIfNotExist HopeMTLShadersLibCmd
		SOURCES ${shader_sources}
	)

endfunction(add_metal_shader_target)
