# -------------------------------------------------
# VERSION
# -------------------------------------------------
cmake_minimum_required(VERSION 3.12)

# -------------------------------------------------
# GROUPING
# -------------------------------------------------
function(assign_groups sourceList)
	# GROUPS
	foreach(_source IN LISTS ${sourceList})
	if (IS_ABSOLUTE "${_source}")
	    file(RELATIVE_PATH _source_rel "${CMAKE_CURRENT_SOURCE_DIR}" "${_source}")
	else()
	    set(_source_rel "${_source}")
	endif()
	    get_filename_component(_source_path "${_source_rel}" PATH)
	    string(REPLACE "/" "\\" _source_path_msvc "${_source_path}")
	    source_group("${_source_path_msvc}" FILES "${_source}")
	endforeach()
endfunction(assign_groups)
